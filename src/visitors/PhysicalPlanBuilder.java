
package visitors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;
import database.Operator;
import database.ScanOperator;
import database.SelectOperator;
import database.JoinOperator;
import database.ProjectOperator;
import database.BNLJOperator;
import database.DatabaseCatalog;
import database.DuplicateEliminationOperator;
import database.HashDuplicateEliminationOperator;
import database.InternalSortOperator;
import database.ExternalSortOperator;
import database.QueryPlan;
import database.SMJExpressionVis;
import database.SMJOperator;
import database.IndexScanOperator;
import logic.*;
import service.ExpressionService;
import service.OptimalOrder;
import optimizer.Selection;
import bPlusTree.IndexInfo;


/**
 * Implementation of the PhysicalPlanBuilder interface used to handle the plan_builder_config
 * file that is going to contain what sorts and joins to use. It will also take into account
 * the number of pages that are being used. It then creates a left deep Query tree with all the
 * operators. It will use either Sort Merge Join, Block Nested Loop Join or Tuple Nested Loop Join
 * It will also use either External Sort which takes into account paging. Or it will use Interanal
 * Sort which is unlimited memory.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class PhysicalPlanBuilder {

	private Operator physicalOperator;
	private int joinMethod;
	private int joinNumberOfPages;
	private boolean internalSort;
	private int sortNumberOfPages;
	private ArrayList<String> fromList;
	private ArrayList<String> fullCol;
	
	/** Constructor PhysicalPlanBuilder
     * Creates Physical Plan using this class
     *
     */
	public PhysicalPlanBuilder(ArrayList<String> fromList, ArrayList<String> fullCol) {
		this.fromList = fromList;
		this.fullCol = fullCol;
		/*
		String plan_config_file = DatabaseCatalog.getInstance().getInputDir() + "/plan_builder_config.txt";
		BufferedReader config_file_reader = null;
		
		try {
			config_file_reader = new BufferedReader(new FileReader(plan_config_file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			
			for (int i = 0; i < 2; i++) {
					String line = config_file_reader.readLine();
					String[] dataStrings = line.split(" ");
					int length = dataStrings.length;
					Integer[] data = new Integer[length];
					for (int j = 0; j < length ; j++){
						data[j] = Integer.parseInt(dataStrings[j]);
					}
					if (i == 0) {
						joinMethod = data[0];
						if (data[0] == 1)
							joinNumberOfPages = data[1];
					}
					else {
						internalSort = (data[0] == 0) ? true : false;
						if (data[0] != 0)
							sortNumberOfPages = data[1];
					}
			}
						
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		*/
		internalSort = false;
		joinNumberOfPages = 4;
		sortNumberOfPages = 4;
		DatabaseCatalog.getInstance().setJoinPages(joinNumberOfPages);
		DatabaseCatalog.getInstance().setSortPages(sortNumberOfPages);
		physicalOperator = null;
		
	}
	/** Method getPysicalOperator
     * Gets the Physical Operator 
     *
     * @return Operator physicalOperator: Physical Operator
     */
	public Operator getPhysicalOperator() {
		return physicalOperator;
	}
	/** Method visit
     * Visits a Logical Scan Operator and creates a physical operator for it
     *
     * @param LogicScanOperator lop: Logical Scan Operator to scan across a table
     */
	public void visit(LogicScanOperator lop) {
		physicalOperator = new ScanOperator(lop.getTablename(), lop.getAliasname());
	}
	/** Method visit
     * Visits a Logical Select Operator and creates a physical operator for it
     *
     * @param LogicSelectOperator lop: Logical Select Operator to perform selection across a table
     */
	/*
	public void visit(LogicSelectOperator lop) {
		
		LogicScanOperator child = (LogicScanOperator) lop.getChild();
		boolean  useIndexScan = false;
		boolean allocateSelect = false;
		String tableName = child.getTablename();
		String aliasName = child.getAliasname() == null ? 
								tableName : child.getAliasname();
		String columnName = null;
		String indexColumnName = null;
		Boolean clustered = null;
		Integer[] lowHighValue = new Integer[2];
			
		Expression selCond = lop.getExpression();
		List<Expression> expressionList = ExpressionService.decomposeAndExpression(selCond);
			
		for (Expression e : expressionList) {
				
			Expression l = ((BinaryExpression) e).getLeftExpression();
			Expression r = ((BinaryExpression) e).getRightExpression();
				
			if (l instanceof Column && r instanceof LongValue ||
					l instanceof LongValue && r instanceof Column) {
				if (e instanceof EqualsTo 
						|| e instanceof GreaterThan
						|| e instanceof GreaterThanEquals
						|| e instanceof MinorThan
						|| e instanceof MinorThanEquals ) {
					
					columnName = (l instanceof Column) ? l.toString() : r.toString();
					if (columnName.indexOf('.') != -1)
						columnName = columnName.split("\\.")[1];
					columnName = tableName + "." + columnName; 
					clustered = DatabaseCatalog.getInstance().indexIsClustered(columnName);
					if (clustered == null) {continue; } else { useIndexScan = true; indexColumnName = columnName; break; }
				}		
			}
		}
			
		if (useIndexScan == true) {
			for (Expression e : expressionList) {
				Expression l = ((BinaryExpression) e).getLeftExpression();
				Expression r = ((BinaryExpression) e).getRightExpression();
				
				if (l instanceof Column && r instanceof LongValue ||
						l instanceof LongValue && r instanceof Column) {
					if (e instanceof EqualsTo 
							|| e instanceof GreaterThan
							|| e instanceof GreaterThanEquals
							|| e instanceof MinorThan
							|| e instanceof MinorThanEquals ) {
						
						Integer val = null;
								
						columnName = (l instanceof Column) ? l.toString() : r.toString();
						val = (l instanceof Column) ? Integer.parseInt(r.toString()) : Integer.parseInt(l.toString());
						if (columnName.indexOf('.') != -1)
							columnName = columnName.split("\\.")[1];
						columnName = tableName + "." + columnName; 
						if (DatabaseCatalog.getInstance().indexIsClustered(columnName) == null) { allocateSelect = true; continue; }
						
						if (e instanceof GreaterThan) {
							if(lowHighValue[0] == null) {
								lowHighValue[0] = val;
							} else {
								lowHighValue[0] = Math.max(lowHighValue[0], val);
							}					
						} else if (e instanceof GreaterThanEquals) {
							if(lowHighValue[0] == null) {
								lowHighValue[0] = val - 1;
							} else{
								lowHighValue[0] = Math.max(lowHighValue[0], val - 1);
							}
						} else if(e instanceof MinorThan){
							if(lowHighValue[1] == null){
								lowHighValue[1] = val;
							} else {
								lowHighValue[1] = Math.min(lowHighValue[1],val);
							}	
						} else if(e instanceof MinorThanEquals) { 
							if(lowHighValue[1] == null){
								lowHighValue[1] = val + 1;
							} else {
								lowHighValue[1] = Math.min(lowHighValue[1], val + 1);
							}
						} else if (e instanceof EqualsTo) {
							lowHighValue[0] = val - 1;
							lowHighValue[1] = val + 1;
						}	
					}
				}
			}
		
			if (clustered != null) {
				System.out.println("\tUsing index scan operator for Table: " + tableName + "  Attribute: " + indexColumnName + "  Lowkey: " + lowHighValue[0] + "  Highkey: " + lowHighValue[1]);
				physicalOperator = new IndexScanOperator(tableName, aliasName, indexColumnName, lowHighValue[0], lowHighValue[1]);
			}
		}
			
		if (useIndexScan == false) 
			lop.getChild().accept(this);
		if (useIndexScan == false || allocateSelect == true)
			physicalOperator = new SelectOperator(physicalOperator, lop.getExpression());
		else
			System.out.println("\tSkipped select operator allocation");
	
	}
	*/
	public void visit(LogicSelectOperator lop) {
//		throw new UnsupportedOperationException("visit is under construction");
		// precondition: the child of a select operator must be a scan operator.
		boolean hasIdxAttr;
		LogicScanOperator child = (LogicScanOperator) lop.getChild();
		ScanOperator scanOp = null;
		{
			String tableName = child.getTablename();
			String aliasName = child.getAliasname() == null ? 
									tableName : child.getAliasname();
			
			//TODO
			IndexInfo idxinfo = Selection.whichIndexToUse(tableName, lop.getExpression());
			hasIdxAttr = (idxinfo != null);
			if (idxinfo != null) {
				double rf = idxinfo.getRF();
				lop.setReductionFactor(rf);
			}
			
			System.out.println("Table name: " + tableName);
			System.out.println("has index attribute: " + hasIdxAttr);
			if (hasIdxAttr) {
				System.out.println("============= Building Index Scan operator==============");
				
				Integer[] range = ExpressionService.bPlusKeys(idxinfo.getAttribute(), lop.getExpression());
				System.out.println("The range is " + range[0] + ", " + range[1]);
				System.out.println("============= End Building Index Scan operator==============");
				Integer lowKey = null;
				Integer highKey = null;
				if (range[0] != null) {
					lowKey = range[0]-1;
				}
				if (range[1]!=null) {
					highKey = range[1]+1;
				}
				physicalOperator = new IndexScanOperator(tableName, aliasName, idxinfo.getAttribute(), lowKey, highKey);
			}
		} 
		
		// if not assigned with index scan.
		if (!hasIdxAttr) physicalOperator = new ScanOperator(child.getTablename(), child.getAliasname());
		
		physicalOperator = new SelectOperator(physicalOperator, lop.getExpression());
	}
	
	
	/** Computes join order for multi join operator.
	 * Then for each join, checks to see if SMJ is applicable (all equality
	 * conditions). Otherwise uses BNLJ.
	 * Constructs all physical join operators.
	 * 
	 * @param lop
	 */
	public void visit(LogicMultiJoinOperator lop) {
		
		Operator lchild;
		Operator rchild;
		
		LogicOperator logicLeftChild;
		LogicOperator logicRightChild;
		
		List<LogicOperator> children;
		
		List<String> fromList;
		
		fromList = lop.getFromList();
		children = lop.getChildrenList();
		
		OptimalOrder order = lop.getJoinOrder();
		
		//get child corresponding to first relation in joinOrder
		logicLeftChild = children.get(fromList.indexOf(order.joinOrder.get(0)));
		logicLeftChild.accept(this);
		lchild = physicalOperator;
		
		SMJExpCheckerVis checker = new SMJExpCheckerVis();
		
		for (int i=1; i < order.joinOrder.size(); i++) {
			logicRightChild = children.get(fromList.indexOf(order.joinOrder.get(i)));
			logicRightChild.accept(this);
			rchild = physicalOperator;
			
			Expression e = order.joinExpressions.get(i-1);
			
			//decide on join operator type!
			if (e != null) {
				e.accept(checker);
			}
			else {
				//don't want SMJ for cross product
				checker.setQualifiesForSMJFalse();
			}
			
			//if qualifies, then use SMJ
			if (checker.qualifiesForSMJ()) {
				ArrayList<String> leftSchema = lchild.getSchema();
				ArrayList<String> rightSchema = rchild.getSchema();
				
				SMJExpressionVis smjExpVis = new SMJExpressionVis(leftSchema, rightSchema);
				
				e.accept(smjExpVis);
				
				ArrayList<String> leftEqualityColumns = smjExpVis.getLeftColumns();
				ArrayList<String> rightEqualityColumns = smjExpVis.getRightColumns();
				
				ArrayList<String> leftOrderByColumns = smjExpVis.generateOrderByList(leftEqualityColumns);
				ArrayList<String> rightOrderByColumns = smjExpVis.generateOrderByList(rightEqualityColumns);
				
				Operator leftSorted = new ExternalSortOperator(lchild, leftOrderByColumns);
				Operator rightSorted = new ExternalSortOperator(rchild, rightOrderByColumns);
				
//				Operator leftSorted = new InternalSortOperator(lchild, leftOrderByColumns);
//				Operator rightSorted = new InternalSortOperator(rchild, rightOrderByColumns);
				
				physicalOperator = new SMJOperator(leftSorted, rightSorted, leftEqualityColumns, rightEqualityColumns, e);
				
			}
			else {
				physicalOperator = new BNLJOperator(lchild, rchild, e, this.joinNumberOfPages);
			}
			
			lchild = physicalOperator;
			
			checker.reset();
		}
	}

	/** Method visit
     * Visits a Logical Join Operator and creates a physical operator for it
     *
     * @param LogicJoinOperator lop: Logical Join Operator to perform Join across two tables
     */
	public void visit(LogicJoinOperator lop) {
		Operator lchild;
		Operator rchild;
		
		lop.getLeftChild().accept(this);
		lchild = physicalOperator;
		
		lop.getRightChild().accept(this);
		rchild = physicalOperator;
		
		if (joinMethod == 0)
			physicalOperator = new JoinOperator(lchild, rchild, lop.getExpression());
		else if (joinMethod == 1){
			int joinPages = DatabaseCatalog.getInstance().getJoinPages();
			physicalOperator = new BNLJOperator(lchild, rchild, lop.getExpression(), joinPages);
		}
		else {
			ArrayList<String> leftSchema = lchild.getSchema();
			ArrayList<String> rightSchema = rchild.getSchema();
			
			SMJExpressionVis smjExpVis = new SMJExpressionVis(leftSchema, rightSchema);
			
			Expression joinExpression = lop.getExpression();
			joinExpression.accept(smjExpVis);
			
			ArrayList<String> leftEqualityColumns = smjExpVis.getLeftColumns();
			ArrayList<String> rightEqualityColumns = smjExpVis.getRightColumns();
			
			ArrayList<String> leftOrderByColumns = smjExpVis.generateOrderByList(leftEqualityColumns);
			ArrayList<String> rightOrderByColumns = smjExpVis.generateOrderByList(rightEqualityColumns);
			
			Operator leftSorted;
			Operator rightSorted;
			
			if (internalSort) {
				leftSorted = new InternalSortOperator(lchild, leftOrderByColumns);
				rightSorted = new InternalSortOperator(rchild, rightOrderByColumns);
			}
			else {
				leftSorted = new ExternalSortOperator(lchild, leftOrderByColumns);
				rightSorted = new ExternalSortOperator(rchild, rightOrderByColumns);
			}
			
			physicalOperator = new SMJOperator(leftSorted, rightSorted, leftEqualityColumns, rightEqualityColumns, lop.getExpression());
		}
		
	}
	/** Method visit
     * Visits a Logical Project Operator and creates a physical operator for it
     *
     * @param LogicProjectOperator lop: Logical Project Operator to perform Projection across a table
     */
	public void visit(LogicProjectOperator lop) {
		
		lop.getChild().accept(this);
		
		ArrayList<String> selColumns = new ArrayList<String>();
		
		for (SelectItem si : lop.getSelectItems()) {
			if (si instanceof AllColumns) {
				for (int i=0; i <fullCol.size(); i++) {
					String table = fullCol.get(i);
					ArrayList<String> schema = new ArrayList<String>(DatabaseCatalog.getInstance().getTableSchema(table));
					String alias = fromList.get(i);
					for (int j = 0; j < schema.size(); j++) {
						schema.set(j, schema.get(j).replaceFirst(table, alias));
					}
					selColumns.addAll(schema);
				}
				System.out.println("SEL COLUMNS " + selColumns);
				
			}
			else
			{
				Column col = (Column) ((SelectExpressionItem) si).getExpression();
				if (col.getTable() != null && (col.getTable().getName()) != null) {
					String tab = col.getTable().getName();
					selColumns.add(tab + '.' + col.getColumnName());
				}
				else {
					selColumns.add(col.getColumnName());
				}
			}
		}
		physicalOperator = new ProjectOperator(physicalOperator, selColumns);
	}
	/** Method visit
     * Visits a Logical Distinct Operator and creates a physical operator for it
     *
     * @param LogicDistinctOperator lop: Logical Distinct Operator to perform distinct across a table
     */
	public void visit(LogicDistinctOperator lop) {
		
		lop.getChild().accept(this);
		
		ArrayList<String> unqColumns = new ArrayList<String>();
		
		for (SelectItem si : lop.getUniqueItems()) {
			if (si instanceof AllColumns) {
				unqColumns = physicalOperator.getSchema();
			}
			else
			{
				Column col = (Column) ((SelectExpressionItem) si).getExpression();
				if (col.getTable() != null && (col.getTable().getName()) != null) {
					String tab = col.getTable().getName();
					unqColumns.add(tab + '.' + col.getColumnName());
				}
				else {
					unqColumns.add(col.getColumnName());
				}
			}
		}
		if (internalSort == true)
			physicalOperator = new InternalSortOperator(physicalOperator, unqColumns);
		else{
			physicalOperator = new ExternalSortOperator(physicalOperator, unqColumns);
			
		}
		
		physicalOperator = new DuplicateEliminationOperator(physicalOperator);
	}
	
	/** Method visit
     * Visits a Logical Sort Operator and creates a physical operator for it
     *
     * @param LogicSortOperator lop: Logical Sort Operator to perform Sort across a table
     */
	public void visit(LogicSortOperator lop) {
		
		lop.getChild().accept(this);
		if (internalSort == true)
			physicalOperator = new InternalSortOperator(physicalOperator, lop.getOrderbyItems());
		else{
			physicalOperator = new ExternalSortOperator(physicalOperator, lop.getOrderbyItems());
		}
		
	}
	
}