package logic;

import net.sf.jsqlparser.expression.Expression;
import optimizer.Selection;
import visitors.PhysicalPlanBuilder;
import java.util.HashMap;
/** Operator used to implement Where. 
 *  Simple logical tree that has the From node in its correct
 *  position in the tree
 *  
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public class LogicSelectOperator extends LogicUnaryOperator {
	
	private String tableName;
	private Expression exp;
	private double reductionFactor = 0.0;
	private HashMap<String, Double> rfArray;
	/** Constructor LogicSelectOperator
     * Creates Logic Select Operator
     *
     * @param LogicOperator child: child Logic Operator
     * @param Expression exp: Condition to perform selection
     */
	public LogicSelectOperator (LogicOperator child, Expression exp) {
		super(child);
		this.setTableName(((LogicScanOperator) child).tablename);
		this.exp = exp;
		rfArray = Selection.computeReductionFactors(tableName, exp);
		
	}
	/** Method getExpression
     * Gets Expression from Join
     *
     * @return Expression exp: Expression that the join operator performs.
     */
	public Expression getExpression() {
		return exp;
	}
	/** Method accept
     * Accepts PhysicalPlanBuilder
     *
     * @param PhysicalPlanBuilder ppb: PhysicalPlanBuilder that builds the Physical Query Plan
     */
	@Override
	public void accept (PhysicalPlanBuilder ppb) {
		
			ppb.visit(this);
	}
	/** Method used to print the current operator
	 *  
	 */
	@Override
	public String print() {
		return String.format("Select[%s]", 
				((exp == null) ? "null" : exp.toString()));
	}
	/** Method used to set Reduction Factor
	 *  @param double d -- Reduction Factor
	 */
	public void setReductionFactor(double d) {
		this.reductionFactor = d;
	}
	/** Method used to get Reduction Factor
	 *  @param double reductionFactor -- Reduction Factor
	 */
	public double getReductionFactor() {
		return reductionFactor;
	}
	/** Method used to set Reduction Factors
	 *  @param HashMap<String,Double> rf -- Reduction Factors
	 */
	public void setReductionFactors(HashMap<String, Double> rf) {
		this.rfArray = rf;
	}
	/** Method used to get Reduction Factors
	 *  @param HashMap<String,Double> rfArray -- Reduction Factors
	 */
	public HashMap<String, Double> getReductionFactors() {
		return rfArray;	
	}
	/** Gets name of table this select operates on
	 * 
	 * @return name of table
	 */
	public String getTableName() {
		return tableName;
	}
	/** Sets name of table this select operates on
	 * 
	 * @param tableName -- name of table
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

}
