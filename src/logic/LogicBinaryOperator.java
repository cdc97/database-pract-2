package logic;

import visitors.PhysicalPlanBuilder;
/** Operator used to implement joins. 
 *  This is an abstract class than join uses.
 *  Simple logical tree that has the Join node in its correct
 *  position in the tree
 *  
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public abstract class LogicBinaryOperator extends LogicOperator {
	
	private LogicOperator left;
	private LogicOperator right;
	/** Constructor LogicBinaryOperator
     * Creates Logic Binary Operator
     *
     * @param LogicOperator left: left Logic Operator
     * @param LogicOperator right: right Logic Operator 
     */
	public LogicBinaryOperator (LogicOperator left, LogicOperator right) {
		this.left = left;
		this.right = right;
	}
	

	/** Get left child
	 * @return left child node
	 */
	public LogicOperator getLeftChild() {
		return left;
	}


	/** Get right child
	 * @return right child node
	 */
	public LogicOperator getRightChild() {
		return right;
	}
	

}
