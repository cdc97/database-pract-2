package logic;

import java.util.ArrayList;
import java.util.List;

import net.sf.jsqlparser.statement.select.SelectItem;
import visitors.PhysicalPlanBuilder;
/** Operator used to implement elimination of duplicate tuples.
 *  Precondition: None
 *  Simple logical tree that has the Distinct node in its correct
 *  position in the tree
 *  
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public class LogicDistinctOperator extends LogicUnaryOperator {
	private List<SelectItem> uniqueItems;
	
	/** Constructor LogicDistinctOperator
     * Creates Logic Distinct Operator
     *
     * @param LogicOperator child: Logic Operator following Distinct
     */
	public LogicDistinctOperator (LogicOperator child, List<SelectItem> unique) {
		super(child);
		this.uniqueItems = unique;
	}
	
	/** Method getUniqueItems
     * Gets Unique/Select Items from Logical Plan
     *
     * @return List<SelectItem> selectItems: Select Condition for Logical Query Plan
     */
	public List<SelectItem> getUniqueItems() {
		return uniqueItems;
	}
	/** Method accept
     * Visits the Distinct Operator
     *
     * @param PhysicalPlanBuilder ppb: Physical Plan Builder
     */
	@Override
	public void accept (PhysicalPlanBuilder ppb) {
		
			ppb.visit(this);
	}
	@Override
	public String print() {
		return "DupElim";
	}
}
