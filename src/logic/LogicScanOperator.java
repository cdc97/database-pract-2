package logic;

import visitors.PhysicalPlanBuilder;
import java.io.PrintStream;
/** Operator used to implement From. 
 *  Simple logical tree that has the From node in its correct
 *  position in the tree
 *  
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public class LogicScanOperator  extends LogicOperator {
	
	String tablename;
	String aliasname;
	/** Constructor LogicScanOperator
     * Creates Logic Scan Operator
     *
     * @param String tablename: Table to perform scan on
     * @param String aliasname: alias name of table
     */
	public LogicScanOperator (String tablename, String aliasname)
	{
		this.tablename = tablename;
		this.aliasname = aliasname;
	}
	/** Method accept
     * Accepts PhysicalPlanBuilder
     *
     * @param PhysicalPlanBuilder ppb: PhysicalPlanBuilder that builds the Physical Query Plan
     */
	public void accept (PhysicalPlanBuilder ppb)
	{
		ppb.visit(this);
	}
	/** Method getTablename
     * Gets Table Name from Scan Operator
     *
     * @return String tablename: name of table to scan
     */
	public String getTablename() {
		return tablename;
	}
	/** Method getAliasname
     * Gets Alias Name from Scan Operator
     *
     * @return String aliasname: name of alias to scan
     */
	public String getAliasname() {
		return aliasname;
	}
	/** Method used to print the current operator
	 *  
	 */
	@Override
	public String print() {
		return String.format("Leaf[%s]", this.tablename);
	}
	/** Method used to print the tree
	 *  
	 *  @param PrintStream ps -- variable that contains the printing stream
	 *  @param int lv -- integer that contains the level of the tree.
	 */
	@Override
	public void printTree(PrintStream ps, int lv) {
		printIndent(ps, lv);
		ps.println(print());
	}
}
