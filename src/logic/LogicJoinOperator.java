package logic;

import java.io.PrintStream;
import visitors.PhysicalPlanBuilder;
import net.sf.jsqlparser.expression.Expression;
/** Operator used to implement joins. 
 *  Simple logical tree that has the Join node in its correct
 *  position in the tree
 *  
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public class LogicJoinOperator extends LogicBinaryOperator {
	
	private Expression exp;
	/** Constructor LogicJoinOperator
     * Creates Logic Join Operator
     *
     * @param LogicOperator left: left Logic Operator 
     * @param LogicOperator right: right Logic Operator 
     * @param Expression exp: Expression to perform join
     */
	public LogicJoinOperator (LogicOperator left, LogicOperator right, Expression exp) {
		
		super(left, right);
		this.exp = exp;
	}
	/** Method getExpression
     * Gets Expression from Join
     *
     * @return Expression exp: Expression that the join operator performs.
     */
	public Expression getExpression() {
		return exp;
	}
	/** Method accept
     * Accepts method that physical plan builder uses. 
     *
     * @param PhysicalPlanBuilder ppb: PhysicalPlanBuilder that builds the Physical Query Plan
     */
	@Override
	public void accept (PhysicalPlanBuilder ppb) {
		
		ppb.visit(this);
	}
	/** Method used to throw exception if not valid operator
	 *  
	 *  @throws Exception on when there's an unsupported operator
	 */
	@Override
	public String print() {
		throw new UnsupportedOperationException();
	}
	/** Method used to throw exception if not valid operator
	 *  
	 *  @throws Exception on when there's an unsupported operator
	 */
	@Override
	public void printTree(PrintStream ps, int lv) {
		throw new UnsupportedOperationException();
	}
}
