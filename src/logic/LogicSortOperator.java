package logic;

import java.util.ArrayList;

import net.sf.jsqlparser.statement.select.OrderByElement;
import visitors.PhysicalPlanBuilder;

/** Operator used to implement Sort. 
 *  Simple logical tree that has the Order By node in its correct
 *  position in the tree
 *  
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public class LogicSortOperator  extends LogicUnaryOperator {

	private ArrayList<String> orders;
	/** Constructor LogicSelectOperator
     * Creates Logic Select Operator
     *
     * @param LogicOperator child: child Logic Operator
     * @param ArrayList<String> orders: List of items to order by
     */
	public LogicSortOperator (LogicOperator child, ArrayList<String> orders) {
		super(child);
		this.orders = orders;
	}
	/** Method getOrderbyItems
     * Gets Items to Order By
     *
     * @return ArrayList<String> orders: List of items to order by
     */
	public ArrayList<String> getOrderbyItems() {
		return orders;
	}
	/** Method accept
     * Accepts PhysicalPlanBuilder
     *
     * @param PhysicalPlanBuilder ppb: PhysicalPlanBuilder that builds the Physical Query Plan
     */
	@Override
	public void accept(PhysicalPlanBuilder ppb) {
		ppb.visit(this);
	}
	/** Method used to print the current operator
	 *  
	 */
	@Override
	public String print() {
		return String.format("Sort%s", 
				((orders == null) ? "[null]" : orders.toString()));
	}
}
