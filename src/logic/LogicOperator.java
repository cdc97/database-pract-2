package logic;

import visitors.PhysicalPlanBuilder;
import java.io.PrintStream;
/** Logical Operator nodes. 
 *  This is an abstract class than all logical operator nodes use.
 *  Simple logical tree that has the nodes in its correct
 *  position in the tree
 *  
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public abstract class LogicOperator {
	/** Method accept
     * Abstract Accept method
     *
     * @param PhysicalPlanBuilder ppb: PhysicalPlanBuilder that builds the Physical Query Plan
     */
	public abstract void accept (PhysicalPlanBuilder ppb);
	protected static void printIndent(PrintStream ps, int lv) {
		while (lv-- > 0)
			ps.print('-');
	}
	/** Method used to print the current operator
	 *  
	 */
	public abstract String print();
	/** Method used to print the tree
	 *  
	 *  @param PrintStream ps -- variable that contains the printing stream
	 *  @param int lv -- integer that contains the level of the tree.
	 */
	public abstract void printTree(PrintStream ps, int lv);

}
