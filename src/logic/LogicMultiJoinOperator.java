package logic;


import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import net.sf.jsqlparser.expression.Expression;
import service.DisjointSets;
import service.ExpressionService;
import service.JoinOrderBuilder;
import service.OptimalOrder;
import visitors.PhysicalPlanBuilder;

/** Class used to represent a multi join Operator. 
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class LogicMultiJoinOperator extends LogicOperator {

	private  ArrayList<String> froms;
	private HashMap<String, Expression> resConds;
	private Expression exp;
	private DisjointSets ds;
	private HashMap<HashSet<String>, Expression> specificJoinCond;
	private ArrayList<LogicOperator> children;
	
	//Optimal Order plan for the set of relations we need to join
	private OptimalOrder finalOrder;
	//private HashMap<HashSet<String>, Expression> specificJoinCond;
	
	/** Constructor. Sets vars, then computes optimal order using JoinOrderBuilder.
	 * 
	 * @param froms -- list of relations to join
	 * @param tables -- list of children to join
	 * @param res -- Relation Conditions
	 * @param ds --	Disjoint Sets
	 * @param specificJoinCond -- map from two relation set to relevant join conditions
	 */
	public LogicMultiJoinOperator(List<String> froms, 
			List<LogicOperator> tables, 
			HashMap<String, Expression> res, 
			DisjointSets ds, HashMap<HashSet<String>, Expression> specificJoinCond) {
		this.froms = (ArrayList<String>) froms;
		children = (ArrayList<LogicOperator>) tables;
		resConds = res;
		this.ds = ds;
		
		
		List<Expression> lst = new ArrayList<>();
		for (String s : res.keySet())
			lst.addAll(ExpressionService.decomposeAndExpression(res.get(s)));
		if (!lst.isEmpty())
			exp = ExpressionService.composeAndExpression(lst);
		
		this.specificJoinCond = specificJoinCond;
		JoinOrderBuilder orderBuilder = new JoinOrderBuilder(this.froms, children, specificJoinCond);
		
		finalOrder = orderBuilder.getFinalOptimalOrder();
	}
	
	/** Gets final join order
	 * 
	 * @return --Optimal Order object for all relations to join.
	 */
	public OptimalOrder getJoinOrder() {
		return finalOrder;
	}
	/** Gets list of children
	 * 
	 * @return List<LogicOperator> -- List of all children
	 */
	public List<LogicOperator> getChildrenList() {
		return children;
	}
	/** Gets Join Expressions
	 * 
	 * @return HashMap<String,Expression> -- List of all Join Expressions
	 */
	public HashMap<String, Expression> getJoinExpressions() {
		return resConds;
	}
	/** Gets From Clause
	 * 
	 * @return List<String> -- From Tables
	 */
	public List<String> getFromList() {
		return froms;
	}
	
	/** Method used to print the current operator
	 *  
	 */
	@Override
	public String print() {
		StringBuilder sb = new StringBuilder();
		sb.append("Join");
		if (exp != null)
			sb.append(String.format("[%s]", exp.toString()));
		else
			sb.append("[]");
		for (String s : ds.componentsInfo())
			sb.append('\n' + s);
		
		return sb.toString();
	}
	/** Method used to print the tree
	 *  
	 *  @param PrintStream ps -- variable that contains the printing stream
	 *  @param int lv -- integer that contains the level of the tree.
	 */
	@Override
	public void printTree(PrintStream ps, int lv) {
		printIndent(ps, lv);
		ps.println(print());
		for (LogicOperator lop : children)
			lop.printTree(ps, lv + 1);
	}
	
	/** Accepts this ppb. Just calls ppb.visit,
	 * to use visitor pattern.
	 * 
	 * @param ppb -- physical plan builder object that builds our plan
	 */
	@Override
	public void accept(PhysicalPlanBuilder ppb) {
		ppb.visit(this);
		
	}
	
}