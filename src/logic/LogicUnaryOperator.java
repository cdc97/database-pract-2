package logic;

import java.io.PrintStream;
import visitors.PhysicalPlanBuilder;
/** Logical Operator nodes. 
 *  This is an abstract class than distinct, project, scan, 
 *  select and sort use.
 *  Simple logical tree that has the nodes in its correct
 *  position in the tree
 *  
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public abstract class LogicUnaryOperator extends LogicOperator {
	
	private LogicOperator child;
	/** Constructor LogicUnaryOperator
     * Creates Logic Unary Operator
     *
     * @param LogicOperator child: child Logic Operator
     */
	public LogicUnaryOperator (LogicOperator child)
	{
		this.child = child;
	}
	
	/**getChild
	 * Getter for child
	 * 
	 * @return child node
	 */
	public LogicOperator getChild() {
		return child;
	}
	/** Method used to print the tree
	 *  
	 *  @param PrintStream ps -- variable that contains the printing stream
	 *  @param int lv -- integer that contains the level of the tree.
	 */
	@Override
	public void printTree(PrintStream ps, int lv) {
		printIndent(ps, lv);
		ps.println(print());
		child.printTree(ps, lv + 1);
	}
}
