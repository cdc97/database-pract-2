package logic;

import java.util.List;
import visitors.PhysicalPlanBuilder;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;
/** Operator used to implement projection. 
 *  Simple logical tree that has the Project node in its correct
 *  position in the tree
 *  
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public class LogicProjectOperator extends LogicUnaryOperator {
	
	private List<SelectItem> selectItems;
	/** Constructor LogicProjectOperator
     * Creates Logic Project Operator
     *
     * @param LogicOperator child: child Logic Operator
     * @param List<SelectItem> si: List of items to project on
     */
	public LogicProjectOperator (LogicOperator child, List<SelectItem> si) {
		
			super(child);
			this.selectItems = si;
	}
	/** Method getSelectItems
     * Gets Select Items from Logical Plan
     *
     * @return List<SelectItem> selectItems: Select Condition for Logical Query Plan
     */
	public List<SelectItem> getSelectItems() {
		return selectItems;
	}
	/** Method accept
     * Accepts PhysicalPlanBuilder
     *
     * @param PhysicalPlanBuilder ppb: PhysicalPlanBuilder that builds the Physical Query Plan
     */
	@Override
	public void accept(PhysicalPlanBuilder ppb) {
		ppb.visit(this);
	}
	/** Method used to print the current operator
	 *  
	 */
	@Override
	public String print() {
		return String.format("Project%s", 
				((selectItems == null) ? "[null]" : selectItems.toString()));
	}
}
