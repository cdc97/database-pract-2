
package bPlusTree;

/** Class used to represent a RecordID for a tuple. Used for the construction
 * of indexes. Only contains pageId and tupleId.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97) 
 */
public class RecordID {
	//pageId of tuple this recordID represents
	private int pageId;
	//tupleId of tuple this tupleID represents
	private int tupleId;
	
	/** Constructor for RecordID. Sets page Id and tuple Id.
	 * 
	 */
	public RecordID(int pageId, int tupleId) {
		this.setPageId(pageId);
		this.setTupleId(tupleId);
	}

	
	/** Gets the record's page id.
	 * 
	 * @return -- the integer value of the page # from which the corresponding 
	 * tuple was read
	 * (in the original relation file).
	 * 
	 */
	public int getPageId() {
		return pageId;
	}

	/** Sets the record's page id. (the page from which the corresponding tuple was
	 * read from the original relation). Used for indexing purposes.
	 * @param pageId-- the int corresponding to the page of the original relation
	 * from which the corresponding tuple was read.
	 * 
	 */
	public void setPageId(int pageId) {
		this.pageId = pageId;
	}

	/** Gets this record's tuple Id
	 * 
	 * @return -- the integer value of the index of the corresponding
	 * tuple on the page it was read from.
	 */
	public int getTupleId() {
		return tupleId;
	}

	/** Sets this record's tuple Id. tuple id is the index of the corresponding 
	 * tuple within the page from which it was read.
	 * @param tupleId -- the index of this tuple on the page it was read from.
	 */
	public void setTupleId(int tupleId) {
		this.tupleId = tupleId;
	}
	
	/** To string function used to print out a record ID. 
	 * Used for debugging only.
	 * @return -- the string representation of this record ID
	 */
	public String toString(){
		return "(" + pageId+","+tupleId+")";
		
	}
}
