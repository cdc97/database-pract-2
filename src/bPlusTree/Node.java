
package bPlusTree;
import java.util.ArrayList;

import database.BinaryTupleWriter;

/** Abstract class used for BPlusTree nodes. Allows us to have both leaf and index nodes
 * in our BPlusTree. Specifies variables and functions that nodes must have.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97) 
 */
public abstract class Node<K,T> {
    //boolean specifying whether the given node instance is a leaf or not.
	protected boolean isLeafNode;
	//list of keys for this node.
	public ArrayList<K> keys;
    //the order of the BPlusTree this node is part of.
	protected int order;
	//the serialization address of this node.
	private int address;
	
	/** Check to see if this node is overflowed. Not used for our BPlusTree implementation.
	 * 
	 * @return -- boolean whether node is overflowed.
	 */
	public boolean isOverflowed() {
		return keys.size() > 2 * order;
	}

    /** Check to see if this node is underflowed.
     * Used to check if last node at a given level is underflowed.
     *
     * @return -- boolean whether node is underflowed. 
     */
	public boolean isUnderflowed() {
		return keys.size() < order;
	}

    /** Check to see if this node is completely full.
     * 
     * @return -- boolean whether node is completely full.
     */
	public boolean isFull(){
		return keys.size() == 2 * order;
	}
	
	/** Method used within specific implementations of nodes to serialize themselves
	 * with a given binary tuple writer.
	 * 
	 * @param btw -- binary tuple writer used to serialize.
	 */
	public abstract void serializeNode(BinaryTupleWriter btw);

    /** Method used to get this node's serialization address.
     * 
     * @return -- this node's serialization address.
     */
	public int getAddress() {
		return address;
	}

    /** Method used to set this node's serialization address.
     * 
     * @param address -- the serialization address for this node.
     */
	public void setAddress(int address) {
		this.address = address;
	}
	
}
