package bPlusTree;

/**
 * Index Info contains the information on whether a table has an index
 * and what its reduction factor values are. It used by the LogicSelectOperator class
 * as well as the Selection class file in optimizer. It helps in determining
 * where to select and join across.  
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public class IndexInfo {

	private String relt = "";
	private String attr = "";
	private boolean clust = false;
	private int order = 1;
	private int numOfLeafNodes = -1;
	private double rf = 0;
	/**Constructor for IndexInfo class. It just creates and sets our variables.
	 * 
	 * @param String r -- String Relation ex// Sailors S
	 * @param String a -- String Attribute ex// Column A
	 * @param boolean c -- boolean to determine if clustered
	 * @param int o -- int order
	 * @param int i -- int number of leaf nodes
	 */
	public IndexInfo(String r, String a, boolean c, int o, int i) {
		relt = r;
		attr = a;
		clust = c;
		order = o;
		numOfLeafNodes = i;
	}	
	
	/**
	 * get the number of leaf nodes.
	 * @return num of leaf nodes.
	 */
	public int getNumOfLeafNodes() {
		return numOfLeafNodes;
	}
	
	/**
	 * set the number of leaf nodes.
	 * @param int num of leaf nodes.
	 */
	public void setNumOfLeafNodes(int num) {
		numOfLeafNodes = num;
	}
	/**
	 * get the String Attribute column
	 * @return String attr
	 */
	public String getAttribute() {
		return attr;
	}
	/**
	 * get the clustered value
	 * @return boolean clust
	 */
	public boolean isClustered() {
		return clust;
	}
	/**
	 * set the reduction value.
	 * @param double rf.
	 */
	public void setRF(double rf) {
		this.rf = rf;
	}
	/**
	 * get the reduction value
	 * @return double rf
	 */
	public double getRF() {
		return rf;
	}
}
