package bPlusTree;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import database.BinaryTupleWriter;

/** IndexNode class used for IndexNodes within our BPlusTree. Contains an 
 * arraylist of keys (inherited from Node) and an arrayList of children nodes.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public class IndexNode<K,T> extends Node<K,T> {

	//ArrayList of children nodes within BPlusTree
	public ArrayList<Node<K,T>> children; 
	//ArrayList of children addresses. Used for serialization.
	private ArrayList<Integer> childrenAddresses;

    /** Constructor for IndexNode. Initializes isLeafNode, keys, children, and 
     * order, then adds the given key and children to the corresponding arraylists.
     * 
     * @param key -- the key value used to separate child0 from child1.
     * @param child0 -- a child node. Comes before child1 (with regards to key value)
     * @param child1 -- a child node that comes after child0.
     * @param order -- the order of the BPlusTree this node belongs to.
     */
	public IndexNode(K key, Node<K,T> child0, Node<K,T> child1, int order) {
		isLeafNode = false;
		keys = new ArrayList<K>();
		keys.add(key);
		children = new ArrayList<Node<K,T>>();
		children.add(child0);
		children.add(child1);
		this.order = order;
	}
	
	/** Constructor for IndexNode. Initializes isLeafNode, keys, children, and order.
	 * 
	 * @param order -- the order of the BPlusTree this node belongs to.
	 */
	public IndexNode(int order) {
		isLeafNode = false;
		keys = new ArrayList<K>();
		children = new ArrayList<Node<K,T>>();
		this.order = order;
	}
	
	
	/** Adds a child to end of index node's children. If children is not empty,
	 * adds the smallest key from the left most leaf of the child to add
	 * to this node's keys. 
	 * 
	 * @param child -- the child node to add to this indexNode
	 * NOTE: assumes children are added in sorted order (from least to greatest)
	 */
	public void addChild(Node<K,T> child) {
		if (!this.children.isEmpty()) {
			K key = getSmallestKeyFromLeftMostLeaf(child);
			this.keys.add(key);
		}

		this.children.add(child);
	}
    
    /** Inserts a child in the first position of this index node's child list.
     * Adds the previous first child's smallest key from its left most leaf
     * to the first position in index node's
     * key list. This method is used to handle underflow cases.
     * 
     * @param child -- child node to insert into first position.
     */
	public void insertChildInFirstPosition(Node<K,T> child) {
		//get first child's first key
		K newKey = getSmallestKeyFromLeftMostLeaf(this.children.get(0));
		this.keys.add(0, newKey);
		this.children.add(0, child);
		
	}

    /** Helper method to get the smallest key from a node's left most leaf
     * child (or a leaf node itself). Returns this key.
     * 
     * @param node -- the node to get the smallest key from left most leaf child (or node itself).
     * @return -- the smallest key from left most leaf child (or node itself)
     */
	private K getSmallestKeyFromLeftMostLeaf(Node<K,T> node) {
		Node<K,T> currentNode = node;
		while(!currentNode.isLeafNode) {
			currentNode = ((IndexNode<K,T>) currentNode).children.get(0);
		}
		
		return currentNode.keys.get(0);
	}

    /** Used to call the appropriate serializeNode method within binary 
     * tuple writer to serialize this index nodes. Before making the call,
     * compiles children addresses to use for serialization.
     * 
     * @param btw -- the binary tuple writer to use for serialization
     */
	@Override
	public void serializeNode(BinaryTupleWriter btw) {
		compileChildrenAddresses();
		
		btw.writeIndexNode((ArrayList<Integer>) keys, childrenAddresses);
		
	}
	
	/** Helper method used to compile a list of children addresses for
	 * serialization.
	 * 
	 */
	private void compileChildrenAddresses() {
		childrenAddresses = new ArrayList<Integer>();
		for (Node<K,T> child : children) {
			childrenAddresses.add(child.getAddress());
		}
	}
	
}
