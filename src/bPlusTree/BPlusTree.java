package bPlusTree;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;

import database.BinaryTupleReader;
import database.BinaryTupleWriter;
import database.DatabaseCatalog;
import database.ScanOperator;
import database.SortOperator;


/**
 * BPlusTree class used for creating indexes on target attributes within target relations.
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public class BPlusTree<K,T> {
    //the root of the BPlusTree instance
	public Node<K,T> root;
	//the target relation to construct BPlusTree on
	private String targetRelation;
	//the target attribute within the target relation to construct index/ BPlusTree on
	//target attribute has format of <relation>.<column>
	private String targetAttribute;
	//the order of this BPlusTree instance. Also known as D (in write up)
	private int order;
	//Arraylist of nodes to serialize, in seralization order.
	private ArrayList<Node<K,T>> serializationList;
	//the number of leaves contained in bPlusTree instance
	private int numLeaves;
	
	/** Constructor for BPlusTree. Clusters if necessary, then creates a bPlusTree
	 * for the target attribute of the target relation (with specified order). 
	 * Then writes serialized tree to specified fileOutPath.
	 * 
	 * @param cluster -- boolean value to specify whether we need to build a clustered index
	 * @param targetRelation -- the target relation to build index for.
	 * @param targetAttribute -- the target attribute within the target relation to build index on.
	 * @param order -- the integer value specifying the order of the BPlusTree
	 * @param fileOutPath -- the String path to where we should write the serialized tree index
	 * 
	 */
	public BPlusTree(boolean cluster, String targetRelation, String targetAttribute, int order, String fileOutPath) {

		this.serializationList = new ArrayList<Node<K,T>>();
		this.targetRelation = targetRelation;
		this.targetAttribute = targetAttribute;
		this.order = order;
		
		if (cluster) {
			try {
				clusterRelation();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		ArrayList<Entry<Integer, ArrayList<RecordID>>> indexData = null;
		try {
			indexData = getAndOrganizeIndexData();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		bulkLoad(indexData);
		
		try {
			serializeTree(fileOutPath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	/** Helper method to order the target relation by the target attribute.
	 * Overwrites target relation file with new sorted order relation.
	 * 
	 */
	private void clusterRelation() throws FileNotFoundException {
		ScanOperator scan = new ScanOperator(targetRelation, null);
		
		ArrayList<String> orderBy = new ArrayList<String>();
		orderBy.add(targetAttribute);
		SortOperator sorted = new SortOperator(scan, orderBy);
		
		String relationPath = DatabaseCatalog.getInstance().getTablePath(targetRelation);
		BinaryTupleWriter btw = new BinaryTupleWriter(relationPath);
		
		btw.dumpOperator(sorted);
		
	}
	
	/** Helper method to handle bulk loading the bplus tree. Constructs leaf nodes
	 * from given indexData. Then constructs index nodes from leaf nodes, the index
	 * nodes from those index nodes, until we get a single index node. This index node becomes 
	 * root. If we only have a single leaf node, then our tree is a single index node 
	 * with a single child leaf node, and no key.
	 * 
	 * @param indexData -- an arrayList of entries between integers (attribute values)
	 * and arraylist of record ids (of tuples). Used as key and value for leaf data 
	 * entries.
	 */
	private void bulkLoad(ArrayList<Entry<Integer, ArrayList<RecordID>>> indexData) {
		
		ArrayList<Node<K,T>> lowerNodeList = new ArrayList<Node<K,T>>();
		
		//create leaf level first
		LeafNode<K,T> currentNode = new LeafNode<K,T>(order);
		lowerNodeList.add(currentNode);
		while(!indexData.isEmpty()) {
			if (currentNode.isFull()) {
				currentNode = new LeafNode<K,T>(order);
				lowerNodeList.add(currentNode);
			}
			Map.Entry<K, T> entry = (Entry<K, T>) indexData.remove(0);
			currentNode.addEntry(entry);
		}
		
		//now handle case of underflowed last leaf
		if (currentNode.isUnderflowed()) {
			LeafNode<K,T> secondToLast = (LeafNode<K,T>) lowerNodeList.get(lowerNodeList.size()-2);
			handleLeafUnderflow(secondToLast, currentNode);
		}
		
		numLeaves = lowerNodeList.size();
		serializationList.addAll(lowerNodeList);
		
		//now generate the rest of the tree (index nodes)
		ArrayList<Node<K,T>> upperNodeList = new ArrayList<Node<K,T>>();
		
		IndexNode<K,T> node = new IndexNode<K,T>(order);
		upperNodeList.add(node);
		while ( !(upperNodeList.size() == 1 && lowerNodeList.isEmpty()) ) {
			//if lower is empty, handle underflow and advance to next level 
			if (lowerNodeList.isEmpty()) {
				if (node.isUnderflowed()) {
					handleIndexUnderflow((IndexNode<K,T>)upperNodeList.get(upperNodeList.size()-2), node);
				}
				
				lowerNodeList = upperNodeList;
				serializationList.addAll(lowerNodeList);
				upperNodeList = new ArrayList<Node<K,T>>();
				node = new IndexNode<K,T>(order);
				upperNodeList.add(node);
			}
					
			//if node is full, should move onto next node
			if (node.isFull()) {
				node = new IndexNode<K,T>(order);
				upperNodeList.add(node);
			}
			
			node.addChild(lowerNodeList.remove(0));
			
		}
		root = upperNodeList.get(0);
		
		serializationList.add(root);
	}
	/**Method that is used to find the number of leaves in the tree
	 * 
	 * @return int numLeaves -- returns the number of leaves
	 */
	public int getNumLeaves() {
		return numLeaves;
	}
	
	/** Helper method to write serialized version to each node to specified
	 * fileOutPath. Creates a BinaryTupleWriter, writes header, then calls
	 * each node's serializeNode method in serialization order.
	 * 
	 * @param fileOutPath -- the String path to where we will write the index.
	 */
	private void serializeTree(String fileOutPath) throws FileNotFoundException {
		BinaryTupleWriter btw = new BinaryTupleWriter(fileOutPath);
		
		int rootAddress = serializationList.size();
		btw.writeIndexHeader(rootAddress, numLeaves, order);
		
		for (int i=0; i < serializationList.size(); i++) {
			Node<K,T> node = serializationList.get(i);
			node.setAddress(i+1);
			node.serializeNode(btw);
		}
	}
	
	/** Helper method to get and sort index data entries from the target relation.
	 *  Uses a binaryTupleReader to generate data entries, then sorts the returned 
	 * entry arraylist by key. Returns the result.
	 * 
	 * @return -- arraylist of entries between integer (attribute value) and arraylist
	 * of record ids (of tuples), sorted by key value
	 */
	private ArrayList<Entry<Integer, ArrayList<RecordID>>> getAndOrganizeIndexData() throws FileNotFoundException {
		DatabaseCatalog dbCatalog = DatabaseCatalog.getInstance();
		String path = dbCatalog.getTablePath(targetRelation);
		Integer targetAttributeIndex = dbCatalog.getTableSchema(targetRelation).indexOf(targetAttribute);
		BinaryTupleReader reader = new BinaryTupleReader(path);
		
		ArrayList<Entry<Integer, ArrayList<RecordID>>> mapArrayList = reader.getIndexDataEntries(targetAttributeIndex);
		mapArrayList.sort(new DataEntryComparator());
		
		return mapArrayList;
	}

    /** Helper method to handle the case when the final leaf node is underflowed.
     * If m is the total # of keys within the second to last leaf and the last leaf, 
     * we keep m/2 keys in the second to last leaf, and the remaining get put into the 
     * last leaf. 
     * 
     * @param secondToLast -- the second to last leaf node.
     * @param last -- the final leaf node that is underflowed.
     */
	private void handleLeafUnderflow(LeafNode<K,T> secondToLast, LeafNode<K,T> last) {
		//calculate the number of values that should stay in secondToLast node
		int amountForLeft = (secondToLast.keys.size() + last.keys.size()) / 2;
		//subtract the above value from number of secondToLast keys to find num to transfer to last
		int numToTransfer = secondToLast.keys.size() - amountForLeft;
		
		int count = 0;
		while (count < numToTransfer) {
			int lastIndex = secondToLast.keys.size()-1;
			K keyToMove = secondToLast.keys.remove(lastIndex);
			T valToMove = secondToLast.values.remove(lastIndex);
			
			last.keys.add(0, keyToMove);
			last.values.add(0, valToMove);
			count++;
		}
	}
	
	/** Helper method to handle the case when the final index node in a level is 
	 * underflowed. If m = the total number of children within the second to last and
	 * the last index node, we keep m/2 children in the second to last node, and place the 
	 * rest in the last index node. 
	 * 
	 * @param secondToLast -- the second to last index node for current level.
	 * @param last -- the last index node (that is underflowed) for current level
	 * 
	 */
	private void handleIndexUnderflow(IndexNode<K,T> secondToLast, IndexNode<K,T> last) {
		//calculate the number of children to keep in secondToLast node
		int amountChildrenToKeep = (secondToLast.children.size() + last.children.size()) / 2;
		//subtract from total children to find number to move
		int numChildrenToMove = secondToLast.children.size() - amountChildrenToKeep;
		
		int count = 0;
		while (count < numChildrenToMove) {
			//remove last key and last child
			int lastChildIndex = secondToLast.children.size()-1;
			secondToLast.keys.remove(lastChildIndex-1);
			Node<K,T> lastChild = secondToLast.children.remove(lastChildIndex);
			last.insertChildInFirstPosition(lastChild);
			count++;
		}
	}
	
	
	/** Custom comparator to sort entries by integer keys.
	 * 
	 */
	private class DataEntryComparator implements Comparator<Map.Entry<Integer, ArrayList<RecordID>>> {
        
        /** Compares 2 entries for sorting purposes. If first entry key < second entry key returns -1.
         * If first entry key > second entry key, returns 1. Else returns 0 (although no 2 keys should
         * be equal, because entries came from a set).
         * 
         * @param o1 -- first entry to compare
         * @param o2 -- second entry to compare
         */
		@Override
		public int compare(Entry<Integer, ArrayList<RecordID>> o1, Entry<Integer, ArrayList<RecordID>> o2) {
			int firstKey = o1.getKey();
			int secondKey = o2.getKey();
			if (firstKey < secondKey) {
				return -1;
			}
			if (firstKey > secondKey) {
				return 1;
			}
			//this case shouldn't happen because these are entries from a set
			return 0;
		}
		
	}

}

