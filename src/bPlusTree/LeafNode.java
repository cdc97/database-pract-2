
package bPlusTree;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import database.BinaryTupleWriter;
import database.Tuple;

/** LeafNode class used to represent leaves in our BPlusTree. Contains a list
 * of keys (inherited from Node) and a corresponding list of values containing
 * data entries.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97) 
 * 
 */
public class LeafNode<K,T> extends Node<K,T> {
	//list of values containing data entries for bplustree
	protected ArrayList<T> values;
	
	/** Constructor for LeafNode. Initializes isLeafNode, keys, values, and order.
	 * Then adds the given first key and first value to the appropriate lists.
	 * 
	 * @param firstKey -- the first key to add to our key list.
	 * @param firstValue -- the first value to add to our value list. Corresponds to
	 *                  first key value.
	 * @param order -- the order of the bplustree this leafnode is part of.
	 */
	public LeafNode(K firstKey, T firstValue, int order) {
		isLeafNode = true;
		keys = new ArrayList<K>();
		values = new ArrayList<T>();
		keys.add(firstKey);
		values.add(firstValue);
		this.order = order;
	}
	
	/** Constructor for LeafNode. Initializes isLeafNode, keys, values, and order.
	 * 
	 * @param order -- the order of the bplustree this leafnode is part of.
	 */
	public LeafNode(int order) {
		isLeafNode = true;
		keys = new ArrayList<K>();
		values = new ArrayList<T>();
		this.order = order;
	}
	
	/** Given an entry, adds the key to the key list,
	 * and the value to the value list.
	 * 
	 * @param entry -- the data entry to add to this leaf. 
	 * ASSUMING entries are added in sorted order (least to greatest)
	 */
	public void addEntry(Map.Entry<K, T> entry) {
		keys.add(entry.getKey());
		values.add(entry.getValue());
		
	}
    
    /** Calls the correct serialization method in binary tuple writer
     * in order to serialize this leaf node.
     * 
     * @param btw -- the binary tuple writer to use for serialization.
     */
	@Override
	public void serializeNode(BinaryTupleWriter btw) {
		btw.writeLeafNode((ArrayList<Integer>) keys, (ArrayList<ArrayList<RecordID>>) values);
		
	}

}
