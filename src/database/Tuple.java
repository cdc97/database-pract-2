
package database;

import java.util.Arrays;

/** Object used to store and manipulate tuple data.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class Tuple {
	//integer array containing actual tuple data
	private Integer[] data;
	private TupleReader t;
	
	/**Constructs a tuple with an array of data values
	 * 
	 * @param data -- tuple of numbers
	 */
	public Tuple(Integer[] data) {
		this.data = data;
	}
	
	/** Constructs a tuple from a string line taken from a table's 
	 * data file.
	 * 
	 * @param dataLine -- a string containing a line of comma-separated-values
	 * 					to be stored in the tuple data array
	 */
	public Tuple(String dataLine) {
		String[] dataStrings = dataLine.split(",");
		int length = dataStrings.length;
		Integer[] data = new Integer[length];
		
		for (int i = 0; i < length ; i++){
			data[i] = Integer.parseInt(dataStrings[i]);
		}
		
		this.data = data;
	}

	/** Constructor for Tuple that takes an old tuple and an array of indices.
	 * Creates a new tuple containing only the data from the old tuple 
	 * located at the indices specified in the indices array.
	 * Used for projection operator.
	 * 
	 * @param oldTuple -- old tuple containing data to use to construct new tuple
	 * @param indices -- array of indices where desired data for new tuple is located
	 */
	public Tuple(Tuple oldTuple, Integer[] indices){
		data = new Integer[indices.length];
		int i = 0;
		for (Integer index : indices){
			data[i] = oldTuple.getElement(index);
			i++;
		}
	}
	
	/** Constructor for Tuple that takes two tuples and combines them.
	 *  Used for cross product.
	 * @param leftHalf -- first (left) half for tuple combination
	 * @param rightHalf -- second (right) half for tuple combination
	 */
	public Tuple(Tuple leftHalf, Tuple rightHalf) {
		int leftLength = leftHalf.getLength();
		int rightLength = rightHalf.getLength();
		data = new Integer[leftLength + rightLength];
		System.arraycopy(leftHalf.getDataArray(), 0, data, 0, leftLength);
		System.arraycopy(rightHalf.getDataArray(), 0, data, leftLength, rightLength);
	}
	
	/** Returns integer data located at index specified.
	 * 
	 * @param index -- index of data to return
	 * @return integer value located at index position in tuple data
	 */
	public Integer getElement(Integer index){
		return this.data[index];
	}
	
	/** Returns entire tuple data array
	 * 
	 * @return array of integer representing tuple data
	 */
	public Integer[] getDataArray(){
		return this.data;
	}
	
	/** Returns length of data array
	 * 
	 * @return integer length of data array
	 */
	public int getLength() {
		return data.length;
	}
	
	/** Used to pretty print tuple data in csv form
	 * @return string representing tuple data as csv
	 */
	public String toString() {
		String result = "";
		for (int i = 0; i < (data.length-1); i++){
			result = result + data[i] + ",";
		}
		result=result + data[data.length-1];
		return result;
	}
	
	/** Returns whether two tuples are equal. To be equal, every column value must
	 * be equivalent. Assumed that tuples have same schema.
	 * @return boolean value to specify whether tuples are equal
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Tuple)) {
			return false;
		}
		Tuple tupleToCompare = (Tuple) obj;
		for (int i = 0; i < data.length ; i++) {
			if (!(data[i].equals(tupleToCompare.getElement(i)))) {
				return false;
			}
		}
		return true;
	}
	
	/** Returns a hashcode to hash equivalent tuples in same location.
	 * @return hashcode to specify where tuples get placed in a hashset object.
	 * 			Necessary to override for HashDuplicateEliminationOperator.
	 */
	@Override
	public int hashCode() {
		int prime = 17;
		int result = 1;
		for (Integer dataInt : data) {
			result= prime * result + dataInt;
		}
		return result;
	}
	/**
	 * The file handle where this tuple came from
	 * @param t TupleReader
	 */
	public void setTupleReader (TupleReader t)
	{
		this.t = t;
	}
	
	/**
	 * returns the handle to the file where the tuple was read from
	 * @return TupleReader
	 */
	public TupleReader getTupleReader () {
		return t;
	}
	
}
