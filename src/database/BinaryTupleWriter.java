
package database;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;

import bPlusTree.RecordID;

/**
 * Implementation of the TupleWriter interface used to handle writing
 * output tuples to a file, in the specified binary format from the instructions.
 * Also handles serializing BPlusTree nodes into an index file.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class BinaryTupleWriter implements TupleWriter {
	//query plan object who's output we are writing to file
	private QueryPlan qp;
	//file channel object used to write to file
	private FileChannel fc;
	//buffer used to store values to write to file
	private ByteBuffer buffer;
	//number of attributes in each output tuple
	private int numAttributes;
	
	/** Constructor for BinaryTupleWriter. Initializes all class
	 * variables.
	 * 
	 * @param qp -- query plan who's output we are writing to file
	 * @param filePath -- output file path
	 * @throws FileNotFoundException
	 */
	public BinaryTupleWriter(QueryPlan qp, String filePath) throws FileNotFoundException {
		this.qp = qp;
		FileOutputStream fout = new FileOutputStream(filePath);
		fc = fout.getChannel();
		buffer = ByteBuffer.allocate(4096);
		
		numAttributes = qp.getRootSchema().size();
	}
	
	/**Constructor for BinaryTupleWriter. Used for BPlusTree serialization or clustering
	 * a relation.
	 * Initializes the necessary variables for writing an index or clustering 
	 * a relation.
	 * 
	 * @param filePath -- the output file path. 
	 * @throws FileNotFoundException
	 */
	public BinaryTupleWriter(String filePath) throws FileNotFoundException {
		FileOutputStream fout = new FileOutputStream(filePath);
		fc = fout.getChannel();
		buffer = ByteBuffer.allocate(4096);
	}

	/** Writes all tuples from the operator's output to the given target
	 * file. Uses the same logic as dump() below, but dumps the output of an
	 * operator rather than a query plan. This method is used to clustering
	 * a relation.
	 * 
	 * @param op -- the operator with output we must write to file.
	 */
	public void dumpOperator(Operator op) {
		numAttributes = op.getSchema().size();
		Tuple tuple = op.getNextTuple();
		Integer[] tupleData;		
		
		while (tuple != null) {
			
			buffer.putInt(numAttributes);
			//place holder until we've counted tuples inserted into buffer
			buffer.putInt(0);
			
			//max space remaining for whole tuples in page
			int maximumTuplesRemaining = (buffer.capacity() - 8)/4/numAttributes;
			int tupleCount = 0;
			while (maximumTuplesRemaining != 0 && tuple != null) {
				tupleData = tuple.getDataArray();

				for (Integer element : tupleData) {
					buffer.putInt(element);
				}
				
				maximumTuplesRemaining--;
				tuple = op.getNextTuple();
				tupleCount++;
				
			}
			
			populateZeros();
			//update tupleCount
			buffer.putInt(4, tupleCount);
			
			//now write buffer to file
			try {
				buffer.flip();
				fc.write(buffer);
			} catch (IOException e) {
				e.printStackTrace();
			}
			buffer.clear();
			
		}
		
		close();
	}
	
	/** Fills the buffer with 0's from current position to the end.
	 */
	private void populateZeros() {
		while (buffer.position() < buffer.capacity()) {
			buffer.putInt(0);
		}
	}

	/** Closes the file channel object used to write.
	 * 
	 */
	@Override
	public void close() {
		try {
			fc.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/** Writes all output tuples to output file.
	 * While there exists an unprocessed output tuple,
	 * and the buffer isn't full (relative to max tuples that can fit),
	 * we place tuple ints into buffer. Then we populate any remaining space with 0's
	 * and update the tuplecount meta data. Then write the buffer to the file,
	 * clear the buffer and begin processing more tuples (until everything is processed).
	 * 
	 */
	@Override
	public void dump() {
			
		Tuple tuple = qp.getNextResultTuple();
		Integer[] tupleData;		
		
		while (tuple != null) {
			
			buffer.putInt(numAttributes);
			//place holder until we've counted tuples inserted into buffer
			buffer.putInt(0);
			
			//max space remaining for whole tuples in page
			int maximumTuplesRemaining = (buffer.capacity() - 8)/4/numAttributes;
			int tupleCount = 0;
			while (maximumTuplesRemaining != 0 && tuple != null) {
				tupleData = tuple.getDataArray();

				for (Integer element : tupleData) {
					buffer.putInt(element);
				}
				
				maximumTuplesRemaining--;
				tuple = qp.getNextResultTuple();
				tupleCount++;
				
			}
			
			populateZeros();
			//update tupleCount
			buffer.putInt(4, tupleCount);
			
			//now write buffer to file
			try {
				buffer.flip();
				fc.write(buffer);
			} catch (IOException e) {
				e.printStackTrace();
			}
			buffer.clear();
			
		}
		
		close();
		
	}
	
	/** Used to serialize a leaf node, given a list of keys and a list of values
	 * (values of lists of record id objects). Writes the leaf node information to the
	 * target output file in the format described in the instructions.
	 * 
	 * @param keys -- list of all target attribute values contained in key
	 * 				 list of leaf node being serialized
	 * @param values -- the list of lists of record IDs corresponding to the key values.
	 */
	public void writeLeafNode(ArrayList<Integer> keys, ArrayList<ArrayList<RecordID>> values) {
		buffer.putInt(0);
		buffer.putInt(keys.size());
		
		for (int i=0; i < keys.size(); i++) {
			Integer key = keys.get(i);
			ArrayList<RecordID> value = values.get(i);
			
			buffer.putInt(key);
			buffer.putInt(value.size());
			
			for (RecordID rid : value) {
				buffer.putInt(rid.getPageId());
				buffer.putInt(rid.getTupleId());
			}
		}
		
		writeBufferToFile();
	}
	
	/** Used to serialize an index node, given a list of keys and a children addresses. 
	 * Writes the index node information to the
	 * target output file in the format described in the instructions.
	 * 
	 * @param keys -- the keys contained in the index node we are serializing.
	 * @param childrenAddresses -- the serialization addresses of all child nodes of this 
	 * 								index node
	 */
	public void writeIndexNode(ArrayList<Integer> keys, ArrayList<Integer> childrenAddresses) {
		buffer.putInt(1);
		buffer.putInt(keys.size());
		
		for (Integer key : keys) {
			buffer.putInt(key);
		}
		
		for (Integer address: childrenAddresses) {
			buffer.putInt(address);
		}
		
		writeBufferToFile();
	}

	/** Used to write the header page for an index.
	 * 
	 * @param rootAddress -- the serialization address of the root node.
	 * @param numLeaves -- the number of leaves contained in the BPlusTree being serialized
	 * @param order -- the order of the BPlusTree being serialized
	 */
	public void writeIndexHeader(int rootAddress, int numLeaves, int order) {
		buffer.putInt(rootAddress);
		buffer.putInt(numLeaves);
		buffer.putInt(order);
		
		writeBufferToFile();
	}
	
	/** Populates the remaining spaces in the buffer with zeroes, then
	 * writes the buffer to the target output file.
	 */
	private void writeBufferToFile() {
		populateZeros();
		
		//now write buffer to file
		try {
			buffer.flip();
			fc.write(buffer);
		} catch (IOException e) {
			e.printStackTrace();
		}
		buffer.clear();
	}
}

