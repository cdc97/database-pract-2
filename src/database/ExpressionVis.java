
package database;

import java.util.ArrayList;
import java.util.Stack;

import net.sf.jsqlparser.expression.*;
import net.sf.jsqlparser.expression.operators.arithmetic.Addition;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseAnd;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseOr;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseXor;
import net.sf.jsqlparser.expression.operators.arithmetic.Concat;
import net.sf.jsqlparser.expression.operators.arithmetic.Division;
import net.sf.jsqlparser.expression.operators.arithmetic.Multiplication;
import net.sf.jsqlparser.expression.operators.arithmetic.Subtraction;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.Between;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.ExistsExpression;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.expression.operators.relational.LikeExpression;
import net.sf.jsqlparser.expression.operators.relational.Matches;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.SubSelect;

/**
 * Class used to evaluate expressions. Implements ExpressionVisitor interface
 * from Jsqlparser package.
 * 
 * Recurses and processes an expressions subexpressions until it reaches a leaf
 * expression (Column expression or LongValue expression). It places operands on
 * operands stack and evaluates corresponding subexpressions as it pops recursive calls. 
 * Takes result variable and "ANDS" it with the result of the subexpression evaluation.
 * Does this until all recursive calls have been popped.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class ExpressionVis implements ExpressionVisitor {
	//contains the tuple in question (to see if tuple satisfies expression)
	private Tuple tuple;
	//the schema of the table corresponding to the tuples we are checking
	private ArrayList<String> schema;
	//the result boolean--true if tuple satisfies expression. False, otherwise.
	private boolean result;
	//Used to hold operands from recursive subcalls until processed
	Stack<Integer> operands;
	
	/**Constructor for ExpressionVis. Sets schema, initializes result as true,
	 * and sets up a fresh stack for operands. 
	 * @param schema -- the schema corresponding to the table from which the 
	 * 					tuples we are checking belong
	 */
	public ExpressionVis(ArrayList<String> schema) {
		this.schema = schema;
		this.result = true;
		this.operands = new Stack<Integer>();
	}
	
	/** Sets tuple variable to new tuple.
	 * Resets result to true.
	 * Clears operands stack.
	 * @param tuple -- the new tuple to check whether satisfies expression.
	 */
	public void resetForNextTuple(Tuple tuple) {
		this.tuple = tuple;
		this.result = true;
		this.operands.clear();
	}
	
	/** Return result, whether tuple satisfies expression.
	 * 
	 * @return boolean denoting whether a tuple satisfies an expression.
	 */
	public boolean getResult() {
		return result;
	}
	
	@Override
	public void visit(NullValue arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Function arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(InverseExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(JdbcParameter arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(DoubleValue arg0) {
		// TODO Auto-generated method stub
		
	}

	/** Interface method. Visit for LongValue expression.
	 *  Gets number from expression, converts it to an int, 
	 *  and places it on the operands stack.
	 */
	@Override
	public void visit(LongValue arg0) {
		Integer value = (int) (arg0.getValue());
		operands.push(value);
	}

	@Override
	public void visit(DateValue arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(TimeValue arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(TimestampValue arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Parenthesis arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(StringValue arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Addition arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Division arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Multiplication arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Subtraction arg0) {
		// TODO Auto-generated method stub
		
	}

	/** Interface method. Visit for AndExpression.
	 *  Gets left and right subexpressions and calls
	 *  accept for both subexpressions. 
	 *  (Essentially recursive call through ExpressionVis).
	 */
	@Override
	public void visit(AndExpression arg0) {
		Expression left = arg0.getLeftExpression();
		Expression right = arg0.getRightExpression();
		
		left.accept(this);
		right.accept(this);
		
	}

	@Override
	public void visit(OrExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Between arg0) {
		// TODO Auto-generated method stub
		
	}

	/** Interface method. Visit for EqualsTo Expression.
	 *  Gets left and right subexpressions, calls accept for
	 *  each. Then updates result to reflect whether 
	 *  the left subexpression is equal to the right subexpression.
	 */
	@Override
	public void visit(EqualsTo arg0) {
		Expression left = arg0.getLeftExpression();
		Expression right = arg0.getRightExpression();
		left.accept(this);
		right.accept(this);
		
		result = result && (operands.pop().intValue() == operands.pop().intValue());
	}

	/** Interface method. Visit for GreaterThan Expression.
	 *  Gets left and right subexpressions, calls accept for each.
	 *  Then updates result to reflect whether the left subexpression
	 *  is greater than the right subexpression.
	 */
	@Override
	public void visit(GreaterThan arg0) {
		Expression left = arg0.getLeftExpression();
		Expression right = arg0.getRightExpression();
		left.accept(this);
		right.accept(this);
		
		result = result && (operands.pop().intValue() < operands.pop().intValue());
		
	}

	/** Interface method. Visit for GreaterThanEquals Expression.
	 *  Gets left and right subexpressions, calls accept for each.
	 *  Then updates result to reflect whether the left subexpression
	 *  is greater than or equal to the right subexpression.
	 */
	@Override
	public void visit(GreaterThanEquals arg0) {
		Expression left = arg0.getLeftExpression();
		Expression right = arg0.getRightExpression();
		left.accept(this);
		right.accept(this);
		
		result = result && (operands.pop().intValue() <= operands.pop().intValue());
		
	}

	@Override
	public void visit(InExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IsNullExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(LikeExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	/** Interface method. Visit for MinorThan Expression.
	 *  Gets left and right subexpressions, calls accept for each.
	 *  Then updates result to reflect whether the left subexpression
	 *  is less than the right subexpression.
	 */
	@Override
	public void visit(MinorThan arg0) {
		Expression left = arg0.getLeftExpression();
		Expression right = arg0.getRightExpression();
		left.accept(this);
		right.accept(this);
		
		result = result && (operands.pop().intValue() > operands.pop().intValue());
		
	}

	/** Interface method. Visit for MinorThanEquals Expression.
	 *  Gets left and right subexpressions, calls accept for each.
	 *  Then updates result to reflect whether the left subexpression
	 *  is less than or equal to the right subexpression.
	 */
	@Override
	public void visit(MinorThanEquals arg0) {
		Expression left = arg0.getLeftExpression();
		Expression right = arg0.getRightExpression();
		left.accept(this);
		right.accept(this);
		
		result = result && (operands.pop().intValue() >= operands.pop().intValue());
		
	}

	/** Interface method. Visit for NotEqualsTo Expression.
	 *  Gets left and right subexpressions, calls accept for each.
	 *  Then updates result to reflect whether the left subexpression
	 *  is not equal to the right subexpression.
	 */
	@Override
	public void visit(NotEqualsTo arg0) {
		Expression left = arg0.getLeftExpression();
		Expression right = arg0.getRightExpression();
		left.accept(this);
		right.accept(this);
		
		result = result && (operands.pop().intValue() != operands.pop().intValue());
		
	}

	/** Interface method. Visit for Column Expression.
	 *  Gets column name, finds index for column name in schema,
	 *  then pushes corresponding element from tuple to operands stack.
	 */
	@Override
	public void visit(Column arg0) {
		String columnName = arg0.getWholeColumnName();
		int index = schema.indexOf(columnName);
		operands.push(tuple.getElement(index));
		
	}

	@Override
	public void visit(SubSelect arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(CaseExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(WhenClause arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ExistsExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(AllComparisonExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(AnyComparisonExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Concat arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Matches arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(BitwiseAnd arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(BitwiseOr arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(BitwiseXor arg0) {
		// TODO Auto-generated method stub
		
	}

}

