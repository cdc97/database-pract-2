
package database;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import bPlusTree.BPlusTree;
import bPlusTree.RecordID;
import bPlusTree.IndexInfo;
import net.sf.jsqlparser.parser.CCJSqlParser;
import net.sf.jsqlparser.parser.ParseException;
import net.sf.jsqlparser.statement.Statement;
import optimizer.StatCollector;
import database.DatabaseCatalog;

/**
 * Interpreter class reads in input data and schema and Creates a QueryPlan
 * that is the build operator tree of all the operators. It then performs the 
 * runQueryPlan method in order to maneuver through the tree and output 
 * the correct values into an output file. 
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */

public class Interpreter {

	/**
	 * Main method that executes building the operator tree and execution of the tree.
	 * Reads input and schema and build a Database Catalog in order to handle it.
	 * 
	 * @param args type: String [] Contains at the 0th location the input directory
	 * @throws IOException 
	 *
	 */
	public static void main(String[] args) throws ParseException, IOException {
		ArrayList<String> configLines = getConfigArgs(args[0]);
		
		DatabaseCatalog ct = DatabaseCatalog.getInstance();
		ct.setInputDir(configLines.get(0));
		ct.setOutputDir(configLines.get(1));
		ct.setTempDir(configLines.get(2));
		
		//Collect stats on tables
		StatCollector s = new StatCollector();
		s.gatherTableandColumnStats();
		DatabaseCatalog.getInstance().initStatInfo(s.getMap());
		
		String indexInfoPath = configLines.get(0) + "/db/index_info.txt";
		ArrayList<String[]> indexInfo = getIndexInfo(indexInfoPath);

		buildIndexes(indexInfo, configLines.get(0) + "/db/indexes");
		
		String queriesPath = configLines.get(0) + "/queries.sql";
		runQueries(queriesPath, configLines.get(1));
		
	}
	
	/** Takes the information from indexInfo to generate the specified
	 * indexes.
	 * 
	 * @param indexInfo -- ArrayList containing broken up lines (string[]) from 
	 * 						index_info file.
	 * @param indexDirPath -- the path to the index directory within the input file.
	 * @throws IOException
	 */
	private static void buildIndexes(ArrayList<String[]> indexInfo, String indexDirPath) throws IOException {

		for(String[] indexSpec : indexInfo) {
			String relation = indexSpec[0];
			
			for (int i = 1; i < indexSpec.length; i=i+3 ) {
				String targetAttribute = indexSpec[i];
				boolean clustered = indexSpec[i+1].equals("1");
				int order = Integer.parseInt(indexSpec[i+2]);
				
				String labeledAttribute = relation + "." + targetAttribute;
				
				String indexOutPath = indexDirPath + "/" + labeledAttribute;
				
				BPlusTree<Integer, ArrayList<RecordID>> b = new BPlusTree<Integer, ArrayList<RecordID>>(clustered, relation, labeledAttribute, order, indexOutPath);
				DatabaseCatalog.getInstance().addIndexInfo(relation, labeledAttribute, clustered, order, b.getNumLeaves());
			}
		
		}
		
	}
	
	/** Reads the index info from the index_info file.
	 * Splits each line by spaces (creating string[])
	 *  and places the result in an arraylist. 
	 *  Returns the arraylist.
	 * 
	 * @param indexInfoPath -- the path to the index_info file
	 * @return -- an arraylist of string[] containing split lines from the index_info file
	 * @throws IOException
	 */
	private static ArrayList<String[]> getIndexInfo(String indexInfoPath) throws IOException {
		ArrayList<String[]> indexInfo = new ArrayList<String[]>();
		
		BufferedReader br = new BufferedReader(new FileReader(indexInfoPath));
		String line;
		while ((line = br.readLine()) != null) {
			String[] splitLine = line.split(" ");
			indexInfo.add(splitLine);
			
			String indexName = splitLine[0] + "." + splitLine[1];
			Boolean isClustered = splitLine[2].equals("1");
			
			DatabaseCatalog.getInstance().addAvailableIndex(indexName, isClustered);
			
			for (int i = 4; i < splitLine.length; i=i+3) {
				indexName = splitLine[0] + "." + splitLine[i];
				isClustered = false;
				DatabaseCatalog.getInstance().addAvailableIndex(indexName, isClustered);
			}
		}
		br.close();
		
		return indexInfo;
	}
	
	/** Runs the queries specified in the queries file.
	 * 
	 * @param queriesPath -- path to the queries file.
	 * @param outputPath -- the path to the output directory
	 * @throws FileNotFoundException
	 * @throws ParseException
	 */
	private static void runQueries(String queriesPath, String outputPath) throws FileNotFoundException, ParseException {
		CCJSqlParser parser = new CCJSqlParser(new FileReader(queriesPath));
		Statement statement;
		int counter = 1;
		QueryPlan qp;
		while ( (statement = parser.Statement()) != null){
			String file = outputPath + "/query" + counter;
			File logicalPlanFile = new File(outputPath + File.separator + "query" + counter + "_logicalplan");
			File physicalPlanFile = new File(outputPath + File.separator + "query" + counter + "_physicalplan");
			PrintStream logicalPlanStream = new PrintStream(logicalPlanFile);
			PrintStream physicalPlanStream = new PrintStream(physicalPlanFile);
			

	        qp = new QueryPlan();
		    try {
		    	//long start = System.currentTimeMillis();
		    	long start = System.nanoTime();
		    	qp.buildQueryPlan(statement);
		    	
		    	//print the logical plan
		    	qp.getLogicRootOperator().printTree(System.out, 0);
		    	qp.getLogicRootOperator().printTree(logicalPlanStream, 0);
		    	logicalPlanStream.close();
		    	
		    	System.out.println("_______");
		    	
		    	qp.getPhysicalRootOperator().printTree(System.out, 0);
		    	qp.getPhysicalRootOperator().printTree(physicalPlanStream, 0);
		    	physicalPlanStream.close();
		    	
		    	//TupleWriter tw = new HumanTupleWriter(qp, file);
		    	TupleWriter tw = new BinaryTupleWriter(qp, file);
				writeQueryResultsToFile(qp, tw);	
				
				//long end = System.currentTimeMillis();
		    	long end = System.nanoTime();
				System.out.println("Time Spent: " + (end-start)/1e6 + " ms");
				
//		    	SortTool st = new SortTool(qp);
//		    	st.dumpTuplesToOutput(file);
				
			} catch (Exception e) {
				e.printStackTrace();
			}

		    counter++;
			
		}
	}
	
	/**
	 * Outputs data into a text file for every individual query. 
	 * 
	 * @param qp type: QueryPlan Contains operator tree that ran the method and contains its tuples
	 * @param bw type: TupleWriter that the tuples will be output to. 
	 *
	 */
	private static void writeQueryResultsToFile(QueryPlan qp, TupleWriter tw) {
		tw.dump();
		tw.close();
	}
	
	/** Reads the config file and adds each line to an
	 * arraylist. Returns the arraylist.
	 * 
	 * @param configFilePath -- path to the config file.
	 * @return -- arraylist containing lines from config file.
	 * @throws IOException
	 */
	private static ArrayList<String> getConfigArgs(String configFilePath) throws IOException {
		ArrayList<String> args = new ArrayList<String>();
		
		BufferedReader br = new BufferedReader(new FileReader(configFilePath));
		String line;
		while ((line = br.readLine()) != null) {
			args.add(line);
		}
		br.close();
		return args;
	}
	
	
	
}

