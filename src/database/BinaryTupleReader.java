package database;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bPlusTree.RecordID;

/**
 * Implementation of the TupleReader interface used to handle reading
 * tuples from a file with the specified binary format from the instructions.
 * Also handles generating entries of index creation.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class BinaryTupleReader implements TupleReader {
	//the path to the target data file
	private String filePath;
	//a filechannel object that can be used to read from the target data file
	private FileChannel fc;
	//the buffer object used to collect ints read from the target data file
	private ByteBuffer buffer;
	//number of attributes in each tuple
	private int numAttributes;
	//number of tuples on the current page (in buffer)
	private int numTuplesOnPage;
	//the current number of tuples we've processed
	private int tupleCount;
	//boolean used to determine whether we've finished reading data file
	private boolean doneReading;
	//the current page of the binary file we're reading
	private int currentPage;
	
	/** Constructor for BinaryTupleReader. Initializes filepath, file channel (fc),
	 * the buffer, then calls readNextPage() to fill the buffer with some initial
	 * values
	 * 
	 * @param filePath -- file path to the target data file
	 * @throws FileNotFoundException
	 */
	public BinaryTupleReader(String filePath) throws FileNotFoundException {
		this.filePath = filePath;
		FileInputStream fin = new FileInputStream(filePath);
		fc = fin.getChannel();
		
		buffer = ByteBuffer.allocate(4096);
		
		readNextPage();
		currentPage = 0;
	}
	
	/** Constructor for BinaryTupleReader used when we use index data. We pass in 
	 * the first RecordID from the index so we can set the binary reader to the appropriate
	 * page and tuple. That way, when readNextTuple() is called, we return the tuple corresponding
	 * to the passed in RecordID.
	 * 
	 * @param filePath  -- file path to the target data file
	 * @param initialRecord -- the Record ID of the first tuple we want to return
	 * @throws FileNotFoundException
	 */
	public BinaryTupleReader(String filePath, RecordID initialRecord) throws FileNotFoundException {
		this.filePath = filePath;
		FileInputStream fin = new FileInputStream(filePath);
		fc = fin.getChannel();
		
		buffer = ByteBuffer.allocate(4096);
		
		int initialPage = initialRecord.getPageId();
		int initialTuple = initialRecord.getTupleId();
		readTargetPage(initialPage);
		setPositionForTargetTuple(initialTuple);
	}
	
	/** Used to load the next page from the file into the buffer.
	 * Sets the numAttributes, numTuplesOnPage, tupleCount, and
	 * doneReading variables. If loading the page is unsuccessful,
	 * sets doneReading to true.
	 * 
	 */
	private void readNextPage() {
		buffer.clear();
		int result = -1;
		try {
			result = fc.read(buffer);
			currentPage++;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (result != -1) {
			buffer.flip();
			
			numAttributes = buffer.getInt();
			numTuplesOnPage = buffer.getInt();
			tupleCount = 0;
			doneReading = false;
		}
		else{
			doneReading = true;
		}
			
	}
	
	/** Loads a target page into the buffer. Changes filechannel position
	 * and then fills buffer.
	 * 
	 * @param pageIndex -- the index of the target page.
	 */
	private void readTargetPage(int pageIndex) {
		buffer.clear();
		currentPage = pageIndex;
		long targetPosition = pageIndex*4096;
		
		try {
			fc.position(targetPosition);
			fc.read(buffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		buffer.flip();
		numAttributes = buffer.getInt();
		numTuplesOnPage = buffer.getInt();
		doneReading = false;
	}
	
	/** Adjusts the buffers position within the current page,
	 * so that the next tuple returned by readNextTuple() is the
	 * tuple specified by tupleIndex.
	 * 
	 * @param tupleIndex -- the index of the next tuple to return.
	 */
	private void setPositionForTargetTuple(int tupleIndex) {
		tupleCount = tupleIndex;
		int targetPosition = (2 + tupleIndex*numAttributes) * 4;
		buffer.position(targetPosition);
	}
	
	/** Method used to grab next tuple from file. If buffer hasn't been
	 * completely processed, generates tuple from buffer. Otherwise, calls 
	 * readNextPage before generating a tuple from buffer. If reading failed,
	 * returns null.
	 *  
	 *  @return --returns the next Tuple that we haven't processed in buffer
	 */
	@Override
	public Tuple readNextTuple() {
		if (tupleCount == numTuplesOnPage) {
			readNextPage();
		}
		
		if (doneReading) {
			return null;
		}
		
		Integer[] data = new Integer[numAttributes];
		int attributeCount = 0;
		while (attributeCount < numAttributes) {
			data[attributeCount] = buffer.getInt();
			attributeCount++;
		}
		tupleCount++;
		return new Tuple(data);
	}

	/** Used to position the reader so that the next tuple returned is the tuple
	 * specified by the given RecordID. Changes pages if necessary, then calls the
	 * setPositionForTargetTuple() helper method.
	 * 
	 * @param rid -- the RecordID of the next tuple we want to return
	 */
	public void positionForNextTuple(RecordID rid) {
		int pageId = rid.getPageId();
		int tupleId = rid.getTupleId();
		
		if (currentPage != pageId) {
			readTargetPage(pageId);
		}
		setPositionForTargetTuple(tupleId);
		
	}
	
	/** Closes filechannel object used to read.
	 * 
	 */
	@Override
	public void close() {
		try {
			fc.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/** Closes file channel object, then creates a new one. 
	 * Causes the next tupleRead to be from the beginning of the file.
	 * 
	 */
	@Override
	public void reset() {
		FileInputStream fin;
		try {
			fc.close();
			fin = new FileInputStream(filePath);
			fc = fin.getChannel();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buffer = ByteBuffer.allocate(4096);
		readNextPage();
	}

	/** Resets readers tuple output to the tuple specified by int index.
	 * @param index -- the index to reset tuple output to.
	 * Currently unimplemented.
	 * May add implementation if we choose to optimize SMJ
	 */
	@Override
	public void reset(int index) {
		//unimplemented.
		
	}

	/** Generates index data entries from our relation. Uses the target attribute
	 * value of each tuple as its key, and a list of Record ID objects of the tuples' page and tuple
	 * indices as the values. Processes the entire relation and then returns an arraylist
	 * of these entries.
	 * 
	 * @param attributeIndex -- the index of the target attribute within each tuple
	 * @return -- an arraylist of entries mapping from attribute value (as key) to a list
	 * 			of Record Ids of all tuples that have the given attribute value (as value)
	 */
	public ArrayList<Map.Entry<Integer, ArrayList<RecordID>>> getIndexDataEntries(int attributeIndex) {
		HashMap<Integer, ArrayList<RecordID>> indexMap = new HashMap<Integer, ArrayList<RecordID>>();
		int pageIndex = 0;
		//fast forward buffer position to first attribute location
		buffer.position(buffer.position() + attributeIndex * 4);
		while(!doneReading) {
			int key = buffer.getInt();
			ArrayList<RecordID> value;
			if (indexMap.containsKey(key)) {
				value = indexMap.get(key);
			}
			else {
				value = new ArrayList<RecordID>();
				indexMap.put(key, value);
			}
			
			RecordID recordToAdd = new RecordID(pageIndex, tupleCount);
			value.add(recordToAdd);
			tupleCount++;
			
			//fastforward to same attribute in next tuple
			buffer.position(buffer.position() + (numAttributes-1) * 4);
			
			if (tupleCount == numTuplesOnPage) {
				readNextPage();	
				buffer.position(buffer.position() + attributeIndex * 4);
				pageIndex++;
			}
		}
		
		return new ArrayList<Map.Entry<Integer, ArrayList<RecordID>>>(indexMap.entrySet());
	}
}


