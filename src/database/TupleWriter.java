
package database;

/**
 * Interface used to generalize all TupleWriters. Used to write all output tuples
 * to a file.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public interface TupleWriter {
	
	/** Writes all output tuples to the target file.
	 * 
	 */
	public void dump();
	
	/** Closes any writing resources used.
	 * 
	 */
	public void close();
	
}
