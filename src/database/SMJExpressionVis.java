package database;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Stack;

import net.sf.jsqlparser.expression.AllComparisonExpression;
import net.sf.jsqlparser.expression.AnyComparisonExpression;
import net.sf.jsqlparser.expression.CaseExpression;
import net.sf.jsqlparser.expression.DateValue;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.expression.InverseExpression;
import net.sf.jsqlparser.expression.JdbcParameter;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.NullValue;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.TimeValue;
import net.sf.jsqlparser.expression.TimestampValue;
import net.sf.jsqlparser.expression.WhenClause;
import net.sf.jsqlparser.expression.operators.arithmetic.Addition;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseAnd;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseOr;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseXor;
import net.sf.jsqlparser.expression.operators.arithmetic.Concat;
import net.sf.jsqlparser.expression.operators.arithmetic.Division;
import net.sf.jsqlparser.expression.operators.arithmetic.Multiplication;
import net.sf.jsqlparser.expression.operators.arithmetic.Subtraction;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.Between;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.ExistsExpression;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.expression.operators.relational.LikeExpression;
import net.sf.jsqlparser.expression.operators.relational.Matches;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.SubSelect;

/**
 * Class used to evaluate expressions. Implements ExpressionVisitor interface
 * from Jsqlparser package.
 * 
 * Processes expressions to extract equalities for SMJ.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class SMJExpressionVis implements ExpressionVisitor {
	//list of left columns. left column = right column at same index.
	ArrayList<String> leftColumns;
	//list of right columns. left column = right column at same index.
	ArrayList<String> rightColumns; 
	//schema for left relation
	ArrayList<String> leftSchema;
	//schema for right relation
	ArrayList<String> rightSchema;
	
	/**Constructor for SMJExpressionVis. Sets schema, initializes result as true,
	 * and sets up a fresh stack for operands. 
	 * @param schema -- the schema corresponding to the table from which the 
	 * 					tuples we are checking belong
	 */
	public SMJExpressionVis(ArrayList<String> leftSchema, ArrayList<String> rightSchema) {
		leftColumns = new ArrayList<String>();
		
		rightColumns = new ArrayList<String>();
		
		this.leftSchema = leftSchema;
		
		this.rightSchema = rightSchema;
		
	}
	
	/** Returns the list of left columns.
	 * left columns match up with right columns
	 * such that columns at the same index must be equal in a tuple.
	 * 
	 * @return -- list of left columns.
	 */
	public ArrayList<String> getLeftColumns() {
		return leftColumns;
	}
	
	/** Returns the list of right columns.
	 * right columns match up with left columns
	 * such that columns at the same index must be equal in a tuple.
	 * 
	 * @return -- list of right columns.
	 */
	public ArrayList<String> getRightColumns() {
		return rightColumns;
	}
	
	/** Given a column list, generates a unique order by 
	 * list (no repeated columns!), so we can sort the target
	 * relations accordingly.
	 * 
	 * @param columnList -- list of column names.
	 * @return -- arraylist of column names with duplicates removed
	 */
	public ArrayList<String> generateOrderByList(ArrayList<String> columnList) {
		ArrayList<String> orderByList = new ArrayList<String>();
		HashSet<String> hashSet = new HashSet<String>();
		
		for (String column : columnList) {
			if (!hashSet.contains(column)){
				orderByList.add(column);
				hashSet.add(column);
			}
		}
		
		return orderByList;
	}
	
	@Override
	public void visit(NullValue arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Function arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(InverseExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(JdbcParameter arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(DoubleValue arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(LongValue arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(DateValue arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(TimeValue arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(TimestampValue arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Parenthesis arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(StringValue arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Addition arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Division arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Multiplication arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Subtraction arg0) {
		// TODO Auto-generated method stub
		
	}

	/** Interface method. Visit for AndExpression.
	 *  Gets left and right subexpressions and calls
	 *  accept for both subexpressions. 
	 *  (Essentially recursive call through ExpressionVis).
	 */
	@Override
	public void visit(AndExpression arg0) {
		Expression left = arg0.getLeftExpression();
		Expression right = arg0.getRightExpression();
		
		left.accept(this);
		right.accept(this);
		
	}

	@Override
	public void visit(OrExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Between arg0) {
		// TODO Auto-generated method stub
		
	}

	/** Interface method. Visit for EqualsTo Expression.
	 *  Gets left and right subexpressions, calls accept for
	 *  each. Then updates result to reflect whether 
	 *  the left subexpression is equal to the right subexpression.
	 */
	@Override
	public void visit(EqualsTo arg0) {
		Expression left = arg0.getLeftExpression();
		Expression right = arg0.getRightExpression();
		left.accept(this);
		right.accept(this);
		
	}

	@Override
	public void visit(GreaterThan arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(GreaterThanEquals arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(InExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(IsNullExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(LikeExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(MinorThan arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(MinorThanEquals arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(NotEqualsTo arg0) {
		// TODO Auto-generated method stub
		
	}

	/** Interface method. Visit for Column Expression.
	 *  Gets column name, finds whether it belongs to the left or right 
	 *  relation, and then places it in the corresponding column arraylist
	 */
	@Override
	public void visit(Column arg0) {
		String columnName = arg0.getWholeColumnName();
		if (leftSchema.contains(columnName)) {
			leftColumns.add(columnName);
		}
		else {
			rightColumns.add(columnName);
		}
		
	}

	@Override
	public void visit(SubSelect arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(CaseExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(WhenClause arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(ExistsExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(AllComparisonExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(AnyComparisonExpression arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Concat arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(Matches arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(BitwiseAnd arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(BitwiseOr arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void visit(BitwiseXor arg0) {
		// TODO Auto-generated method stub
		
	}

}
