
package database;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.LinkedList;

import optimizer.TableAndColumnStats;
import bPlusTree.IndexInfo;

/**
 * Singleton class used to keep track of relevant file paths.
 * Also used to keep track of initial table schemas (for scans).
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */

public class DatabaseCatalog {
	//String containing the input directory path argument provided to the jar
	private String inputDir;
	//String containing the extension to reach input folder's data
	private final String dataDir = "/db/data";
	//String containing the extension used to reach input folder's schema.txt
	private final String schemaFile = "/db/schema.txt"; 
	//Singleton instance of DatabaseCatalog
	private static DatabaseCatalog catalog = null;
	//Hashtable containing a mapping from table name to initial schema (as arraylist)
	private Hashtable<String, ArrayList<String>> schemaCatalog;
	//String containing the temporary directory path argument for storing temp (external) files
	private String temporaryDir;
	//number of pages used for sorting
	private String outputDir;
	private int sortPages;
	//number of pages used for BNLJ
	private int joinPages;
	//a map of indexes available from index name (string) to whether it is clustered (boolean)
	private HashMap<String,Boolean> availableIndexes;
	
	private HashMap<String, TableAndColumnStats> statInfo = null;
	private Map<String, List<IndexInfo>> tableInfo; // The index info for the tables
	
	/**
	 * Constructor used to create an instance of DatabaseCatalog.
	 * Can only be called by the class itself (singleton pattern).
	 */
	protected DatabaseCatalog() {
		schemaCatalog = new Hashtable<String, ArrayList<String>>();
		availableIndexes = new HashMap<String, Boolean>();
		tableInfo = new HashMap<String, List<IndexInfo>>();
	}
	
	/**
	 * Used to get an instance of DatabaseCatalog. If one already exists,
	 * existing one is returned. Otherwise, creates a new instance, stores it in
	 * catalog variable, and returns it.
	 * @return singleton instance of DatabaseCatalog.
	 */
	public static DatabaseCatalog getInstance() {
		if (catalog == null) {
			catalog = new DatabaseCatalog();
		}
		return catalog;
	}
	
	/**
	 * Used to give DatabaseCatalog the input directory path
	 * argument from Interpreter class. Once DatabaseCatalog stores 
	 * inputdir, it populates the schemaCatalog.
	 * @param dir -- String path to input directory
	 */
	public void setInputDir(String dir) {
		inputDir = dir;
		populateSchemaCatalog();
	}
	
	/**
	 * Returns input directory path argument
	 * @return input directory path
	 */
	public String getInputDir() {
		return inputDir;
	}
	
	/**
	 * Used to give DatabaseCatalog the temporary directory path
	 * argument from Interpreter class. This is used for storing
	 * temporary data on "external" files (example external sort)
	 * @param dir -- String path to temporary directory
	 */
	public void setTempDir(String dir) {
		temporaryDir = dir;
	} 
	
	/**
	 * Returns temporary directory path argument
	 * @return temporary directory path
	 */
	public String getTempDir() {
		return temporaryDir;
	}
	/**
	 * Sets Output directory path argument
	 * @param String dir -- String directory
	 */
	public void setOutputDir (String dir) {
		outputDir = dir;
	}
	/**
	 * Returns Output directory path argument
	 * @return String outputDir -- Output directory path
	 */
	public String getOutputDir() {
		return outputDir;
	}
	/**
	 * Returns Data directory path argument
	 * @return String dataDir -- Data directory path
	 */
	public String getDataDir() {
		return dataDir;
	}
	/**
	 * Returns Schema directory path argument
	 * @return String inputDir + schemaFile -- Path arguement of schema
	 */
	public String getSchemaFile() {
		return inputDir + schemaFile;
	}
	/**
	 * sets the number of sort pages written to external (sort) file
	 * @param p number of sort pages
	 */
	public void setSortPages(int p) {
		sortPages = p;
	}
	/**
	 * gets the number of sort pages written to external (sort) file
	 * @return number of sort pages
	 */
	public int getSortPages() {
		return sortPages;
	}
	/**
	 * sets the number of join pages read per io
	 * @param p number of pages
	 */
	public void  setJoinPages(int p) {
		joinPages = p;
	}
	/**
	 * gets the number of join pages to read per io
	 * @return the number of join pages
	 */
	public int getJoinPages() {
		return joinPages;
	}
	
	/** Helper function used to read the schemaFile and store mappings from
	 * table name to schema in the schemaCatalog. 
	 * Column names are stored as table.column_name.
	 */
	private void populateSchemaCatalog() {
		try (BufferedReader br = 
				new BufferedReader(new FileReader(inputDir + schemaFile))) {
			String line = br.readLine();
			while (line != null) {
				placeSchemaElement(line);
				line = br.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	/** Helper function used to place a single mapping from table name to schema
	 *  in the schema catalog.
	 * @param line -- a String representing a line from schema.txt. 
	 * Contains table name then column names, separated by spaces.
	 */
	private void placeSchemaElement(String line) {
		String[] items = line.split(" ");
		String tableName = items[0];
		String[] schemaArray = Arrays.copyOfRange(items, 1, items.length);
		ArrayList<String> schema = new ArrayList<String>(Arrays.asList(schemaArray));
		
		for (int i = 0; i < schema.size(); i++ ) {
			String el = schema.get(i);
			schema.set(i, tableName + "." + el); 
		}
		
		schemaCatalog.put(tableName, schema);
	}
	
	/** Used to get a table's schema from the schemaCatalog.
	 * @param tableName -- the name of the table corresponding to the target schema
	 * @return an arraylist of strings representing the column names in the table
	 */
	public ArrayList<String> getTableSchema(String tableName){
		return schemaCatalog.get(tableName);
	}
	
	/** Used to get the path to a table's data file.
	 * @param tableName -- the table name containing target data
	 * @return String path to table's data file
	 */
	public String getTablePath(String tableName) {
		return inputDir + dataDir + "/" + tableName;
	}
	
	/** Adds an indexName to our set of AvailableIndexes.
	 * @param indexName -- String indexName to add to set of available indexes.
	 */
	public void addAvailableIndex(String indexName, Boolean isClustered) {
		availableIndexes.put(indexName, isClustered);
	}
	
	/** Gets an index path, if that index exists.
	 *  Otherwise returns null
	 * @param indexName -- name of index to get path to (if possible)
	 * @return -- String of file path to specified index if exists, else null.
	 */
	public String getIndexPathIfAvailable(String indexName) {
		String path = null;
		if (availableIndexes.keySet().contains(indexName)) {
			path = inputDir+"/db/indexes/" + indexName;
		}
		
		return path;
	}
	
	/** Returns whether a given index is clustered (true) or not (false).
	 * Or null if index doesn't exist.
	 * @param indexName -- the index to check if clustered
	 * @return -- Boolean representing whether the index is clustered.
	 */
	public Boolean indexIsClustered(String indexName) {
		return availableIndexes.get(indexName);
	}
	/**
	 * Sets the Statistics Information on Tables
	 * @param HashMap<String, TableAndColumnStats> s -- Hashmap containing table information
	 */
	public void initStatInfo (HashMap<String, TableAndColumnStats> s) {
		statInfo = s;
	}
	/**
	 * gets the Statistics Information on Tables
	 * @param String tableName -- Table to get information of
	 * @return HashMap<String, TableAndColumnStats> stats.Info -- Hashmap containing table information
	 */
	public TableAndColumnStats getTableAndColumnStats(String tableName)
	{
		return statInfo.get(tableName);
	}
	
	/**
	 * Add the index information.
	 *
	 * @param relation the <em>original</em> name for the table.
	 * @param attr
	 */
	public void addIndexInfo(String relation, String attr, boolean clust, int ord, int n) {
		// create the table item if does not exist.
		if (!tableInfo.containsKey(relation)) {
			tableInfo.put(relation, new LinkedList<IndexInfo>());
		}
		
		List<IndexInfo> indicesOfTheTable = tableInfo.get(relation);
		
		IndexInfo info = new IndexInfo(relation, attr, clust, ord, n);
		
		// we should add clustered at the head of the list.
		if (clust) {
			indicesOfTheTable.add(0, info);
		} else {
			indicesOfTheTable.add(info);
		}
	}
	
	/**
	 * Get the index information from relation name and the attribute name.
	 * @param relation
	 * @param attr
	 * @return the index information.
	 * 		   <tt>null</tt> if does not found.
	 */
	public IndexInfo getIndexInfo(String relation, String attr) {
		List<IndexInfo> indexInfos = getIndexInfoList(relation);
		if (indexInfos == null) return null;
		
		// find the index info with the attribute name
		for (IndexInfo info : indexInfos) {
			if (info.getAttribute().equals(relation + "." + attr)) return info;
		}
		
		// Does not found
		return null;
	}
	
	/**
	 * Get the list of the index information of this relation.
	 * @param relation
	 * @return list of index information which belong to this table
	 * 		   <tt>null</tt> if does not found.
	 */
	public List<IndexInfo> getIndexInfoList(String relation) {
		// transfer to the origin table name
		//String originTable = DBCat.origName(relation);
		if (!tableInfo.containsKey(relation)) {
			return null;
		}
		return tableInfo.get(relation);
	}
	
}


