package database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.UUID;


import net.sf.jsqlparser.statement.select.OrderByElement;
import java.nio.*;
import database.QueryPlan;
/** Operator used to implement External Sort
 *  It begins by finding out the number of pages it will need to sort across.
 *  It sorts across each individual page and then it merges all the pages values
 *  together. 
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */

public class ExternalSortOperator extends Operator {
	
	//Child operator from operator query plan tree
	private Operator child;
	//Array list of strings specifying the sort order
	private ArrayList<String> orderByColumns;
	
	//create a sub-directory in temporary directory to store sorted runs
	private final String randomId = UUID.randomUUID().
			toString().substring(0, 8);
	//temporary directory used for sorting
	private final String temporaryDirectory 
						= DatabaseCatalog.getInstance().getTempDir() + "\\" +
							randomId + File.separator;
	
	//the sorted tuples are stored in this file
	private TupleReader sortedTupleReader = null;
	
	//this is used to keep track of files used for intermediate sorted runs
	private List<TupleReader> tupleReaderBuffers = 
			new ArrayList<TupleReader>(DatabaseCatalog.getInstance().getSortPages() - 1);
	//size of a page
	private static final int PAGESIZE = 4096;
	//number of bytes for one integer
	private static final int INTEGERSIZE = 4;
	//used to keep track of how many tuples per page we can fit
	private int tuplesPerPage = 0;
	//the number of tuples per sorted run
	private int tuplesPerRun = 0;

	/** Operator used to implement Blocked Nested Loop Join.
	 *  Constructor that Creates the ExternalSortOperator and places it correctly in the
	 *  PhysicalPlanBuilder in its tree. It creates its read and write to an external
	 *  file that it will need to use to sort across 1 page at a time. 
	 *  
	 *  @param Operator Child -- Child is the next Operator to be done after External Sort
	 *  @param ArrayList <String> columns -- Columsn to sort External Operator on
	 */
	public ExternalSortOperator(Operator child, ArrayList<String> columns) {
		
		//save context about this sort
		this.child = child;
		this.orderByColumns = columns;
		this.setSchema(child.getSchema());
		
		//Create a directory to store sorted runs 
		new File(temporaryDirectory).mkdirs();
		
		//The number tuples per page 
		tuplesPerPage = PAGESIZE / (
				(INTEGERSIZE + 1) * child.getSchema().size());
		//the number of tuples per sorted run. 
		tuplesPerRun = tuplesPerPage * DatabaseCatalog.getInstance().getSortPages();
		
		//create a array to keep the sorted tuple
		List<Tuple> tps = new ArrayList<Tuple>(tuplesPerRun);
				
		//For each run, gather/get the tuples, sort them and write them to disk
		int sortedRuns = 0;
		while (true) {
			try {
				tps.clear(); 
				int cnt = tuplesPerRun;
				Tuple tp = null;
				while (cnt-- > 0 && 
						(tp = child.getNextTuple()) != null)
						tps.add(tp);
				
				if (tps.isEmpty()) break;
				
				tps.sort(new CustomComparator());
				
				HumanTupleWriter tw = new HumanTupleWriter(null, getFileName(0, sortedRuns++));
				for (Tuple tuple : tps)
					tw.writeTuple(tuple);
				tw.close();
								
				if (tps.size() < tuplesPerRun) break;
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}
		
		if (sortedRuns == 0) return;
		
		//merge the sortedRuns
		int curPass = 1;
		int lastRuns = sortedRuns;
		while (lastRuns > 1)
			lastRuns = merge(curPass++, lastRuns);
		
		//file containing sorted data
		File oldFile = new File(getFileName(curPass - 1, 0));
		File newFile = new File(temporaryDirectory + "mergedRun");
		oldFile.renameTo(newFile);
		
		//create a handle to the sorted file
		try {
			sortedTupleReader = new HumanTupleReader(temporaryDirectory + "mergedRun");
		} catch (FileNotFoundException e) {
			sortedTupleReader = null;
		}
	}
	/** getNextTuple is used to get the next tuple from the Reader file.
	 * 
	 */
	@Override
	public Tuple getNextTuple() {
		if (sortedTupleReader == null) return null;
		return sortedTupleReader.readNextTuple();
	}
	/** Reset exists to scroll back to the top of an array in the TupleReader file
	 * 
	 */
	@Override
	public void reset() {
		if (sortedTupleReader == null) return;
		sortedTupleReader.reset();

	}
	
	/** Resets operator's tuple output to the tuple specified by int index.
	 * @param index -- the index to reset tuple output to.
	 * 
	 */
	@Override
	public void reset(int index) {
		sortedTupleReader.reset(index);
	}
	/** Operator used to implement Blocked Nested Loop Join.
	 *  It begins by finding out the number of pages it will need to join across 
	 *  I
	 *  in the HashSet, then return it.
	 * 
	 * @param int pass -- Gets the files path
	 * @param int run  -- Gets the file name
	 */
	private String getFileName(int pass, int run) {
		return temporaryDirectory + String.valueOf(pass) + 
				"_" + String.valueOf(run);
	}
	
	//merge the sorted runs
	/**
	 * Merge the sorted runs
	 * @param curPass -- which pass we are on
	 * @param lastRuns -- total number of runs
	 * @return run
	 */
	private int merge(int curPass, int lastRuns) {
		int curRuns = 0;
		int i = 0;
		
		//while merging the sorted runs, merge x pairs of runs into a new sorted run x times long
		//we are done when we merge everything into a single run
		while (i < lastRuns) {
			tupleReaderBuffers.clear();
			
			int maxJ = Math.min(i + DatabaseCatalog.getInstance().getSortPages() - 1, lastRuns);
			
			//get handle on sorted run input file(s)
			for (int j = i; j < maxJ; j++) {
				try {
					TupleReader tr = new HumanTupleReader(
							getFileName(curPass - 1, j));
					tupleReaderBuffers.add(tr);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					break;
				}
			}
			
			//get a handle on sorted run output file
			//get a tuple from each sorted run file and put it into priority queue
			//keep track of which file each tuple comes from
			//retrieve the (smallest) tuple (based on sort order) from the queue 
			//based on which file that tuple came from, retrieve another tuple from that
			//file and add it to the priority queue and repeat until no more tuples
			try {
				HumanTupleWriter tw = new HumanTupleWriter(null, getFileName(curPass, curRuns++));
				PriorityQueue<Tuple> pq = new PriorityQueue<Tuple>(tuplesPerRun, new CustomComparator());
	
				for (TupleReader tr : tupleReaderBuffers) {
					Tuple tp = tr.readNextTuple();
					if (tp != null) {
						tp.setTupleReader(tr);
						pq.add(tp);
					}
				}
				
				while (!pq.isEmpty()) {
					Tuple tp = pq.poll();
					tw.writeTuple(tp);
					TupleReader tr = tp.getTupleReader();
					tp = tr.readNextTuple();
					if (tp != null) {
						tp.setTupleReader(tr);
						pq.add(tp);
					}
				}
				
				tw.close();
				
				for (TupleReader tr : tupleReaderBuffers)
					tr.close();
				
				for (int j = i; j < maxJ; j++) {
					File file = new File(getFileName(curPass - 1, j));
					file.delete();
				}
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
			
			i += DatabaseCatalog.getInstance().getSortPages() - 1;
		}
		
		return curRuns;
	}
	
	/** Inner class used to implement Comparator<Tuple>
	 * Used to create a custom comparator for ORDER BY sorting.
	 *
	 */
	private class CustomComparator implements Comparator<Tuple> {
		//schema list for tuples being sorted
		private ArrayList<String> schema;
		
		/** Constructor for customComparator.
		 * Stores sortoperator's schema pointer.
		 * Then adds missing columns to orderByColumns list.
		 */
		public CustomComparator() {
			schema = getSchema();
			completeOrderByColumns();
		}
		
		/** Helper function to add missing columns to orderByColumns list.
		 * Appends any columns in naturally occurring order from schema 
		 * that haven't already been included in orderByColumns list
		 */
		private void completeOrderByColumns() {
			ArrayList<String> tempSchema = new ArrayList<String>(getSchema());
			for(String column : orderByColumns) {
				tempSchema.remove(column);
			}
			orderByColumns.addAll(tempSchema);
		}
		
		/** Overwrites compare function for comparator interface.
		 * For every column in orderByColumns, checks to see if arg0 tuple's values
		 * in the corresponding column are less than or greater than the 
		 * arg1 tuple's values. If less then returns -1. If greater than then returns 1.
		 * Otherwise it attempts to use the next column in orderByColumns as a tie breaker.
		 * If all column values are equal, returns 0. 
		 * @return -- integer to specify whether arg0 goes before (-1) arg1, after(1) arg1 or
		 * 				interchangeably(0) with arg1
		 */
		@Override
		public int compare(Tuple arg0, Tuple arg1) {
			for (String column : orderByColumns) {
				int index = schema.indexOf(column);
				if (arg0.getElement(index) < arg1.getElement(index)) {
					return -1;
				}
				else if (arg0.getElement(index) > arg1.getElement(index)) {
					return 1;
				}
			}
			return 0;
		}

	}
	/** Method used to print the tree
	 *  
	 *  @param PrintStream ps -- variable that contains the printing stream
	 *  @param int lv -- integer that contains the level of the tree.
	 */
	@Override
	public void printTree(PrintStream ps, int lv) {
		printIndent(ps, lv);
		ps.println(print());
		child.printTree(ps, lv + 1);
	}
	/** Method used to print the the current operator
	 *  
	 */
    @Override
    public String print() {
    	
    	if (orderByColumns != null) {
    		return String.format("ExternalSort%s", orderByColumns.toString());
    	}
    	else {
    		return String.format("ExternalSort[]");
    	}
    }


}

