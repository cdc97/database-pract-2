
package database;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;

import bPlusTree.RecordID;

/**
 * Operator that uses a relation's index to extract tuples
 * that satisfy the given low key and high key conditions.
 * Can be used in place of a scan operator when index is available,
 * and we have a select condition.
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class IndexScanOperator extends Operator {
	//whether the index we are using is clustered
	private boolean isClustered;
	//index reader used to read our index
	private BinaryIndexReader indexReader;
	//tuple reader used to read the target relation
	private BinaryTupleReader tupleReader;
	//whether this index scan operator is done returning tuples
	private boolean done;
	//the location of the attribute the index is built on within each tuple's schema
	int attributeIndex;
	//the highKey value, such that all tuples returned must have target attribute value < highkey
	private Integer highKey;
	private Integer lowKey;
	private String relation;
	
	/** Constructor for Index Scan Operator. Adjusts schema for aliases
	 * and initializes variables (causing the index reader to begin 
	 * deserializing the index).
	 * 
	 * @param relation -- the name of the target relation
	 * @param alias -- the alias used for the target relation. Or null if none.
	 * @param indexName -- the name of the index. Form of <relation>.<column>
	 * @param lowKey -- the low key value for searching the index (<)
	 * @param highKey -- the high key value for searching the index (>)
	 */
	public IndexScanOperator(String relation, String alias, String indexName, Integer lowKey, Integer highKey) {
		ArrayList<String> schema = new ArrayList<String>(DatabaseCatalog.getInstance().getTableSchema(relation));
		this.relation = relation;
		attributeIndex = schema.indexOf(indexName);
		
		if (alias != null) {
			for (int i = 0; i < schema.size(); i++) {
				schema.set(i, schema.get(i).replaceFirst(relation, alias));
			}
		}
		this.setSchema(schema);
		
		this.highKey = highKey;
		this.lowKey = lowKey;
		
		String relationPath = DatabaseCatalog.getInstance().getTablePath(relation);
		done = false;
		isClustered = DatabaseCatalog.getInstance().indexIsClustered(indexName);
		
		try {
			indexReader = new BinaryIndexReader(indexName, lowKey, highKey);
			RecordID first = indexReader.getNextRID();
			tupleReader = new BinaryTupleReader(relationPath, first);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	/** Returns the next valid tuple that satisfies the lowkey highkey 
	 * criteria.
	 * 
	 * @return -- the next valid tuple. Valid means lowKey < attribute val < highKey.
	 */
	@Override
	public Tuple getNextTuple() {
		if (done) {
			return null;
		}
		
		Tuple tuple = tupleReader.readNextTuple();
		//clustered index
		if (isClustered) {
			if (highKey != null) {
				if (tuple.getElement(attributeIndex) < highKey) {
					return tuple;
				}
				else {
					done = true;
					return null;
				}
			}
			return tuple;
		}
		
		//unclustered code
		RecordID next = indexReader.getNextRID();
		if (next == null) {
			done = true;
		}
		else{
			tupleReader.positionForNextTuple(next);
		}
		return tuple;

	}

	/** Resets this Index Scan Operator so we start returning 
	 * tuples from the beginning.
	 * 
	 */
	@Override
	public void reset() {
		indexReader.reset();
		tupleReader.reset();
		RecordID first = indexReader.getNextRID();
		tupleReader.positionForNextTuple(first);
		done = false;
	}
	/** Method used to print the current operator
	 *  
	 */
	@Override
	public String print() {
		return "IndexScan["+ relation +"," + getSchema().get(attributeIndex) + ","
				+ lowKey + "," +highKey+"]";
	}
	/** Method used to print the tree
	 *  
	 *  @param PrintStream ps -- variable that contains the printing stream
	 *  @param int lv -- integer that contains the level of the tree.
	 */
	@Override
	public void printTree(PrintStream ps, int lv) {
		printIndent(ps, lv);
		ps.println(print());
	}

}