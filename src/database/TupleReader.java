
package database;
/**
 * Interface used to generalize all TupleReaders. Used to read tuples
 * from files by scan operators.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public interface TupleReader {
	
	/**Reads the next unread tuple from the target file
	 * 
	 * @return returns the next tuple read from the file
	 */
	public Tuple readNextTuple();
	
	/**Closes any reading resources used.
	 * 
	 */
	public void close();
	
	/**Resets the reader to the beginning of the file.
	 * 
	 */
	public void reset();
	
	
	/**Resets the reader to the specific indexed tuple.
	 * @param index -- the int index of the tuple we want to reset to.
	 */
	public void reset(int index);
}
