
package database;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/** Operator used to implement elimination of duplicate tuples.
 * Checks to see if current tuple is already inside the HashSet set.
 * If it is, we move onto the next tuple. If not, we place the tuple
 * in the HashSet, then return it.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public class HashDuplicateEliminationOperator extends Operator {
	//child Operator in Operator tree query plan
	private Operator child;
	//HashSet of Tuples to keep track of distinct tuples we've processed
	private HashSet<Tuple> set;
	
	/** Constructor for HashDuplicateEliminationOperator.
	 * Sets child to child operator and creates a new, empty 
	 * HashSet.
	 * @param child -- child Operator in Operator tree query plan
	 */
	public HashDuplicateEliminationOperator(Operator child) {
		this.child = child;
		setSchema(child.getSchema());
		set = new HashSet<Tuple>();
	}
	
	/** Gets next distinct tuple.
	 * If child's tuple is null returns null. If hashset already
	 * contains tuple, calls getNextTuple() again. Otherwise, adds tuple
	 * to hashset and returns tuple.
	 */
	@Override
	public Tuple getNextTuple() {
		Tuple tuple = child.getNextTuple();
		if (tuple == null) {
			return null;
		}
		else if(set.contains(tuple)) {

			return getNextTuple();
		}
		set.add(tuple);
		return tuple;
	}

	/** Used to reset the tuples returned by operator. If reset,
	 * getNextTuple() begins processing at beginning of child's tuples.
	 * Calls child's reset function and then clears hashset of tuples we've 
	 * seen.
	 */
	@Override
	public void reset() {
		child.reset();
		set.clear();
		
	}
	/** Method used to print the current operator
	 *  
	 */
	@Override
	public String print() {
		// TODO Auto-generated method stub
		return "HashDupElim";
	}
	/** Method used to print the tree
	 *  
	 *  @param PrintStream ps -- variable that contains the printing stream
	 *  @param int lv -- integer that contains the level of the tree.
	 */
	@Override
	public void printTree(PrintStream ps, int lv) {
		printIndent(ps, lv);
		ps.println(print());
		child.printTree(ps, lv + 1);
	}

}

