
package database;

import java.io.PrintStream;
import java.util.ArrayList;
import net.sf.jsqlparser.expression.Expression;

/** Operator used to implement Sort Merge Join.
 *  It begins by taking two sorted ArrayLists of all the values in both tables. 
 *  It then searches through the right side of the table to match the join equality on the value
 *  of the left side of the Table. If it does it adds it to the output. If it does not it increments
 *  the left side ArrayList and it then continues to search from where the last element on the left 
 *  side was last put to output. (values could be the same so we can't keep moving forward we have
 *  to check that the current value is going to give the same results as the previous one).
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public class SMJOperator extends Operator {
	
	//left child operator in operator query plan. Used for outer tuples.
    private Operator leftChild;
    //right child operator in operator query plan. Used for inner tuples.
    private Operator rightChild;
	//indices of left order by column elements within left schema
    private ArrayList<Integer> leftIndices;
    //indices of right order by column elements within right schema
    private ArrayList<Integer> rightIndices;
    //tuple we're processing from left relation
    private Tuple leftTuple;
    //tuple we're processing from right relation
    private Tuple rightTuple; 
    //index from right relation used for resetting after looping
    private int rightTupleIndex;
    //whether or not we are currently looping
    private boolean looping;
    private Expression expression;
    
    /** Constructor used to create the SMJ operator. 
     * 
     * @param Operator left -- The Left table to perform the SMJ across
     * @param Operator right -- The Right table to perform the SMJ across
     * @param ArrayList <String> leftOrderBy -- the array of values on left table based on the equality condition
     * @param ArrayList <String> rightOrderBy -- the array of values on right table based on the equality condition
     */
	public SMJOperator(Operator left, Operator right, ArrayList<String> leftOrderBy, ArrayList<String> rightOrderBy, Expression expression) {
		ArrayList<String> leftSchema = left.getSchema();
        ArrayList<String> rightSchema = right.getSchema();
        ArrayList<String> joinSchema = new ArrayList<String>(leftSchema);
        joinSchema.addAll(rightSchema);
        
        this.setSchema(joinSchema);
          
        leftChild = left;
        rightChild = right;
        leftIndices = new ArrayList<Integer>();
        rightIndices = new ArrayList<Integer>();
        
        setUpIndices(leftIndices, leftOrderBy, leftSchema);
        setUpIndices(rightIndices, rightOrderBy, rightSchema);
     
        leftTuple = leftChild.getNextTuple();
        rightTuple = rightChild.getNextTuple();
        
        rightTupleIndex = 0;
        looping = false;
        this.expression = expression;
	}
	
	/** Method setUpIndices that sets up the indices for every column in orderByColumns. 
	 * 
	 * @param ArrayList<Integer> indices -- The values of every row based on index number
	 * @param ArrayList<String> orderByCols -- The list of all the columns based on order by
	 * @param ArrayList<String> schema -- The schema used for the current Tables
	 */
	private void setUpIndices(ArrayList<Integer> indices, ArrayList<String> orderByCols, ArrayList<String> schema) {
		for (String column : orderByCols) {
			Integer index = schema.indexOf(column);
			indices.add(index);
		}
	}
	
	/** Method getNextTuple that gets the next tuple based on equality from left and right
	 * side of table. 
	 * 
	 * @return Tuple tuple
	 */
	@Override
	public Tuple getNextTuple() {
		while ((leftTuple != null) && ((rightTuple != null) || looping)) {
			int compareResult = compare(leftTuple, rightTuple);
			if (compareResult < 0) {
				leftTuple = leftChild.getNextTuple();
				if (looping) {
					looping = false;
					rightChild.reset(rightTupleIndex);
					rightTuple = rightChild.getNextTuple();
				}
			}
			else if (compareResult > 0) {
				rightTuple = rightChild.getNextTuple();
				rightTupleIndex++;
			}
			//tuples meet join conditions
			else {
				looping = true;
				Tuple result = new Tuple(leftTuple, rightTuple);
				rightTuple = rightChild.getNextTuple();
				return result;
			}
		}
		return null;
	}
	/** Method reset that gets to the top of the left and right table.
	 * 
	 */
	@Override
	public void reset() {
		leftChild.reset();
		rightChild.reset();
		rightTupleIndex = 0;
		looping = false;
		
	}
	/** Method compare that gets the next tuple based on equality 
	 * from left and right side of table. 
	 * 
	 * @param Tuple left -- tuple on left side of left table
	 * @param Tuple right -- tuple on right side of right table
	 * @return int i -- returns 1 if true on equivalency. 0 on false.
	 */
	private int compare(Tuple left, Tuple right) {
		if (right == null) {
			return -1;
		}
		
		for (int i=0; i < leftIndices.size(); i++) {
			int leftElement = left.getElement(leftIndices.get(i));
			int rightElement = right.getElement(rightIndices.get(i));
			
			if (leftElement < rightElement) {
				return -1;
			}
			else if (leftElement > rightElement) {
				return 1;
			}
		}
		return 0;
	}
		
	/** Method used to print the current operator
	 *  
	 */
    @Override
    public String print() {
        String expression = (this.expression != null) ? this.expression.toString() : "";
        return String.format("SMJ[" + expression + "]");
    }
	/** Method used to print the tree
	 *  
	 *  @param PrintStream ps -- variable that contains the printing stream
	 *  @param int lv -- integer that contains the level of the tree.
	 */
	@Override
	public void printTree(PrintStream ps, int lv) {
		printIndent(ps, lv);
		ps.println(print());
		leftChild.printTree(ps, lv + 1);
		rightChild.printTree(ps, lv + 1);
	}	
	
	
}
