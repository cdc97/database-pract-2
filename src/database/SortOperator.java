package database;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/** Operator used to implement sorts.
 * 
 * It grabs all tuples from child operator, then sorts them (according to orderByColumns) 
 * and then stores the result in an ArrayList of Tuples. when getNextTuple is called,
 * it returns the Tuples one by one from the stored list.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class SortOperator extends Operator {
	//Child operator from operator query plan tree
	private Operator child;
	//Arraylist of strings specifying the sort order
	private ArrayList<String> orderByColumns;
	//Arraylist of tuples, ends up being sorted 
	private ArrayList<Tuple> allTuples;
	//used to keep track of the index of the tuple in allTuples we should return next
	private int index;
	
	/** SortOperator constructor. Stores child and columns parameters
	 * Sets schema as childs schema. Grabs and stores all child tuples, then calls
	 * sort, using a CustomComparator object (inner class).
	 * @param child -- child operator from operator query plan tree
	 * @param columns -- list of columns specifying columns listed in ORDER BY 
	 */
	public SortOperator(Operator child, ArrayList<String> columns) {
		this.child = child;
		this.orderByColumns = columns;
		this.setSchema(child.getSchema());
		allTuples = new ArrayList<Tuple>();
		getAllChildTuples();
		allTuples.sort(new CustomComparator());
		index = 0;
	}
	
	/** Helper function to get and store all tuples from child operator.
	 */
	private void getAllChildTuples() {
		Tuple tuple;
		while ((tuple = child.getNextTuple()) != null) {
			allTuples.add(tuple);
		}
	}
	
	/** Returns the tuple located in allTuples list at the 
	 * index specified by index variable. Returns null when 
	 * out of tuples.
	 * 
	 */
	@Override
	public Tuple getNextTuple() {
		if (index < allTuples.size()){
			return allTuples.get(index++);
		}
		return null;
	}

	/**Resets operator's tuple output.
	 * Just reset index to 0 so don't have to get tuples from child
	 * operator again and resort.
	 */
	@Override
	public void reset() {
		index=0;
	}

	/** Resets operator's tuple output to the tuple specified by int index.
	 * @param index -- the index to reset tuple output to.
	 * 
	 */
	@Override
	public void reset(int index) {
		this.index = index;
	}
	
	/** Inner class used to implement Comparator<Tuple>
	 * Used to create a customcomparator for ORDER BY sorting.
	 *
	 */
	private class CustomComparator implements Comparator<Tuple> {
		//schema list for tuples being sorted
		private ArrayList<String> schema;
		
		/** Constructor for customComparator.
		 * Stores sortoperator's schema pointer.
		 * Then adds missing columns to orderByColumns list.
		 */
		public CustomComparator() {
			schema = getSchema();
			completeOrderByColumns();
		}
		
		/** Helper function to add missing columns to orderByColumns list.
		 * Appends any columns in naturally occurring order from schema 
		 * that haven't already been included in orderByColumns list
		 */
		private void completeOrderByColumns() {
			ArrayList<String> tempSchema = new ArrayList<String>(getSchema());
			for(String column : orderByColumns) {
				tempSchema.remove(column);
			}
			orderByColumns.addAll(tempSchema);
		}
		
		/** Overwrites compare function for comparator interface.
		 * For every column in orderByColumns, checks to see if arg0 tuple's values
		 * in the corresponding column are less than or greater than the 
		 * arg1 tuple's values. If less then returns -1. If greater than then returns 1.
		 * Otherwise it attempts to use the next column in orderByColumns as a tie breaker.
		 * If all column values are equal, returns 0. 
		 * @return -- integer to specify whether arg0 goes before (-1) arg1, after(1) arg1 or
		 * 				interchangeably(0) with arg1
		 */
		@Override
		public int compare(Tuple arg0, Tuple arg1) {
			for (String column : orderByColumns) {
				int index = schema.indexOf(column);
				if (arg0.getElement(index) < arg1.getElement(index)) {
					return -1;
				}
				else if (arg0.getElement(index) > arg1.getElement(index)) {
					return 1;
				}
			}
			return 0;
		}
		
		

	}
	/** Method used to print the tree
	 *  
	 *  @param PrintStream ps -- variable that contains the printing stream
	 *  @param int lv -- integer that contains the level of the tree.
	 */
	@Override
	public void printTree(PrintStream ps, int lv) {
		printIndent(ps, lv);
		ps.println(print());
		child.printTree(ps, lv + 1);
	}
	/** Method used to print the current operator
	 *  
	 */
    @Override
    public String print() {
    	
    	if (orderByColumns != null) {
    		return String.format("ExternalSort%s", orderByColumns.toString());
    	}
    	else {
    		return String.format("ExternalSort[]");
    	}
    }



}
