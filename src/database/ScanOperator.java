package database;
import java.io.PrintStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/** Operator used to implement scans.
 * Read lines in table data from data file and returns corresponding 
 * tuples one by one.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class ScanOperator extends Operator {
	
	//tupleReader used to read and generate tuples from target table
	private TupleReader tupleReader;
	private String tablename;
	
	/** Constructor for ScanOperator. Gets and stores path to table data file.
	 * Also creates and sets BufferedReader. Gets and stores table schema as well.
	 * Finally, if a non-null alias argument is included, table names in schema columns
	 * are replaced with alias name.
	 * 
	 * @param tableName -- name of target table
	 * @param alias -- alias name for target table. Used in schema columns from here on.
	 */
	public ScanOperator(String tableName, String alias) {
		String tableDataPath = DatabaseCatalog.getInstance().getTablePath(tableName);
		try {
			//tupleReader = new HumanTupleReader(tableDataPath);
			tupleReader = new BinaryTupleReader(tableDataPath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		ArrayList<String> schema = new ArrayList<String>(DatabaseCatalog.getInstance().getTableSchema(tableName));
		if (alias != null) {
			for (int i = 0; i < schema.size(); i++) {
				schema.set(i, schema.get(i).replaceFirst(tableName, alias));
			}
		}
		this.setSchema(schema);
	}
	
	/** Reads a line from data file, creates corresponding tuple, and
	 * returns tuple. Returns null when out of lines to read.
	 * 
	 * @return tuple created from line read from data file.
	 */
	@Override
	public Tuple getNextTuple() {
		return tupleReader.readNextTuple();
	}

	/** Resets operator's tuple output.
	 *  Closes reader, then opens new reader on the table data file.
	 */
	@Override
	public void reset() {
		tupleReader.reset();	
	}
	/** Method used to print the current operator
	 *  
	 */
	@Override
	public String print() {
		return "TableScan[" + tablename + "]";
	}
	/** Method used to print the tree
	 *  
	 *  @param PrintStream ps -- variable that contains the printing stream
	 *  @param int lv -- integer that contains the level of the tree.
	 */
	@Override
	public void printTree(PrintStream ps, int lv) {
		printIndent(ps, lv);
		ps.println(print());
	}

}

