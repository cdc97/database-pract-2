
package database;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/** Operator used to implement elimination of duplicate tuples.
 *  Precondition: tuples received from child are sorted.
 * Checks to see if current tuple is equal to the previous tuple processed.
 * If it is, skips the current tuple. If not, returns current tuple.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public class DuplicateEliminationOperator extends Operator {
	//child Operator in Operator tree query plan
	private Operator child;
	//contains last tuple we processed successfully
	private Tuple lastTuple;
	
	/** Constructor to create an instance of DuplicateEliminationOperator.
	 *  Stores child, sets schema as child's schema.
	 *  Initializes lastTuple as null.
	 * @param child -- child Operator in Operator tree query plan
	 */
	public DuplicateEliminationOperator(Operator child) {
		this.child = child;
		setSchema(child.getSchema());
		lastTuple = null;
	}
	
	/** Used to get next distinct tuple.
	 * If tuple has already been processed previously, continue to next tuple.
	 * Otherwise return tuple until we see tuple==null.
	 * Precondition: tuples are sorted.
	 */
	@Override
	public Tuple getNextTuple() {
		Tuple tuple = child.getNextTuple();
		while (tuple != null && tuple.equals(lastTuple)) {
			tuple = child.getNextTuple();
		}
		
		if (tuple == null) {
			return null;
		}
		
		lastTuple = tuple;
		return tuple;
		
	}

	/** Used to reset the output of DuplicateEliminationOperator.
	 * If getNextTuple() is called after reset, the operator will begin 
	 * processing from the top of the child operator output.
	 */
	@Override
	public void reset() {
		child.reset();
		
	}
	
	/** Method used to print the current operator
	 *  
	 */
	@Override
	public String print() {
		return "DupElim";
	}
	/** Method used to print the tree
	 *  
	 *  @param PrintStream ps -- variable that contains the printing stream
	 *  @param int lv -- integer that contains the level of the tree.
	 */
	@Override
	public void printTree(PrintStream ps, int lv) {
		printIndent(ps, lv);
		ps.println(print());
		child.printTree(ps, lv + 1);
	}


}
