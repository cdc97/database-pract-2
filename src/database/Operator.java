package database;

import java.util.ArrayList;
import java.util.List;
import java.io.PrintStream;

/** Abstract class extended by every Operator.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */

public abstract class Operator {
	//an ArrayList of strings denoting the schema of the operator's output table
	private ArrayList<String> schema;
	
	//function that returns an operator's next result Tuple
	abstract public Tuple getNextTuple();
	
	//function to reset an operator's output 
	abstract public void reset();
	
	/** Function used to return an operator's schema.
	 * @return ArrayList of strings denoting schema of operator's output table
	 */
	public ArrayList<String> getSchema() {
		return schema;
	}
	
	/** Sets the operator's schema variable to schema.
	 * @param schema -- the schema of the operator's output table
	 */
	public void setSchema(ArrayList<String> schema) {
		this.schema = schema;
	}
    
	/** Function used to print all tuples being output by this
	 * Operator.
	 */
	public void dump(){
		Tuple next = this.getNextTuple();
		while (next != null){
			System.out.println(next);
			next= this.getNextTuple();
		}
	}
	
	/** Function used to reset a sort operator to a given tuple index,
	 * indexing from 0.
	 * @param index -- the index of the tuple to reset to.
	 */
	public void reset(int index) {
		
	}
	
	/** Method used to print the indent
	 *  @param PrintStream ps -- Stream of printing
	 *  @param int lv -- level in the tree
	 */
	protected static void printIndent(PrintStream ps, int lv) {
		while (lv-- > 0)
			ps.print('-');
	}
	
	/**
	 * Prints this operator
	 * @return
	 */
	public abstract String print();
	
	/**
	 * Print the plan
	 * @param ps
	 * @param lv
	 */
	public abstract void printTree(PrintStream ps, int lv);
	
}

