
package database;

import java.util.ArrayList;
import java.io.PrintStream;

import net.sf.jsqlparser.expression.Expression;
/** Operator used to implement Blocked Nested Loop Join.
 *  It begins by finding out the number of pages it will need to join across the two tables
 *  It then gets each tuple from current page and joins them together on the join condition
 *  It performs this across each page. And then it merges all the pages consecutively together.
 *  It is not sorted.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public class BNLJOperator extends Operator {

	//left child operator in operator query plan. Used for outer tuples.
    private Operator leftChild;
    //right child operator in operator query plan. Used for inner tuples.
    private Operator rightChild;
    //expression that all tuples must satisfy in order to be returned by getNextTuple
    private Expression expression;
    //instance of expressionVisitor used to check if tuples satisfy expressions
    private ExpressionVis expressionVisitor;
    
    //tuple array used to buffer outer tuples. 
    //Size is numPages*4096 / (# of tuple attributes * 4)
	private Tuple[] buffer;
    //number of tuples currently contained in buffer
	private int numTuplesInBuffer;
	//the current index where we are in processing our buffer
	private int bufferIndex;
	//current tuple from the right child operator
	private Tuple currentTuple;
	
	/** Constructor used to create the Block Nested loop join
	 * It joins the tables based on join condition per each page. 
	 * 
	 * @param Operator left -- Left Table
	 * @param Operator right -- Right Table
	 * @param Expression expression -- Join condition to join tables on
	 * @param int numPages -- number of pages thats going to be used for BNLJ
	 */
	public BNLJOperator(Operator left, Operator right, Expression expression, int numPages){
		ArrayList<String> leftSchema = left.getSchema();
        ArrayList<String> rightSchema = right.getSchema();
        ArrayList<String> joinSchema = new ArrayList<String>(leftSchema);
        joinSchema.addAll(rightSchema);
        
        this.setSchema(joinSchema);
        
        this.expressionVisitor = new ExpressionVis(joinSchema);
        
        leftChild = left;
        rightChild = right;
        this.expression = expression;
        
        int bufferSize = (numPages * 4096) / (leftSchema.size() * 4);
        this.buffer = new Tuple[bufferSize];
        loadTuplesIntoBuffer();
        currentTuple = rightChild.getNextTuple();
	}
	
	/** Method loadtuplesIntoBuffer used to get all the tuples in the current page
	 *  
	 */
	private void loadTuplesIntoBuffer() {
		bufferIndex = 0;
		numTuplesInBuffer = 0;
		Tuple tuple;
		while (numTuplesInBuffer < buffer.length && (tuple = leftChild.getNextTuple()) != null) {
			buffer[numTuplesInBuffer] = tuple;
			numTuplesInBuffer++;
		}
	}
	/** Method getNextTuple used to get all the next tuple in current table
	 *  
	 *  @param Tuple tuple -- Returns next tuple in given page
	 */
	@Override
	public Tuple getNextTuple() {
		while (true) {			
			
			if (bufferIndex == numTuplesInBuffer) {
				currentTuple = rightChild.getNextTuple();
				bufferIndex = 0;
			}
			
			if (currentTuple == null) {
				//if buffer was not completely filled, that means we should return null
				//because no more tuples to fetch from outer relation
				if (numTuplesInBuffer < buffer.length) {
					return null;
				}
				rightChild.reset();
				currentTuple = rightChild.getNextTuple();
				loadTuplesIntoBuffer();
			}
			
			Tuple combined = new Tuple(buffer[bufferIndex], currentTuple);
			bufferIndex++;
            if (expression != null) {
                expressionVisitor.resetForNextTuple(combined);
                expression.accept(expressionVisitor);
                
                if (expressionVisitor.getResult()) {
                    return combined;
                }
            
            }
        
            if (expression == null) {
                return combined;
            }
		}

		
	}
	/** Method reset used to get to the beginning of a current page of tuples
	 *  
	 */
	@Override
	public void reset() {
		leftChild.reset();
        rightChild.reset();
        currentTuple = rightChild.getNextTuple();
		loadTuplesIntoBuffer();
	}
	/** Method used to print the current operator
	 *  
	 */
    @Override
    public String print() {
        String expression = (this.expression != null) ? this.expression.toString() : "";
        return String.format("BNLJ[" + expression + "]");
    }
	/** Method used to print the tree
	 *  
	 *  @param PrintStream ps -- variable that contains the printing stream
	 *  @param int lv -- integer that contains the level of the tree.
	 */
	@Override
	public void printTree(PrintStream ps, int lv) {
		printIndent(ps, lv);
		ps.println(print());
		leftChild.printTree(ps, lv + 1);
		rightChild.printTree(ps, lv + 1);
	}

}
