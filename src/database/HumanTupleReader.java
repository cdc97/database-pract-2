
package database;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Implementation of the TupleReader interface used to handle reading
 * tuples from a file in human-readable format (line by line csv).
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class HumanTupleReader implements TupleReader {
	//a reader used to read the data file
	private BufferedReader tableData;
	//the String containing the file path to the data file for the target table
	private String filePath;
	
	/**Constructor for HumanTupleReader. Initializes new BufferedReader and sets
	 * filePath variable
	 * 
	 * @param filePath -- string file path to data file
	 * @throws FileNotFoundException
	 */
	public HumanTupleReader(String filePath) throws FileNotFoundException {
		this.tableData = new BufferedReader(new FileReader(filePath));
		this.filePath = filePath;
	}
	
	/** Reads next line from data file and creates a corresponding Tuple object,
	 * then returns it. If reading fails, returns null.
	 *
	 * @return -- returns the next Tuple that we haven't yet returned
	 */
	@Override
	public Tuple readNextTuple() {
		try {
			String line = tableData.readLine();
			if (line != null){
				Tuple tuple = new Tuple(line);
				return tuple;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

	/** Closes tableData, our BUfferedReader, used to read the file.
	 * 
	 */
	@Override
	public void close() {

		try {
			tableData.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/** Closes tableData, then creates a new BufferedReader for our filepath, causing
	 * the next tuple read to be the first tuple in the file.
	 * 
	 */
	@Override
	public void reset() {
		try {
			tableData.close();
			tableData = new BufferedReader(new FileReader(this.filePath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/** Resets readers tuple output to the tuple specified by int index.
	 * @param index -- the index to reset tuple output to.
	 * 
	 */
	@Override
	public void reset(int index) {
		reset();
		fastForwardReader(index);
	}
	
	/**Fast forwards a reader position to specific 
	 * line number. 
	 * @param lineNumber -- int value denoting the line number we want to reach
	 * line number indexed from 0.
	 */
	private void fastForwardReader(int lineNumber) {
		for (int i=0; i < lineNumber; i++) {
			try {
				tableData.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
