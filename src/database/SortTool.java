
package database;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Tool used to sort output for debugging. 
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class SortTool {
	//query plan who's output to sort
	QueryPlan qp;
	
	//Arraylist of tuples, ends up being sorted 
	private ArrayList<Tuple> allTuples;
	
	//schema of tuples, used for orderBy
	private ArrayList<String> orderByColumns;
	
	/** Constructor for sort tool.
	 * 
	 * @param qp --query plan with output we must sort
	 */
	public SortTool(QueryPlan qp) {
		this.qp = qp;	
		allTuples = new ArrayList<Tuple>();
		orderByColumns = qp.getRootSchema();
		loadTuples();
		allTuples.sort(new CustomComparator());
		//System.out.println(allTuples.size());
	}
	
	/** Loads all tuples from qp for sorting
	 * 
	 */
	private void loadTuples() {
		Tuple tuple;
		while ((tuple = qp.getNextResultTuple()) != null) {
			allTuples.add(tuple);
		}
	}
	
	/** Writes all sorted tuples to output.
	 * 
	 * @param filePath -- string path to output location
	 */
	public void dumpTuplesToOutput(String filePath){
		try {
			HumanTupleWriter htw = new HumanTupleWriter(null, filePath);
			htw.dump(allTuples);
			htw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private class CustomComparator implements Comparator<Tuple> {
		//schema list for tuples being sorted
		private ArrayList<String> schema;
		
		/** Constructor for customComparator.
		 */
		public CustomComparator() {
			schema = qp.getRootSchema();
		}
		
		
		/** Overwrites compare function for comparator interface.
		 * For every column in orderByColumns, checks to see if arg0 tuple's values
		 * in the corresponding column are less than or greater than the 
		 * arg1 tuple's values. If less then returns -1. If greater than then returns 1.
		 * Otherwise it attempts to use the next column in orderByColumns as a tie breaker.
		 * If all column values are equal, returns 0. 
		 * @return -- integer to specify whether arg0 goes before (-1) arg1, after(1) arg1 or
		 * 				interchangeably(0) with arg1
		 */
		@Override
		public int compare(Tuple arg0, Tuple arg1) {
			for (String column : orderByColumns) {
				int index = schema.indexOf(column);
				if (arg0.getElement(index) < arg1.getElement(index)) {
					return -1;
				}
				else if (arg0.getElement(index) > arg1.getElement(index)) {
					return 1;
				}
			}
			return 0;
		}

	}
	
}
