
package database;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.*;

/** Operator used to implement project.
 * Given target column names, finds the indices corresponding to the target 
 * columns in the child operator's schema. Then uses these indices to construct new
 * tuples from child's operator's tuples containing just the data at the specified 
 * indices. Returns the result subtuple.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class ProjectOperator extends Operator {
	//child Operator in Operator query plan.
	private Operator child;
	//Integer array containing indices corresponding to data we want in our projection
	private Integer[] indices;
	
	/** Constructor for ProjectOperator. Sets child variable and sets this operator's
	 * schema as the input list of target column names. Also creates and populates the
	 * indices array with indexes corresponding to target data in the child tuples. 
	 * 
	 * @param child -- child Operator in Operator query plan
	 * @param columnNames -- target columns for projection. Schema of output table.
	 */
	public ProjectOperator(Operator child, ArrayList<String> columnNames) {
		this.child = child;
		this.setSchema(columnNames);
		indices = new Integer[columnNames.size()];
		
		ArrayList<String> childSchema = child.getSchema();
		int i = 0;
		for (String columnName : columnNames) {
			indices[i] = childSchema.indexOf(columnName);
			i++;
		}
		
	}
	
	/** Returns next tuple containing only data included in the projection 
	 * output.
	 */
	@Override
	public Tuple getNextTuple() {
		Tuple childTuple = child.getNextTuple();
		if (childTuple != null){
			return new Tuple(childTuple, indices);
		}
		return null;
	}
	
	/** Resets operator's tuple output.
	 */
	@Override
	public void reset() {
		child.reset();
	}
	/** Method used to print the current operator
	 *  
	 */
	@Override
	public String print() {
		return String.format("Project%s", 
				((this.getSchema() == null) ? "[null]" : this.getSchema().toString()));
	}
	/** Method used to print the tree
	 *  
	 *  @param PrintStream ps -- variable that contains the printing stream
	 *  @param int lv -- integer that contains the level of the tree.
	 */
	@Override
	public void printTree(PrintStream ps, int lv) {
		printIndent(ps, lv);
		ps.println(print());
		child.printTree(ps, lv + 1);
	}
	
	

}
