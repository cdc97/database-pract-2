package database;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.io.PrintStream;

import net.sf.jsqlparser.expression.Expression;

/** Operator used to implement selects.
 * Gets tuple from child operator then checks to see if tuple satisfies 
 * the select expression. If it does, tuple is returned. Otherwise, we move onto
 * next tuple.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class SelectOperator extends Operator {
	//child operator in operator query plan tree
	private Operator child;
	//select expression that tuples must satisfy to be returned
	private Expression expression;
	// expressionVisitor instance used to evaluate whether tuple satisfies expression
	private ExpressionVis expressionVisitor;
	
	/** SelectOperator Constructor. Sets schema as child's schema. Stores child and
	 * expression parameters. Also instantiates and stores new ExpressionVis object.
	 * 
	 * @param child -- child operator in operator query plan tree
	 * @param expression -- select expression that tuples must satisfy to be returned
	 */
	public SelectOperator(Operator child, Expression expression) {
		this.setSchema(child.getSchema());
		this.child = child;
		this.expression = expression;
		this.expressionVisitor = new ExpressionVis(this.getSchema());
	}
	
	/** Gets child tuple, checks to see if it satisfies expression.
	 * If it does, returns tuple. Continues until we run out of tuples or find
	 * a tuple satisfying expression.
	 * If tuple == null, returns null.
	 * 
	 */
	@Override
	public Tuple getNextTuple() {
		while(true) {
			Tuple tuple = child.getNextTuple();
			if (tuple == null) {
				return null;
			}
			expressionVisitor.resetForNextTuple(tuple);
			expression.accept(expressionVisitor);
			if (expressionVisitor.getResult() == true){
				return tuple;
			}
		}
		
	}

	/** Resets operator's tuple output.
	 */
	@Override
	public void reset() {
		child.reset();
	}
	/** Method used to print the current operator
	 *  
	 */
    @Override
    public String print() {
        String expression = (this.expression != null) ? this.expression.toString() : "";
        return String.format("Select[" + expression + "]");
    }
	/** Method used to print the tree
	 *  
	 *  @param PrintStream ps -- variable that contains the printing stream
	 *  @param int lv -- integer that contains the level of the tree.
	 */
	@Override
	public void printTree(PrintStream ps, int lv) {
		printIndent(ps, lv);
		ps.println(print());
		child.printTree(ps, lv + 1);
	}


}

