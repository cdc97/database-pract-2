
package database;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * Implementation of the TupleWriter interface used to handle writing
 * output tuples to a file in human readable format (csv, line by line).
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class HumanTupleWriter implements TupleWriter {
	//buffered writer object used to write to output file
	private BufferedWriter bw;
	//query plan who's output we are writing to output file
	private QueryPlan qp;
	
	/**Constructor for HumanTupleWriter. Initializes class variables.
	 * 
	 * @param qp -- query plan who's output we are writing to file
	 * @param fileOutputPath -- string file path to output file
	 * @throws FileNotFoundException
	 */
	public HumanTupleWriter(QueryPlan qp, String fileOutputPath) throws FileNotFoundException {
		this.qp = qp;
		bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileOutputPath)));
	}
	
	/** Helper function to write a tuple to the target file,
	 * then create a new line.
	 * 
	 * @param tuple
	 */
	public void writeTuple(Tuple tuple) {
		try {
			bw.write(tuple.toString());
			bw.newLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Closes our current bufferedwriter.
	 * 
	 */
	@Override
	public void close() {
		try {
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/** Writes all output tuples from our query plan object to
	 * the target output file, line by line.
	 * 
	 */
	@Override
	public void dump() {
		Tuple tuple;
		while ((tuple = qp.getNextResultTuple()) != null) {
			writeTuple(tuple);
		}
		
	}
	
	/** Writes all output tuples from inputted ArrayList object to
	 * the target output file, line by line.
	 * @param tuples -- list of all tuples to print to output
	 */
	public void dump(ArrayList<Tuple> tuples) {
		for (Tuple tuple : tuples) {
			writeTuple(tuple);
		}
	}

}

