package database;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import net.sf.jsqlparser.expression.Expression;

/** Operator used to implement joins.
 * Computes cross product tuples and checks to see if computed tuples
 * satisfy the given expression. If expression is null, computes the cross
 * product.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class JoinOperator extends Operator {
    //left child operator in operator query plan. Used for outer tuples.
    private Operator leftChild;
    //right child operator in operator query plan. Used for inner tuples.
    private Operator rightChild;
    //expression that all tuples must satisfy in order to be returned by getNextTuple
    private Expression expression;
    //instance of expressionVisitor used to check if tuples satisfy expressions
    private ExpressionVis expressionVisitor;
    //Current outer tuple for computing cross product tuples in getNextTuple
    private Tuple outerTuple;
    
    /** Constructor for join operator. Creates new schema by appending right schema 
     * onto left schema. Stores new schema inside operator. Instantiates new instance
     * of expressionvisitor and stores it. Stores children variables and join expression.
     * Initializes outerTuple as first tuple from leftChild.
     * 
     * @param left -- left child operator in operator query plan
     * @param right -- right child operator in operator query plan
     * @param expression -- join expression that tuples must satisfy to be returned
     */
    public JoinOperator(Operator left, Operator right, Expression expression) {
        ArrayList<String> leftSchema = left.getSchema();
        ArrayList<String> rightSchema = right.getSchema();
        ArrayList<String> joinSchema = new ArrayList<String>(leftSchema);
        joinSchema.addAll(rightSchema);
        
        this.setSchema(joinSchema);
        
        this.expressionVisitor = new ExpressionVis(joinSchema);
        
        leftChild = left;
        rightChild = right;
        this.expression = expression;
        outerTuple = leftChild.getNextTuple();
        
    }
    
    /** Computes every tuple in the cross product of leftChild and rightChild, then
     * checks to see if tuple satisfies expression. If it does, returns tuple. If not,
     * continues until it finds a combined tuple that satisfies expression. 
     * If expression is null, just returns every tuple in
     * cross product (one per call).
     */
    @Override
    public Tuple getNextTuple() {
        
        boolean done = false;
        
        while (!done) {

            Tuple innerTuple = rightChild.getNextTuple();
            if (innerTuple == null) {
                rightChild.reset();
                innerTuple = rightChild.getNextTuple();
                outerTuple = leftChild.getNextTuple();
            }
        
            if (outerTuple == null) {
                done = true;
                return null;
            } 
        
            Tuple combined = new Tuple(outerTuple, innerTuple);
            if (expression != null) {
                expressionVisitor.resetForNextTuple(combined);
                expression.accept(expressionVisitor);
                
                if (expressionVisitor.getResult()) {
                    done = true;
                    return combined;
                }
            
            }
        
            if (expression == null) {
                done = true;
                return combined;
            }
        }
        return null;
    }

    /** Resets its tuple output. After reset, getNextTuple calls return tuples 
     * from the beginning of the cross product of leftChild and rightChild.
     * Calls reset for both children operators, then reinitializes outerTuple
     * to leftChild's first tuple.
     */
    @Override
    public void reset() {
        leftChild.reset();
        rightChild.reset();
        outerTuple = leftChild.getNextTuple();
    }
	/** Method used to print the current operator
	 *  
	 */
    @Override
    public String print() {
        String expression = (this.expression != null) ? this.expression.toString() : "";
        return String.format("JOIN [" + expression + "]");
    }
	/** Method used to print the tree
	 *  
	 *  @param PrintStream ps -- variable that contains the printing stream
	 *  @param int lv -- integer that contains the level of the tree.
	 */
	@Override
	public void printTree(PrintStream ps, int lv) {
		printIndent(ps, lv);
		ps.println(print());
		leftChild.printTree(ps, lv + 1);
		rightChild.printTree(ps, lv + 1);
	}

}