
package database;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import bPlusTree.RecordID;

/** Class used to read indexes. Deserializes nodes in the tree as needed.
 * Returns Record IDs of tuples that satisfy the given low key, high key values
 * within the given index.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public class BinaryIndexReader {
		//the path to the target data file
		private String filePath;
		//a filechannel object that can be used to read from the target data file
		private FileChannel fc;
		//the buffer object used to collect ints read from the target data file
		private ByteBuffer buffer;	
		private int currentPage;
		//boolean used to determine whether we've finished reading data file
		private boolean doneReading;
		//low key integer such that all valid returned tuples have targ attribute> lowKey
		private Integer lowKey;
		//high key integer such that all valid returned tuples have targ attribute < highKey
		private Integer highKey;
		//the number of leaf nodes in the index we are reading
		private int numLeaves;
		//the number of RecordID objects remaining in the current data entry
		private int numRIDRemaining;
		//the number of data entries remaining in the current node
		private int numDataRemaining;
		
		/** Constructor for Binary Index Reader. Initializes variables, loads in header page,
		 * then calls navigate to leaf to prepare to return RecordID objects one by one.
		 * 
		 * @param indexName -- the name of the index to read
		 * @param lowKey -- the lower bound (<) of target attribute values
		 * @param highKey -- the upper bound (>) of target attribute values
		 * @throws FileNotFoundException
		 */
		public BinaryIndexReader(String indexName, Integer lowKey, Integer highKey) throws FileNotFoundException {
			this.lowKey = lowKey;
			this.highKey = highKey;
			
			this.filePath = DatabaseCatalog.getInstance().getIndexPathIfAvailable(indexName);
			FileInputStream fin = new FileInputStream(filePath);
			fc = fin.getChannel();
			
			buffer = ByteBuffer.allocate(4096);
			//load in header
			readTargetPage(0);
			doneReading = false;

			navigateToLeaf();

		}
		
		/** Reads a target page, indexing from 0. 
		 * Adjusts the filechannel position and fills buffer.
		 * @param pageIndex -- the target page index.
		 */
		private void readTargetPage(int pageIndex){
			buffer.clear();
			currentPage = pageIndex;
			long targetPosition = pageIndex*4096;
			
			try {
				fc.position(targetPosition);
				fc.read(buffer);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			buffer.flip();
		}
		
		/** Traverses the index tree to reach the first leaf
		 * that satisfies the lowKey highKey criteria.
		 */
		private void navigateToLeaf() {
			int rootAddress = buffer.getInt();
			numLeaves = buffer.getInt();
			
			//now move to root
			readTargetPage(rootAddress);
			buffer.position(4);
			
			boolean isLeaf = false;
			while (!isLeaf) {
				int nextAddress = getTargetChildAddress();
				readTargetPage(nextAddress);
				isLeaf = (buffer.getInt() == 0);
			}
			
			navigateToFirstRID();
		}
		
		/** Used to determine the address of the next child node
		 * that satisfies our lowKey highKey criteria. Compares
		 * lowKey against keyValues. Once an appropriate key value 
		 * is found, determines the corresponding child address and
		 * returns it.
		 * 
		 * @return -- the address of the target child (that satisfies low key, high key
		 * 			criteria)
		 */
		private int getTargetChildAddress() {
			int numKeys = buffer.getInt();
		
			if (lowKey == null) {
				buffer.position((2 + numKeys) * 4);
				return buffer.getInt();
			}
			
			int keyCount = 0;
			int key;
			while (keyCount < numKeys) {
				key = buffer.getInt();
				if ((lowKey + 1) < key) {
					break;
				}
				keyCount++;
			}
			
			int targetPosition = (2 + numKeys + keyCount) * 4;
			buffer.position(targetPosition);
			
			return buffer.getInt();
		}
		
		/** Navigates to the first data entry that satisfies the lowkey
		 * high key criteria within the given leaf.
		 * 
		 */
		private void navigateToFirstRID() {
			numDataRemaining = buffer.getInt();
			
			int key = buffer.getInt();
			if (lowKey != null) {
				while (key < (lowKey + 1)) {
					numDataRemaining--;
					int numRID = buffer.getInt();
					int targetPosition = buffer.position() + (numRID * 2 * 4);
					buffer.position(targetPosition);
					key = buffer.getInt();
				}
			}
			
			numRIDRemaining = buffer.getInt();
		}

		/** Advances to the next data entry. If we are out of data 
		 * entries in the current leaf, we advance to the next leaf (if there
		 * are any remaining). If the highKey criteria is no longer met,
		 * we set doneReading = true to stop returning RecordID values.
		 * 
		 */
		private void advanceToNextLeafEntry() {
			numDataRemaining--;
			if (numDataRemaining == 0 || buffer.position() == buffer.capacity()) {
				if (currentPage < numLeaves) {
					readTargetPage(currentPage + 1);
					buffer.position(4);
					numDataRemaining = buffer.getInt();
				}
				else {
					//all data entries have been processed, so done
					doneReading = true;
					return;
				}
			}
			
			int key = buffer.getInt();
			
			if (highKey!=null) {
				if (key < highKey) {
					numRIDRemaining = buffer.getInt();
				}
				else{
					doneReading = true;
				}
			} 
			else {
				numRIDRemaining = buffer.getInt();
			}
		}
		
		/** Used to get the next valid RecordID object
		 * from the index that satisfies the lowKey highKey
		 * criteria.
		 * 
		 * @return -- RecordID representation of a tuple with 
		 *			an attribute value that satisfies the given criteria.
		 */
		public RecordID getNextRID() {
			if (doneReading) {
				return null;
			}
			
			int pageId = buffer.getInt();
			int tupleId = buffer.getInt();
			numRIDRemaining--;
			if (numRIDRemaining == 0) {
				advanceToNextLeafEntry();
			}
			
			return new RecordID(pageId, tupleId);
		}
		
		/** Resets the index reader back to the first recordID.
		 * 
		 */
		public void reset() {
			readTargetPage(0);
			doneReading = false;
			
			navigateToLeaf();
			
		}
}
