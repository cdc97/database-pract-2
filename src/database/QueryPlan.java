package database;

import java.util.Collections;
import logic.*;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.*;
import visitors.PhysicalPlanBuilder;
import service.DisjointSets;
import service.Element;
import service.ExpressionService;
import service.TableService;

/**
 * Class used build query tree and properly pass the correct table name, alias names
 * and column values to operator classes for nodes to be created.
 *
 * Similar to arithmetic operators, SQL operators have a well defined "order of operation".
 * You process the from clause first allocating the scan node(s) for the object in the from clause,
 * selection node if there is a table filter predicate on that object, and then you allocate the
 * join nodes, if there are tables in the from clause that needs to be joined. The scan node is a
 * leaf node and selection node on top of a scan node is to apply any table filter on the object
 * that is scanned. If there are more than one table in the from clause, then the tables have to
 * be joined. So, you allocate a join node with left input as the scan/selection node and right
 * input as the next table in the from clause. Note that the next table in the from clause may
 * have a table filter predicate on it in which case, we allocate a scan node followed by a
 * selection node to apply the filter to the right input. The join node may have a join condition
 * in which case we allocate the join node and associate a join predicate with the join node. We
 * keep going building a left deep tree. The tree is left deep because the left input to a join may
 * be another join and the right input are leaf nodes (i.e. scan/selection). After processing joins,
 * we process three other SQL operators, projection, distinct and order-by. The input to projection
 * is the join. If the query has distinct or order by, then we allocate nodes for them to process.
 * So, we  build the query plan bottom up and then execute the plan top down. We call getNextTuple
 * on the root node in the query plan tree and this iterates down the tree getting tuples from the
 * node below and completing the SQL operations (i.e. scan, selection, joins, project, distinct,
 * order-by). We use some internal data structures to process each clauses of the SQL select
 * statement to create the query plan.
 *
 *
 *
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */

public class QueryPlan {

	 private Operator root;
	 private LogicOperator logicalRoot;
	 private ArrayList<String> fromList;
	 private ArrayList<String> fullColList;

    /** Constructor for QueryPlan.
     * Sets root to null. QueryPlan is used to build Operator Tree
     *
     */
    public QueryPlan() {
		 root = null;	 
		 logicalRoot = null;
	}
    /** Method getRootSchema
     * Gets the correct schema for the root table.
     *
     * @return schema type: ArrayList<String> that contains the table schema
     */
    public ArrayList<String> getRootSchema() {
        return root.getSchema();
    }
    
    public LogicOperator getLogicRootOperator() {
    	return logicalRoot;
    }
    
    public Operator getPhysicalRootOperator() {
    	return root;
    }


    /** Method buildQueryPlan
     * Takes the statement from the Interpreter
     * and builds a Query Tree.
     *
     * @param Stmnt type: Statement that contains query in String form
     */
	public void buildQueryPlan(Statement stmnt){
		
		PlainSelect ps;
		List<SelectItem> selItems;
		FromItem fromItems;
		List<Join> joinItems;
		Expression selectionItems;
		Distinct uniqueItems;
		List<OrderByElement> orderItems;
		
		List<Expression> listOfExpressions;
		HashMap<String, List<Expression>> selectConditions;
		HashMap<String, List<Expression>> joinConditions;
		HashMap<String, List<Expression>> oldJoinConditions;
		HashMap<String, Expression> andSelectConditions;
		HashMap<String, Expression> andJoinConditions;
		HashMap<String, Expression> andOldJoinConditions;
		fromList = new ArrayList<String>();
		ArrayList<String> selList = new ArrayList<String>();
		ArrayList<String> orderList = new ArrayList<String>();
		ArrayList<String> uniqueList = new ArrayList<String>();
		DisjointSets ds = new DisjointSets();
			
		ps = (PlainSelect) ((Select) stmnt).getSelectBody();
		selItems = ps.getSelectItems();
		fromItems = ps.getFromItem();
		joinItems = ps.getJoins();
		selectionItems = ps.getWhere();
		uniqueItems = ps.getDistinct();
		orderItems = ps.getOrderByElements();
		
		fullColList = new ArrayList<String>();
		
		System.out.println(stmnt);	
		/* 
		 * In fromList, we keep the table names or its alias if an alias is specified
		 * We keep it in the order they appear in the FROM clause
		 * Get the first table (or alias) in the FROM clause
		 */
		fromList.add(TableService.getAliasForTablenameInFromItem(fromItems));
		fullColList.add(TableService.getTableNameNoAlias(fromItems));
	    /*	
	     * First table is in the from items. 
	     * Second table and the rest will be in the join items
	     * JoinItems just lets us know if there is a join
	     * and if there is a join then we just go through 
	     * and put the remaining tables into the fromList
	     */
		if (joinItems != null){
			for (int i = 0; i < joinItems.size(); i++){
				fromList.add(TableService.getAliasForTablenameInJoinItem(joinItems, i));
				fullColList.add(TableService.getNoAliasForTablenameInJoinItem(joinItems,i));
				
			}
		}
				
		
		/*
		 * initialize the select conditions and join conditions 
		 * for all tables. 
		 */
		selectConditions = new HashMap<String, List<Expression>>();
		joinConditions = new HashMap<String, List<Expression>>();
		oldJoinConditions = new HashMap<String, List<Expression>>();
		for (String tab : fromList) {
			selectConditions.put(tab, new ArrayList<Expression>());
			joinConditions.put(tab, new ArrayList<Expression>());
			oldJoinConditions.put(tab,  new ArrayList<Expression>());
		}
		
		/*
		 * We decompose the and expression
		 */
		listOfExpressions = ExpressionService.decomposeAndExpression(selectionItems);
		
		
		for (Expression exp : listOfExpressions) {
			int index = 0;
			List<String> tabs = ExpressionService.getTableReference(exp);
	
			
			if (tabs == null) 
				index = fromList.size() - 1;
			
			for (String tab : tabs) {
				index = Math.max(index, fromList.indexOf(tab));
			}
			/*
			if (tabs == null) {
				joinConditions.get(fromList.get(fromList.size() - 1)).add(exp);
				return;
			}
			*/
			switch (tabs.size()) {
			case 0:
				selectConditions.get(fromList.get(index)).add(exp);
				break;
			case 1:
				if (!ExpressionService.isValidCmp(exp) || ExpressionService.isSelfCmp(exp)) {
					selectConditions.get(fromList.get(index)).add(exp);
					break;
				}
				
				String[] attr = new String[1];
				Integer[] range = ExpressionService.getSelRange(exp, attr);
				
				if (attr[0] == null || attr[0].isEmpty())
					throw new IllegalStateException();
				
				Element ufe = ds.find(attr[0]);
				if (range[0] != null && range[0].equals(range[1]))
					ufe.setEquality(range[0]);
				else {
					if (range[0] != null)
						ufe.setLowerBound(range[0]);
					if (range[1] != null)
						ufe.setUpperBound(range[1]);
				}
				break;
			case 2:
				oldJoinConditions.get(fromList.get(index)).add(exp);
				
				if (ExpressionService.isSameTable(exp)) {
					selectConditions.get(fromList.get(index)).add(exp);
				}
				
				if (ExpressionService.isEqualToExp(exp)) {
					BinaryExpression be = (BinaryExpression) exp;
					ds.union(be.getLeftExpression().toString(), 
							be.getRightExpression().toString());
				}	
				else
					joinConditions.get(fromList.get(index)).add(exp);
				break;
			default:
				throw new IllegalStateException();
			}
		}
		
		for (String attr : ds.attributeSet()) {
			Element ufe = ds.find(attr);
			String table = attr.split("\\.")[0];
			String column = attr.split("\\.")[1];
			List<Expression> lst = selectConditions.get(table);
			
			Integer eq = ufe.getEquality();
			Integer lower = ufe.getLower();
			Integer upper = ufe.getUpper();
			
			if (eq != null)
				lst.add(ExpressionService.createCondition(
						table, column, eq, true, false));
			else {
				if (lower != null)
					lst.add(ExpressionService.createCondition(
							table, column, lower, false, true));
				if (upper != null)
					lst.add(ExpressionService.createCondition(
							table, column, upper, false, false));
			}
		}
		
		System.out.println(ds.componentsInfo());
	

		/*
		 * If there is more than one condition per table
		 * We combine the and expression together. 
		 */
		andSelectConditions = new HashMap<String, Expression>();
		andJoinConditions = new HashMap<String, Expression>();
		andOldJoinConditions = new HashMap<String, Expression>();
		for (String tab : fromList) {
			andSelectConditions.put(tab, ExpressionService.composeAndExpression(selectConditions.get(tab)));
			andJoinConditions.put(tab, ExpressionService.composeAndExpression(oldJoinConditions.get(tab)));
			andOldJoinConditions.put(tab, ExpressionService.composeAndExpression(oldJoinConditions.get(tab)));
		}
		
		HashMap<HashSet<String>, Expression> specificJoinConditions = new HashMap<HashSet<String>, Expression>();
		
		for (int i=0; i < fromList.size(); i++) {
			String r1 = fromList.get(i);
			List<Expression> e1 = oldJoinConditions.get(r1);
			
			for (int j = i+1; j < fromList.size(); j++) {
				HashSet<String> key = new HashSet<String>(); 
				String r2 = fromList.get(j);
				List<Expression> e2 = oldJoinConditions.get(r2);
				key.add(r1);
				key.add(r2);
				
				specificJoinConditions.put(key, ExpressionService.composeAndExpression(r1, e1, r2, e2));
			}	
		}
		
		System.out.println("SPECIFIC JOIN CONDITIONS: " + specificJoinConditions);
		
		//Build the logical query plan
		List<LogicOperator> tables = new ArrayList<>();
		for (int i = 0; i < fromList.size(); i++) {
			LogicOperator tmp;
			if (i == 0)
				tmp = new LogicScanOperator(TableService.getTablenameInFromitem(fromItems), TableService.getAliasForTablenameInFromItem(fromItems));
			else
				tmp = new LogicScanOperator(TableService.getTablenameInJoinItem(joinItems, i -1), TableService.getAliasForTablenameInJoinItem(joinItems, i -1));
			if (andSelectConditions.get(fromList.get(i)) != null)
				tmp = new LogicSelectOperator(tmp, andSelectConditions.get(fromList.get(i)));
			tables.add(tmp);
		}
		
		logicalRoot = null;
		if (fromList.size() > 1) {
			logicalRoot = new LogicMultiJoinOperator(fromList, tables, andJoinConditions, ds, specificJoinConditions);
		}
		else {
			logicalRoot = tables.get(0);
		}
		
		
		if (selItems != null)
			logicalRoot = new LogicProjectOperator(logicalRoot, selItems);
		
		if (orderItems != null)
			logicalRoot = new LogicSortOperator(logicalRoot, orderList);
		
		if (uniqueItems != null)
			logicalRoot = new LogicDistinctOperator(logicalRoot, selItems);
		
		
		System.out.println("plan built");
		
		PhysicalPlanBuilder ppb = new PhysicalPlanBuilder(fromList, fullColList);
		logicalRoot.accept(ppb);
		root = ppb.getPhysicalOperator();
		
	}
    /** Method getNextResultTuple
     * Gets the Tuple value from each node.
     *
     * @return tuple type: Tuple that contains every value from the table
     */
	public Tuple getNextResultTuple(){
		Tuple tuple = root.getNextTuple();
		if (tuple == null) {
			return null;
		}
		return tuple;
	}
    /** Method dumpQueryPlan
     *  Method used for debugging
     */
    public void dumpQueryPlan(){
	        root.dump();
    }

}
		

