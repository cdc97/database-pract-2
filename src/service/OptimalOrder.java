package service;

import java.util.ArrayList;
import java.util.HashMap;

import net.sf.jsqlparser.expression.Expression;

/** Class to represent Optimal Order join plan
 * 
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class OptimalOrder {

		//cost of this join order
		public float cost;
			
		//join from left to right
		//first two joined together, result of that joined with third,...
		public ArrayList<String> joinOrder;
		
		//estimated size of this table
		public long estimatedTuples;
		
		//map for v values
		public HashMap<String, Double> vValueMap;
			
		//expressions for each join in order
		//expression at index 0 is for the join between element 0 and 1 in join Order, etc.
		public ArrayList<Expression> joinExpressions;
		
		/** Constructor for OptimalOrder. Sets variables.
		 * 
		 * @param cost --cost of this optimal plan.
		 */
		public OptimalOrder(float cost) {
			this.vValueMap = new HashMap<String, Double>();
			this.joinExpressions = new ArrayList<Expression>();
			this.cost = cost;
			this.joinOrder = new ArrayList<String>();
		}
		
		/** Return string information for this plan.
		 * 
		 */
		public String toString() {
			return "\n\nOrdering: " + joinOrder + " with cost: " + cost 
					+"\nEstimated tuple size: " + estimatedTuples 
					+"\nExpressions: " + joinExpressions + "\n"
					+"VMap: " + vValueMap;
		}
}
