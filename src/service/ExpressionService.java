package service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.operators.relational.*;

public class ExpressionService {
	
	/** Method decomposeAndExpression
	 * Takes the Select Expression list and creates
	 * a list of each individual expression thats going
	 * to be performed on the table.
	 *
	 * @param exp type: Expression that contains several expressions as one expression
	 * @return re type: List<Expression> that contains all the expressions as a list
	 */
	public static List<Expression> decomposeAndExpression (Expression exp) {
		List<Expression> le = new ArrayList<Expression>();

		while (exp instanceof AndExpression) {
			AndExpression and = (AndExpression) exp;
			le.add(and.getRightExpression());
			exp = and.getLeftExpression();
		}
		le.add(exp);
			        
		Collections.reverse(le);
		return le;
	}
	
    /** Method comeposeAndExpression
     * Puts the Select Expression list together after
     * being decomposed.
     *
     * @param e type: List<Expression> that contains a list of several expressions
     * @return re type: Expression that contains all the expressions put into one expression
     */
	public static Expression composeAndExpression (List<Expression> e) {
		if (e.isEmpty())
			return null;
		Expression re = e.get(0);
		for (int i = 1; i < e.size(); i++)
			re = new AndExpression(re, e.get(i));
		return re;
	}

	/** Method comeposeAndExpression
     * Composes an expression from any expressions in the two input lists that include
     * both input relation names.
     *
     *@param relation1 --String name of relation
     * @param e1 type: List<Expression> that contains a list of several expressions
     * @param relation2 -- String name of relation
     * @param e2 type: List<Expression> that contains a list of several expressions
     * @return re type: Expression that contains all relevant expressions put into one expression
     */
	public static Expression composeAndExpression (String relation1, List<Expression> e1, 
													String relation2, List<Expression> e2) {
		Expression result = null;
		Expression result2 = null;
		if (!e1.isEmpty()) {
			//process e1
			for (int i = 0; i < e1.size(); i++){
				Expression tmp = e1.get(i);
				String[] tmpSplit = tmp.toString().split(" ");
				String firstCol = tmpSplit[0];
				String secondCol = tmpSplit[2];
				if (firstCol.substring(0, firstCol.indexOf(".")).contains(relation2)
					|| secondCol.substring(0, secondCol.indexOf(".")).contains(relation2)) {
					if (result == null) {
						result = tmp;
					}
					else {
						result = new AndExpression(result, e1.get(i));
					}
				}
				
			}
		}
		
		if (!e2.isEmpty()) {
			//process e2
			for (int i = 0; i < e2.size(); i++){
				Expression tmp = e2.get(i);
				String[] tmpSplit = tmp.toString().split(" ");
				String firstCol = tmpSplit[0];
				String secondCol = tmpSplit[2];
				if (firstCol.substring(0, firstCol.indexOf(".")).contains(relation1)
						|| secondCol.substring(0, secondCol.indexOf(".")).contains(relation1)) {
					if (result2 == null) {
						result2 = tmp;
					}
					else {
						result2 = new AndExpression(result2, e2.get(i));
					}
				}
				
			}
		}
		
		if (result == null && result2 == null) {
			return null;
		}
		else if (result == null) {
			return result2;
		}
		else if (result2 == null) {
			return result;
		}
		
		
		return new AndExpression(result, result2);
		
	}
	
    /** Method getTableReference
     * Takes the an Expression list and finds
     * the needed tables.
     *
     * @param exp type: Expression that contains the expression we're looking to search from
     * @return re type: List<String> list of all the tables needed from that expression
     */
	public static List<String> getTableReference(Expression exp) {
		List<String> tl = new ArrayList<String>();
		        
		if (!(exp instanceof BinaryExpression))
			return tl;
		        
		BinaryExpression be = (BinaryExpression) exp;
		Expression lt = be.getLeftExpression();
		Expression rt = be.getRightExpression();
		        
		Column col;
		if (lt instanceof Column) {
			col = (Column) lt;
			if (col.getTable() == null) return null;
			tl.add(col.getTable().toString());
		}
		if (rt instanceof Column) {
			col = (Column) rt;
			if (col.getTable() == null) return null;
			tl.add(col.getTable().toString());
		}
		        
		//If table names are the same, then remove the table names.
		//if (tl.size() == 2 && tl.get(0).equals(tl.get(1)))
			//tl.remove(1);
		        
		return tl;
	}
	
	public static boolean isSelect(Expression exp) {
		List<String> tmp = getTableReference(exp);
		return (tmp != null && tmp.size() == 1);
	}
	
	public static boolean isJoin(Expression exp) {
		List<String> tmp = getTableReference(exp);
		return (tmp != null && tmp.size() == 2);
	}
	
	public static boolean isValidCmp(Expression exp) {
		if (exp == null) return false;
		return (exp instanceof EqualsTo) || 
				(exp instanceof MinorThan) || 
				(exp instanceof MinorThanEquals) || 
				(exp instanceof GreaterThan) || 
				(exp instanceof GreaterThanEquals);
	}
	
	public static Expression createCondition(String tab, String col, 
			int val, boolean isEq, boolean isGE) {
		Table t = new Table(null, tab);
		Column c = new Column(t, col);
		LongValue v = new LongValue(String.valueOf(val));
		
		if (isEq)
			return new EqualsTo(c, v);
		if (isGE) {
			Expression e = new GreaterThanEquals(c,v);
			return e;
		}
		return new MinorThanEquals(c, v);
	}
	
	public static boolean isEqualToExp(Expression exp) {
		return (exp instanceof EqualsTo);
	}
	public static boolean isSelfCmp(Expression exp) {
		if (exp == null || !isSelect(exp))
			return false;
		Expression left = ((BinaryExpression) exp).getLeftExpression();
		Expression right = ((BinaryExpression) exp).getRightExpression();
		return (left instanceof Column) && (right instanceof Column);
	}
	
	public static boolean isSameTable(Expression exp) {
		
        List<String> tl = getTableReference(exp);

        return (tl.size() == 2 && tl.get(0).equals(tl.get(1)));
        
	}
	
	public static Integer[] getSelRange(Expression exp, String[] attr) {
		if (!isSelect(exp))
			throw new IllegalArgumentException();
		
		Expression left = 
				((BinaryExpression) exp).getLeftExpression();
		Expression right = 
				((BinaryExpression) exp).getRightExpression();
		
		Integer val = null;
		
		System.out.println(left);
		System.out.println(right);
		
		if (left instanceof Column) {
			attr[0] = left.toString();
			val = Integer.parseInt(right.toString());
		}
		else {
			attr[0] = right.toString();
			val = Integer.parseInt(left.toString());
		}
		
		boolean oppo = !(left instanceof Column);
		boolean inclusive = !(exp instanceof MinorThan) && 
				!(exp instanceof GreaterThan);
		boolean isUpper = (exp instanceof MinorThan ||
				exp instanceof MinorThanEquals || 
				exp instanceof EqualsTo);
		boolean isLower = (exp instanceof GreaterThan ||
				exp instanceof GreaterThanEquals || 
				exp instanceof EqualsTo);
		
		if (!isLower && !isUpper)
			throw new IllegalArgumentException();
		
		Integer[] ret = new Integer[2];
		
		if (isLower)
			updateRange(ret, val, true, inclusive, oppo);
		if (isUpper)
			updateRange(ret, val, false, inclusive, oppo);
		
		return ret;
	}
	
	private static void updateRange(Integer[] range, int val, 
			boolean isLower, boolean inclusive, boolean oppo) {
		if (oppo) {
			updateRange(range, val, !isLower, inclusive, false);
			return;
		}
		
		if (!inclusive)
			val = (isLower) ? val + 1 : val - 1;
		
		if (isLower)
			range[0] = (range[0] == null) ? val : 
				Math.max(range[0], val);
		else
			range[1] = (range[1] == null) ? val :
				Math.min(range[1], val);
	}
	
	public static Integer[] bPlusKeys(String indexAttr, Expression selCond) {
		if (selCond == null) return null;
		List<Expression> conds = decomposeAndExpression(selCond);
		
		Integer[] ret = new Integer[2]; // low and high
		
		for (Expression expr : conds) {
			Expression left = 
					((BinaryExpression) expr).getLeftExpression();
			Expression right = 
					((BinaryExpression) expr).getRightExpression();
			
			String attr = null;
			Integer val = null;
			if (left instanceof Column) {
				attr = left.toString();
				val = Integer.parseInt(right.toString());
			}
			else {
				attr = right.toString();
				val = Integer.parseInt(left.toString());
			}
			if (attr.indexOf('.') != -1)
				attr = attr.split("\\.")[1];
			attr = indexAttr.split("\\.")[0] + "." + attr;
			if (!indexAttr.equals(attr)) continue;			
			// TODO
			// update low key and high key
			//update low key
			if(expr instanceof GreaterThan){ // inclusive low key
				if(ret[0] == null){
					ret[0] = val+1;
				} else{
					ret[0] = Math.max(ret[0], val+1);
				}
							
			} else if (expr instanceof GreaterThanEquals) {
				if(ret[0] == null){
					ret[0] = val;
				} else{
					ret[0] = Math.max(ret[0], val);
				}
			}else if(expr instanceof MinorThan){
				if(ret[1] == null){
					ret[1] = val;
				} else {
					ret[1] = Math.min(ret[1],val);
				}	
			} else if(expr instanceof MinorThanEquals){ // exclusive high key
				if(ret[1] == null){
					ret[1] = val+1;
				} else {
					ret[1] = Math.min(ret[1], val+1);
				}
			} else if (expr instanceof EqualsTo){
				ret[0] = val;
				ret[1] = val;
			}
		}		
		return ret;
	}
}
