package service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import database.DatabaseCatalog;
import database.Tuple;
import logic.LogicOperator;
import logic.LogicScanOperator;
import logic.LogicSelectOperator;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import optimizer.TableAndColumnStats;

/** Used to determine optimal join order, using instruction heuristics.
 * Uses a dynamic programming algorithm.
 * First generates and sorts powerlist (power sets but in list form).
 * Then processes sets from size = 1 upwards, computing the optimal order
 * for each set. Decides costs using previous subsets, building up to
 * the optimal solution for our complete set of relations to join.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class JoinOrderBuilder {
	//keys are relation names concatenated
	private HashMap<ArrayList<String>, OptimalOrder> optimalOrderings;
	//powerset of relations (as list)
	private ArrayList<ArrayList<String>> powerList;
	//map between set of two relations to join conditions for those two relations
	private HashMap<HashSet<String>, Expression> conditionsMap;
	//list of relations that need to be joined
	private ArrayList<String> joinRelations;
	//the list of children (corresponding to joinRelations) that we need to join
	private ArrayList<LogicOperator> correspondingChildren;
 	
	/** Constructor for JoinOrderBuilder. Sets vars, computes powerset,
	 * sorts power set, then runs join order algorithm.
	 * 
	 * @param joinRelations -- relations to join
	 * @param children -- relations to join as children
	 * @param conditionsMap -- map from set of two relations to conditions for those two relations
	 */
	public JoinOrderBuilder(ArrayList<String> joinRelations, ArrayList<LogicOperator> children,
						HashMap<HashSet<String>, Expression> conditionsMap) {

		this.joinRelations = joinRelations;
		
		this.correspondingChildren = children;
		
		this.conditionsMap = conditionsMap;
		
		optimalOrderings = new HashMap<ArrayList<String>, OptimalOrder>();
		
		
		powerList = buildPowerList(joinRelations);
		powerList.sort(new PowerSetComparator());
		powerList.remove(0);
		
		buildJoinOrder();
		System.out.println("OPTIMAL ORDERINGS" + optimalOrderings);
	}
	
	
	/** Method to start dynamic programming algorithm to determine
	 * optimal join order. 
	 * Processes powerList in order. Creates Optimal Orders for each powerset,
	 * using base cases or previous subset results to determine costs.
	 * 
	 */
	private void buildJoinOrder() {
		
		for (ArrayList<String> subList : powerList) {
			//single relation subset
			if (subList.size() == 1) {
			
				OptimalOrder plan = new OptimalOrder(0);
				plan.joinOrder.addAll(subList);
				
				populateSingleRelationOrder(plan);
				
				optimalOrderings.put(subList, plan);
			}
			//two relation subset
			else if (subList.size() == 2) {
				OptimalOrder plan = computeOptimalOrderForTwo(subList);
				
				
				optimalOrderings.put(subList, plan);
			}
			//everything else
			else {
				handleSublist(subList);
			}
		
		}
			
		
	}
	
	
	//used to create vmaps for selects or scans
	//also populates tuple estimates
	/** Handles populating information for a single relation optimal
	 * order object. Creates VMap values and estimates number of
	 * tuples output by the relation (using reduction factors if 
	 * select).
	 * 
	 * @param order -- OptimalOrder object to populate for single relation
	 */
	private void populateSingleRelationOrder(OptimalOrder order) {
		String relation = order.joinOrder.get(0);
		
		//check if its a scan or a select
		//compute appropriate v values for each attribute.
		
		LogicOperator child = correspondingChildren.get(joinRelations.indexOf(relation));
		
		String tableName;
		TableAndColumnStats stats;
		
		long tupleEstimate;
		
		if (child instanceof LogicScanOperator) {
			tableName = ((LogicScanOperator) child).getTablename();
			
			stats = DatabaseCatalog.getInstance().getTableAndColumnStats(tableName);
			
			tupleEstimate = stats.getTpNum();
			order.estimatedTuples = tupleEstimate;
			
			HashMap<String, Double> vMap = new HashMap<String, Double>();
			
			for (String attribute : stats.getAttrName()) {
				int[] attRange = stats.getRange(attribute);
				
				int high = attRange[1];
				int low = attRange[0];
				
				double v = high - low + 1;
				
				if (v > tupleEstimate) {
					v = (double) tupleEstimate;
				}
				else if (v < 1) {
					v = 1;
				}
				
				vMap.put(relation + "." + attribute, v);
			}
			
			order.vValueMap = vMap;
		}
		//must be a select then!
		else {
			//compute size, then get v values and remember to clamp if v value greater than R ( V value also can't be less than 1)
			tableName = ((LogicSelectOperator) child).getTableName();
			stats = DatabaseCatalog.getInstance().getTableAndColumnStats(tableName);
			
			tupleEstimate = stats.getTpNum();
			//GET REDUCTION VALS
			
			HashMap<String, Double> reductionFactors = ((LogicSelectOperator) child).getReductionFactors();
			Double cumulativeRF = 1d;
			if (reductionFactors != null) {
				for (Double r : reductionFactors.values()) {
					cumulativeRF = cumulativeRF * r;
				}
			}

			
			tupleEstimate = Math.round(tupleEstimate * cumulativeRF); 
			if (tupleEstimate < 1) {
				tupleEstimate = 1;
			}
			
			order.estimatedTuples = tupleEstimate;
			
			HashMap<String, Double> vMap = new HashMap<String, Double>();
			for (String attribute : stats.getAttrName()) {
				int[] attRange = stats.getRange(attribute);
				int high = attRange[1];
				int low = attRange[0];
				
				double v = high - low + 1;
				
				double rF = 1;
				//multiply by reduction val
				System.out.println("REDUCTION FACTORS" + reductionFactors);
				if ( reductionFactors != null && reductionFactors.containsKey(attribute)) {
					rF = reductionFactors.get(attribute);
					System.out.println("GOT REDUCTION FACTOR FOR: " + attribute + " RF: " + rF);
				}
				
				v = (double) (v * rF);
				
				if (v > tupleEstimate) {
					v = (double) tupleEstimate;
				}
				else if (v < 1) {
					v = 1;
				}
				
				
				vMap.put(relation + "." + attribute, v);
			}
			
			
			order.vValueMap = vMap;
		}
		
		
	}

	
	/**Looked at http://stackoverflow.com/questions/1670862/obtaining-a-powerset-of-a-set-in-java
	 * for powerset code, since I didn't think it was critical for me to come up with 
	 * the code myself. Creates powerList
	 * 
	 * @param initialList -- list from which to create powerlist
	 * @return powerlist we created
	 */
	private ArrayList<ArrayList<String>> buildPowerList(ArrayList<String> initialList) {
		ArrayList<ArrayList<String>> lists = new ArrayList<ArrayList<String>>();
		
		if (initialList.size() == 0) {
			lists.add(new ArrayList<String>());
			return lists;
		}
		
		String first = initialList.get(0);
		
		ArrayList<String> rest = new ArrayList<String>(initialList.subList(1, initialList.size()));
		
		for (ArrayList<String> l : buildPowerList(rest)) {
			ArrayList<String> newList = new ArrayList<String>();
			
			newList.add(first);
			newList.addAll(l);
			
			lists.add(newList);
			lists.add(l);
		}
		
		return lists;
	}
	
	/** Creates OptimalOrder object for two relations. 
	 * Then calls createJoinVMap object.
	 * 
	 * @param relationsToJoin -- set of 2 relations to create optimal order for.
	 * @return returns populated optimal order object for relations in relationsToJoin
	 */
	private OptimalOrder computeOptimalOrderForTwo(ArrayList<String> relationsToJoin) {
		
		//remove values to get key array
		String relationTwo = relationsToJoin.remove(1);
		//use key to get relationOne
		OptimalOrder relationOneOrder = optimalOrderings.get(relationsToJoin);
		
		relationsToJoin.add(1, relationTwo);
		
		//repeat the process
		String relationOne = relationsToJoin.remove(0);
		
		OptimalOrder relationTwoOrder = optimalOrderings.get(relationsToJoin);

		relationsToJoin.add(0, relationOne);
		
		//create order for this join, get join expressions
		OptimalOrder thisOrder = new OptimalOrder(0);
		Expression joinExpression = conditionsMap.get(new HashSet<String>(relationsToJoin));
		
		//determine order to feed args in
		if (relationOneOrder.estimatedTuples <= relationTwoOrder.estimatedTuples) {
			
			createJoinVMap(joinExpression, relationOneOrder, relationTwoOrder, thisOrder);
		}
		else {
			
			createJoinVMap(joinExpression, relationTwoOrder, relationOneOrder, thisOrder);
		}
		
		
		return thisOrder;
		
	}
	
	
	//CURRENTLY HANDLES ESTIMATING TUPLES, CREATING JOIN VMAP, AND POPULATING JOIN ORDER
	//ALSO ADDS JOIN EXPRESSION
	/** Creates join VMap, estimates join size, updates VMap values, and populates 
	 * join OptimalOrder object with all of these, in addition to relevant join expression.
	 * 
	 * @param exp -- the join expression used for this join
	 * @param left -- Optimal order object for outer relation
	 * @param right -- Optimal order object for inner relation
	 * @param join -- Optimal order object for join of outer and inner
	 * 
	 */
	private void createJoinVMap(Expression exp, OptimalOrder left, OptimalOrder right, OptimalOrder join) {
		HashMap<String, Double> joinVMap = new HashMap<String, Double>();
		joinVMap.putAll(left.vValueMap);
		joinVMap.putAll(right.vValueMap);
		
		ArrayList<HashSet<String>> equalityList = new ArrayList<HashSet<String>>();
		//get list of expressions
		List<Expression> list = ExpressionService.decomposeAndExpression(exp);

		boolean present = false;
		//for list, find equality expressions, and compute equality sets
		for (Expression cond : list) {
			String condition = null;
			if (cond != null) {
				condition = cond.toString();
			}
			
			if (condition!= null && condition.contains("=")) {
				String[] condSplit = condition.split(" = ");
				for (HashSet<String> set : equalityList) {
					if (set.contains(condSplit[0]) || set.contains(condSplit[1])) {
						set.add(condSplit[0]);
						set.add(condSplit[1]);
						present = true;
					}
					
				}
				
				if (!present) {
					HashSet<String> newSet = new HashSet<String>();
					newSet.add(condSplit[0]);
					newSet.add(condSplit[1]);
					equalityList.add(newSet);
					present = false;
				}
			}
		}
		
		//will now compute join tuple estimate, since I have access to equality sets and V haven't been updated
		double denominator = 1;
		//use equality sets to determine v value groupings
		for (HashSet<String> set : equalityList) {
			ArrayList<String> listOfSet = new ArrayList<String>(set);
			double maxV = joinVMap.get(listOfSet.get(0));
			for (int i = 1; i < listOfSet.size(); i++) {
				String attribute = listOfSet.get(1);

				double V = joinVMap.get(attribute);
				if (V > maxV) {
					maxV = V;
				}
			}
			denominator = denominator * maxV;
		}
		//compute join estimate
		long joinEstimate = left.estimatedTuples * (right.estimatedTuples / Math.round(denominator));
		
		if (joinEstimate < 1) {
			join.estimatedTuples = 1;
		}
		else {
			join.estimatedTuples = joinEstimate;
		}
		
		//now time to update v values
		for (HashSet<String> set : equalityList) {
			ArrayList<String> listOfSet = new ArrayList<String>(set);
			double minV = joinVMap.get(listOfSet.get(0));
			for (int i = 1; i < listOfSet.size(); i++) {
				String attribute = listOfSet.get(1);
				double V = joinVMap.get(attribute);
				if (V < minV) {
					minV = V;
				}
			}
			//found min, update all
			for (String attr : listOfSet) {
				joinVMap.put(attr, minV);
			}
		}
		
		join.vValueMap = joinVMap;
		clampVMap(join);
		
		join.joinExpressions.addAll(left.joinExpressions);
		join.joinExpressions.add(exp);
		
		join.joinOrder.addAll(left.joinOrder);
		join.joinOrder.addAll(right.joinOrder);
		
	}
	 
	/** Ensures that no V values are greater than estimated
	 * table size.
	 * 
	 * @param order -- order object to clamp v values for.
	 */
	private void clampVMap(OptimalOrder order) {
		long estimatedSize = order.estimatedTuples;
		for (Double V : order.vValueMap.values()) {
			if (V > estimatedSize) {
				V = (double) estimatedSize;
			}
		}
	}
	
	/** Handles subset of 3 elements and greater. Computes 
	 * min cost among all possible size orderings and chooses the 
	 * minimum configuration. Creates corresponding optimal ordering
	 * fills it in, and adds it to optimalOrderings map.
	 * 
	 * @param subset
	 */
	private void handleSublist(ArrayList<String> subset) {
		
		float minCost = 0;
		OptimalOrder minCostSubOrder = null;
		String minCostTop = null;
		
		int size = subset.size();
		//pick a top table candidate
		for (int i = 0; i < size; i++) {
			//remove top relation from list
			String top = subset.remove(i);
			
			//subset with top table removed is key for optimal ordering
			OptimalOrder subOrdering = optimalOrderings.get(subset);
			
			float cost = subOrdering.cost + subOrdering.estimatedTuples; 

			if (cost < minCost || i == 0 ) {
				minCost = cost;
				minCostSubOrder = subOrdering;
				minCostTop = top;
			}
			
			subset.add(i, top);
		}
		
		ArrayList<String> topSublist = new ArrayList<String>();
		topSublist.add(minCostTop);
		
		//get top's optimal ordering info
		OptimalOrder topOrder= optimalOrderings.get(topSublist);
		
		Expression exp = computeJoinExpression(minCostSubOrder, topOrder);
		
		
		OptimalOrder optForJoin = new OptimalOrder(minCost);
		
		createJoinVMap(exp, minCostSubOrder, topOrder, optForJoin);
		
		optimalOrderings.put(subset, optForJoin);
		
	}
	
	//assume right is a single relation
	/** Computes join expression. Assumes right relation is a single
	 * relation (not a join, cause left deep tree). 
	 * 
	 * @param left -- Optimal Order object for left relation
	 * @param right -- Optimal Order object for right relation
	 * @return -- Expression specific to the join between left and right
	 */
	private Expression computeJoinExpression(OptimalOrder left, OptimalOrder right) {
		Expression result = null;
		
		String rightRelation = right.joinOrder.get(0);
		HashSet<String> keySet = new HashSet<String>();
		keySet.add(rightRelation);
		
		for (String relation : left.joinOrder) {
			keySet.add(relation);
			Expression e = conditionsMap.get(keySet);
			
			if (e != null) {
				if (result == null) {
					result = e;
				}
				else {
					result = new AndExpression(result, e);
				}
			}
			keySet.remove(relation);
		}
		
		return result;
	}
	
	/** Returns the ultimate Optimal Order object 
	 * 
	 * @return -- the optimal order object for our full set of relations
	 */
	public OptimalOrder getFinalOptimalOrder() {
		
		return optimalOrderings.get(joinRelations);
		
	}
	
	/** Comparator class to sort PowerSet by element size.

	 *
	 */
	private class PowerSetComparator implements Comparator<ArrayList<String>> {
		/** Sort by element size. From least to greatest.
		 * 
		 */
		@Override
		public int compare(ArrayList<String> arg0, ArrayList<String> arg1) {
			if (arg0.size() < arg1.size()) {
				return -1;
			}
			
			else if (arg0.size() > arg1.size()) {
				return 1;
			}
			return 0;
		}
		
	}
	
	
}
