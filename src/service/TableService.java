package service;

import java.util.List;

import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.Join;

public class TableService {
	
	/** Method getTablenameInFromitem
     * Returns the Table name in a From Clause
     *
     * @param FromItem fromItems: FromItem that contains the From Clause
     * @return String: String that is the From clause
     */
	public static String getTablenameInFromitem(FromItem fromItems) {
		return fromItems.toString().split(" ")[0];
	}
	/** Method getTablenameInJoinItem
     * Returns the Table name in a Join Clause
     *
     * @param List<Join> joinItems: joinItems that contains the Join Clause
     * @return String: String that is the Join clause
     */
	public static String getTablenameInJoinItem(List<Join> joinItems, int i) {
		return joinItems.get(i).getRightItem().toString().split(" ")[0];
	}
	/** Method getAliasForTablenameInFromItem
     * Returns the Alias name in a From Clause
     *
     * @param FromItem fromItems: fromItems that contains the From Clause
     * @return String: String that is the Alias
     */
	public static String getAliasForTablenameInFromItem(FromItem fromItems) {
		return fromItems.getAlias() == null ? fromItems.toString() : fromItems.getAlias();
	}
	
	/** Method getAliasForTablenameInFromItem
     * Returns the Alias name in a From Clause
     *
     * @param FromItem fromItems: fromItems that contains the From Clause
     * @return String: String that is the Alias
     */
	public static String getTableNameNoAlias(FromItem fromItems) {
		String fr = fromItems.toString();
		if (fr.contains(" ")){
			return fromItems.toString().substring(0, fromItems.toString().indexOf(" "));
		}
		return fromItems.toString();
	}
	
	
	
	/** Method getAliasForTablenameInJoinItem
     * Returns the Alias name in a Join Clause
     *
     * @param List<Join> joinItems: joinItems that contains the Join Clause
     * @return String: String that is the Alias
     */
	public static String getAliasForTablenameInJoinItem(List<Join> joinItems, int i) {
		
		return joinItems.get(i).getRightItem().getAlias() == null ? 
						joinItems.get(i).getRightItem().toString() : joinItems.get(i).getRightItem().getAlias();
	}

	
	/** Method getAliasForTablenameInJoinItem
     * Returns the Table name in a Join Clause
     *
     * @param List<Join> joinItems: joinItems that contains the Join Clause
     * @return String: String that is the Alias
     */
	public static String getNoAliasForTablenameInJoinItem(List<Join> joinItems, int i) {
		
		return getTableNameNoAlias(joinItems.get(i).getRightItem());
	}
}
