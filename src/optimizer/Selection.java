package optimizer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import database.DatabaseCatalog;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.schema.Column;
import service.ExpressionService;
import bPlusTree.IndexInfo;

/**
 * Singleton class used determine the most efficient
 * way to perform Selection. (Where clause)
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */
public class Selection {
	// this is used for keep track each attr's name and its range
	static HashMap<String, Integer[]> attInfo; 
	static List<String> attName;
	static List<Double> cost;
	static double plainScanCost;
	/**Method used to determine which index to use on a selection table
	 * @param String tableName -- Table to perform Selection on
	 * @param Expression exp -- Selection Expression
	 * @return IndexInfo -- creates indexes and reduction factors for each attribute
	 */
	public static IndexInfo whichIndexToUse(String tableName, Expression exp) {
		//TODO
		/**
		 *Integer[0] is max; Integer[1] is min
		 */
		attInfo = new HashMap<String, Integer[]>();
		attName = new ArrayList<String>();
		cost = new ArrayList<Double>();
		plainScanCost = -1;
		
		List<Expression> exps = ExpressionService.decomposeAndExpression(exp);
		System.out.println("listlistlist: " + exps);
		if(exps == null) {
			throw new NullPointerException();
		}
		
		for(Expression ex : exps){
			Expression left =((BinaryExpression)ex).getLeftExpression();
			Expression right =((BinaryExpression)ex).getRightExpression();
			String attr = "";
			if(left instanceof Column && right instanceof LongValue) {
				//String[] split =((Column)left).toString().split("\\.");
				attr = ((Column)left).toString().split("\\.")[1];				
			} else if(right instanceof Column && left instanceof LongValue){
				attr = ((Column)right).toString().split("\\.")[1];
			} else if(right instanceof Column && left instanceof Column){
				//calculate plain cost
				
				plainScanCost = 
						(DatabaseCatalog.getInstance().getTableAndColumnStats(tableName).getTpNum() *
						 DatabaseCatalog.getInstance().getTableSchema(tableName).size() * 4)/4096;
				continue;
			}else if (ex instanceof EqualsTo){
				plainScanCost = (DatabaseCatalog.getInstance().getTableAndColumnStats(tableName).getTpNum() *
						DatabaseCatalog.getInstance().getTableSchema(tableName).size() * 4)/4096;
				continue;
			}
			try {
				
				String[] in = {attr};
				System.out.println("get range " + ex);
				Integer[] range = ExpressionService.getSelRange(ex,in);
				updateAttInfo(attr,range);
			} catch (IllegalArgumentException e) {
				continue;
			}	
		}

		// tpsize: tuple number  * 4 bytes * attribute number
		TableAndColumnStats s = DatabaseCatalog.getInstance().getTableAndColumnStats(tableName);
		double tpsize = DatabaseCatalog.getInstance().getTableAndColumnStats(tableName).getTpNum() *
				DatabaseCatalog.getInstance().getTableSchema(tableName).size() * 4;
		// page number 
		double pageNum = tpsize / 4096;
		// calculate each attr's cost
		Set<Entry<String,Integer[]>> set = attInfo.entrySet();
		// each entry is a attr name : range
		for(Entry<String,Integer[]> entry : set){
			IndexInfo indexInfo = DatabaseCatalog.getInstance().getIndexInfo(tableName, entry.getKey());	
			double localCost = -1;
			if(indexInfo == null){ // only plain scan
				
				localCost = pageNum;
				
			} else { // has index 
				int[] minMax = DatabaseCatalog.getInstance().getTableAndColumnStats(tableName).getRange(entry.getKey());
				double maxRange = minMax[1] - minMax[0];
				double curLow = 0;
				double curHigh = 0;	
				
				Integer[] i = entry.getValue();
				//check max
				if(entry.getValue()[0] == null){
					curLow = minMax[0];
				}
				else
					curLow = entry.getValue()[0];
				//check min
				if(entry.getValue()[1] == null){
					curHigh = minMax[1];
				}
				else
					curHigh = entry.getValue()[1];
				// calculate reduction factor
				double rf =(curHigh - curLow) /maxRange;
				indexInfo.setRF(rf);
				
				if(indexInfo.isClustered()){ // if cur attr has clustered index
					localCost = 3 + pageNum * rf;
				} else {
					double leafNum = indexInfo.getNumOfLeafNodes();
					localCost = 3 + (leafNum + DatabaseCatalog.getInstance().getTableAndColumnStats(tableName).getTpNum())*rf;
				}
			}
			attName.add(entry.getKey());
			cost.add(localCost);	
		}
		// check which attr has the lowerest cost
		if(plainScanCost !=-1) {
			if(cost.size()!=0){
				for(double c : cost){
					if(plainScanCost < c){
						return null;
					}
				}
			}
		}
		if(cost.size()==0){
			return null;
		}
		if(attName.size() != cost.size()){
			throw new IllegalArgumentException();
		}
		int minIndex = 0;
		double minCost = cost.get(0);
		for(int i = 1; i < cost.size(); i++){
			if(minCost > cost.get(i)){
				minCost = cost.get(i);
				minIndex = i;
			}
		}
		return DatabaseCatalog.getInstance().getIndexInfo(tableName, attName.get(minIndex));
		
		
		
	}
	/**Method used to determine which index to use on a selection table
	 * @param String tableName -- Table to perform Reduction factor computation on
	 * @param Expression exp -- Selection Expression
	 * @return HashMap<String,Double> -- Reduction factors
	 */
	public static HashMap<String, Double> computeReductionFactors(String tableName, Expression exp) {
		//TODO
		/**
		 *Integer[0] is max; Integer[1] is min
		 */
		attInfo = new HashMap<String, Integer[]>();
		attName = new ArrayList<String>();
		cost = new ArrayList<Double>();
		plainScanCost = -1;
		
		HashMap<String, Double> rfactors = new HashMap<String, Double>();
		
		List<Expression> exps = ExpressionService.decomposeAndExpression(exp);
		System.out.println("listlistlist: " + exps);
		if(exps == null) {
			throw new NullPointerException();
		}
		
		for(Expression ex : exps){
			Expression left =((BinaryExpression)ex).getLeftExpression();
			Expression right =((BinaryExpression)ex).getRightExpression();
			String attr = "";
			if(left instanceof Column && right instanceof LongValue) {
				//String[] split =((Column)left).toString().split("\\.");
				attr = ((Column)left).toString().split("\\.")[1];				
			} else if(right instanceof Column && left instanceof LongValue){
				attr = ((Column)right).toString().split("\\.")[1];
			} else if(right instanceof Column && left instanceof Column){
				//calculate plain cost
				
				plainScanCost = 
						(DatabaseCatalog.getInstance().getTableAndColumnStats(tableName).getTpNum() *
						 DatabaseCatalog.getInstance().getTableSchema(tableName).size() * 4)/4096;
				continue;
			}else if (ex instanceof EqualsTo){
				plainScanCost = (DatabaseCatalog.getInstance().getTableAndColumnStats(tableName).getTpNum() *
						DatabaseCatalog.getInstance().getTableSchema(tableName).size() * 4)/4096;
				continue;
			}
			try {
				
				String[] in = {attr};
				System.out.println("get range " + ex);
				Integer[] range = ExpressionService.getSelRange(ex,in);
				updateAttInfo(attr,range);
			} catch (IllegalArgumentException e) {
				continue;
			}		
		}

		// tpsize: tuple number  * 4 bytes * attribute number
		TableAndColumnStats s = DatabaseCatalog.getInstance().getTableAndColumnStats(tableName);
		double tpsize = DatabaseCatalog.getInstance().getTableAndColumnStats(tableName).getTpNum() *
				DatabaseCatalog.getInstance().getTableSchema(tableName).size() * 4;
		// page number 
		double pageNum = tpsize / 4096;
		// calculate each attr's cost
		Set<Entry<String,Integer[]>> set = attInfo.entrySet();
		// each entry is a attr name : range
		for(Entry<String,Integer[]> entry : set){
			IndexInfo indexInfo = DatabaseCatalog.getInstance().getIndexInfo(tableName, entry.getKey());	
			double localCost = -1;
			{ // only plain scan
				
				localCost = pageNum;
				
			}
			
			{ // has index 
				int[] minMax = DatabaseCatalog.getInstance().getTableAndColumnStats(tableName).getRange(entry.getKey());
				double maxRange = minMax[1] - minMax[0];
				double curLow = 0;
				double curHigh = 0;	
				
				Integer[] i = entry.getValue();
				//check max
				if(entry.getValue()[0] == null){
					curLow = minMax[0];
				}
				else
					curLow = entry.getValue()[0];
				//check min
				if(entry.getValue()[1] == null){
					curHigh = minMax[1];
				}
				else
					curHigh = entry.getValue()[1];
				// calculate reduction factor
				double rf =(curHigh - curLow) /maxRange;
				//indexInfo.setRF(rf);
				rfactors.put(entry.getKey(), rf);
				
				
			}
			attName.add(entry.getKey());
			cost.add(localCost);	
		}
		return rfactors;
		
		
		
	}
	/** 
	 * update the range for each of my attr
	 * @param String attr -- Table Column
	 * @param Integer[] range -- Range of indexes
	 * 
	 */
	public static void updateAttInfo(String attr, Integer[] range){
		if(!attInfo.containsKey(attr)){
			Integer[] r = new Integer[2];
			for(int i = 0; i < range.length; i++){
				r[i] = range[i];
			}
			attInfo.put(attr, r);
		} else {
			Integer[] preRange = attInfo.get(attr);		
		// update min  
		if(range[0]!=null){
			if(preRange[0] == null){
				preRange[0] = range[0];
			} else {
				preRange[0] = Math.max(preRange[0],range[0]);
			}
				attInfo.put(attr, preRange);
			//}
		}
		//update max
		if(range[1]!=null){
			if(preRange[1] == null){
				preRange[1] = range[1];
			} else {
				preRange[1] = Math.min(preRange[1],range[1]);		
			}
				attInfo.put(attr, preRange);
			//}
		}			
		}
	}
}