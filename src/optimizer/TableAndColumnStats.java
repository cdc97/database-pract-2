package optimizer;

import java.util.HashMap;
import java.util.Set;
/**Class that creates the Table and column stats that will be output
 * by stat collector file.
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 */
public class TableAndColumnStats {
	
	String tabName;
	HashMap<String, int[]> map;
	int attNum = -1;
	int tpNum;
	/**Constructor that calculates the ranges
	 * 
	 * @param String tabName -- Table name
	 * @param String [] name -- Column Names
	 * @param int [] low -- lower bound
	 * @param int [] high -- upper bound
	 *
	 */
	public TableAndColumnStats (String tabName, String[] name, int[] low, int[] high ){
		this.tabName = tabName;
		map = new HashMap<String, int[]>();
		if(name.length!= low.length || low.length != high.length
				|| name.length != high.length){
			throw new IllegalArgumentException();
		}			
		for(int i = 0; i < name.length; i++){
			int[] range ={ low[i], high[i]};
			if(name[i]!= null) {
				map.put(name[i],range);
			}
			
		}
		attNum = name.length;
	
	}
	
	/** 
	 * get the total number of attribute 
	 * @return int attNum -- Attribute number
	 */
	public int getAttNum(){
		if(attNum == -1){
			throw new IllegalArgumentException();
		}
		return attNum;
	}
	
	/**
	 * 
	 * return an int array contains the lowest and highest value 
	 * related to the current attr name, return null if the attr
	 * is not in the table  
	 * @input: attribute name 
	 * @return: int[] int[0] is min; int[1] is high
	 */
	public int[]  getRange(String attrName){
		if(map.containsKey(attrName)){
		return map.get(attrName);
		} else {
			return null;
		}
	}
	/**
	 * return a list of attr names corresponding to this table 
	 * @return
	 */
	public String[] getAttrName(){
		Set<String> attrName = map.keySet();
		String[] list = attrName.toArray(new String[attrName.size()]);
		return list;
	}
	/**
	 * return the table name 
	 * @return
	 */
	public String getTableName(){
		return tabName;
	}
	/**
	 * add new attr to the current table 
	 * @param String attrName -- Column Name
	 * @param int low -- range low
	 * @param int high -- range high
	 */
	public void addAttr(String attrName, int low, int high){
		if(map.containsKey(attrName)){
			throw new IllegalArgumentException();
		} else {
			int[] range = {low, high};
			map.put(attrName,range);
		}
	}
	
	/**
	 * set the tuple number for this table
	 * @param int num -- number of tuples
	 */
	public void setTpNum(int num){
		tpNum = num;
	}
	/**
	 * Get number of tuples
	 * @return num of tps in this table
	 */
	public int getTpNum(){
		return tpNum;
	}
	
	
}

