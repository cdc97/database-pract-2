package optimizer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import database.BinaryTupleReader;
import database.DatabaseCatalog;
import database.Tuple;
/**Stat collector class that writes stats to output file
 * 
 * @author Achintya Sankarraman (as3622), Cameron Chafetz (cdc97)
 *
 */

//Collect statistics on each table and columns in the database
//the table and columns are described in the schema file (in input/db directory)
//write the statistic information to stats.txt file in input/db directory
public class StatCollector {

	File statFile;
	BufferedReader statFileBufferedReader;
	BufferedWriter statFileBW;
	HashMap<String, TableAndColumnStats> statInfo; 
	/**Stat collector class that writes stats to output file
	 * 
	 * @throws FileNotFound exception when there's no file to output too. 
	 *
	 */
	public StatCollector () throws IOException{
			
		//Open the statFile for writing statistics
		try {
			statFile = new File(DatabaseCatalog.getInstance().getOutputDir() + File.separator + "stats.txt" );	
			statFileBW = new BufferedWriter(new FileWriter(statFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the schema file and read the table and column (attribute) names
	 * For each table, open the data file and compute the stats
	 * The stats of interest are number of tuples in the table and for each column 
	 * (attributes) in the table, the column min anx max value
	 * Collect these stats for each table and write it to the stats.txt file
	 * @throws IOException
	 */
	public void gatherTableandColumnStats () throws IOException {
		
		// read the schema file 
		File schemaFile = new File(DatabaseCatalog.getInstance().getSchemaFile());
		BufferedReader schemaFileBR = new BufferedReader(new FileReader(schemaFile));
		statInfo = new HashMap<String,TableAndColumnStats>();
		String line = null;
		
		while((line = schemaFileBR.readLine()) !=null) {
			
			String[] names = line.split("\\s+");
			String tableName = names[0];
			TableAndColumnStats TCStats = 
					new TableAndColumnStats(tableName, new 
							String[names.length-1],new int[names.length-1],new int[names.length-1]);
			int numberOfTuples = 0;
		
			//for each attributes (columns) initialize the min/max value
			//set min to a very high value and max to a very low value
			int minValues[] = new int[names.length-1];
			int maxValues[] = new int [names.length-1];
			for (int i = 0;  i < names.length - 1; i++) {
				minValues[i] = Integer.MAX_VALUE;
				maxValues[i] = Integer.MIN_VALUE;
			}
			

			BinaryTupleReader dataFileBR = new BinaryTupleReader(DatabaseCatalog.getInstance().getInputDir() + 
					DatabaseCatalog.getInstance().getDataDir() + File.separator + tableName);
			Tuple tp  = null;
			
			while((tp = dataFileBR.readNextTuple()) != null){
				numberOfTuples++;
				for(int i = 0; i < names.length - 1; i++){
					minValues[i] = Math.min(minValues[i], tp.getElement(i));
					maxValues[i] = Math.max(maxValues[i], tp.getElement(i));
				}
			}
			
			statFileBW.write(tableName + " ");
			statFileBW.write(numberOfTuples + " ");
			
			for(int i = 1; i < names.length; i++){ // not count table name 
				statFileBW.write(names[i]+",");
				statFileBW.write(minValues[i-1]+",");
				statFileBW.write(maxValues[i-1] + " ");
				TCStats.addAttr(names[i], minValues[i-1], maxValues[i-1]);
				TCStats.setTpNum(numberOfTuples);
			}
			
			statInfo.put(tableName, TCStats);
			statFileBW.newLine();
			
			
		}
		statFileBW.close();		
		schemaFileBR.close();
	}
	
	/**
	 * return a hash map containing all the table names as key
	 * and the table stats as the value
	 * @return handle to the HashMap containing the stats for each table
	 */
	public HashMap<String,TableAndColumnStats> getMap(){		
		return statInfo;	
	}
	
		
}
